# Developing

## Git naming conventions

We make use of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) with one exception: we write `feature` instead of `feat`.

This means not starting the description with a capital leter and not ending with a period.
The description will be in simple present tense (`add button` instead of `adding button` or `added button`).

The _types_ are being used in our branch names as well, which are structered in the following maner:

```
<type>/<issue number>-<short description>
```

All lower case, dashes for separating issue number and words

**Examples**

- `feature/123-something-new`
- `fix/456-the-problem`
- `chore/789-update-dependencies`

## Tooling

- The map system is built using [MapLibre](https://maplibre.org/)
- See the [Mapbox playground](https://docs.mapbox.com/playground/) for various map features

## Guidelines when working with the application

This chapter helps you get a base understanding of how data is moved from A to B within the application.

### Maps

We use [MapLibre](https://maplibre.org/) as viewer for our maps. The tool is built like an onion. There are many layers to explore:

1. The map class is initiated. Once this class has loaded we can start loading map data;
2. We retrieve data from a selected **source**. This is currently either a GeoJSON, rastertile or vectortile;
3. The source its **features** are styled and filtered using the attributes that came from the source.

### online services

To monitor the availability of our online services we use [UptimeBot](https://uptime.bot/).

### Geodata

We're using mulitple external sources to generate a number of map layers. To monitor these sources a monitoring tool is used called [Freshping](freshping.io).

# App structure

The maps are structured under various tabs. These tabs have layers and when selecting these they start the process of potentially loading a new datalayer and stylelayer.

These layer states are saved in a `activeLayerId` and `activeLayerData`. The `activeLayerData` can reference a subLayer, which is why the `activeLayerId` exists.

## Context and Relevant layers

Todo!

# Naming of layers and structure in the application

Our own configuration syntax and types :

```js
DataLayers [
    DataLayer {...}
    DataLayer {...}
    DataLayer {
        StyleLayers [
            StyleLayer {}
            StyleLayer {}
            StyleLayer {}
        ]
    }
    DataLayer {
        sublayers [
            DataLayer {
                StyleLayers [
                    StyleLayer {}
                    StyleLayer {}
                    StyleLayer {}
                ]
            }
            DataLayer {
                StyleLayers [
                    StyleLayer {}
                    StyleLayer {}
                ]
            }
        ]
    }
]
```

A chosen DataLayer becomes : `activeDataLayer` with a `activeDataLayerKey` throughout the application.

In `Map` conform Mapbox Styling syntax are :

- mapLayer
- currentLayers
- newLayers
- layer

A StyleLayer becomes a `mapLayer` when converted to a Mapbox Styling Syntax. Also used in `MakeStyleLayer.js`

In `StyleLayers.js` are `mapLayer`s defined . So confrom Mapbox styling syntax .

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

module.exports = {
  output: 'standalone',

  typescript: {
    ignoreBuildErrors: true,
  },
}

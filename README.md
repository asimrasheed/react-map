# Vector tile map viewer / analyse tool.

React-map is our go-to-tool to create visuals of mapping data which
is configurabe for mulitple topic. The first topic is DEGO / Energytransition.

## Datavoorziening Energie Gebouwde Omgeving (DEGO)

[http://tvw.commondatafactory.nl](http://tvw.commondatafactory.nl).

[http://dego.vng.nl](http://dego.vng.nl).
[http://dook.commondatafactory.nl](http://dook.commondatafactory.nl).

## AKA Transitievisie Warmte Viewer

[![pipeline status](https://gitlab.com/commondatafactory/react-map/badges/TVWviewer/pipeline.svg)](https://gitlab.com/commondatafactory/react-map/-/commits/TVWviewer)
[![coverage report](https://gitlab.com/commondatafactory/react-map/badges/TVWviewer/coverage.svg)](https://gitlab.com/commondatafactory/react-map/-/commits/TVWviewer)

## Introduction

DEGO / Transitivisie Warmte Viewer is a build for all municipalities
in the Netherlands to facilitate in the specific information needs around
energy and buildings. Build on behalf of the dutch association of municipalities.
[VNG Realisatie](https://vng.nl).

All municipalities are required to make plans and facilitate the transition away from
fossil gas. The Viewer is build ontop of carefully collected energy and energy related datasets.
To help the people of the municipalities set up their
policy, and make decisions of which neighbourhoods are best to start with.
The viewer is visualizing the most relevant data in an easy and most comprehensive
manner.

This way, everyone can use the tool the way they like best or the way they need
it most, for example to make decisions, or just to convey their ideas and make
it feasible to talk about the upcoming project.

It is simple to experiment with different data on buildings and neighbourhoods
by clicking the buttons, and see which neighbourhoods need more attention than
others (i.e. which seem more difficult).

### Common Data Factory

The project Transitievisie Warmte is part of the Common Data Factory
([http://commondatafactory.nl](http://commondatafactory.nl)) which helps
municipalities with processing data and visualizing it.
We make use of the Standard for Public Code
([https://standard.publiccode.net/](https://standard.publiccode.net/)), and
follow the principles of Common Ground
([https://commonground.nl](https://commonground.nl)).

## Installation

This application can easily be installed in a Haven compliant cluster by
using our [Helm chart](https://gitlab.com/commondatafactory/helm-charts/react-map).

Please refer to the [Helm chart](https://gitlab.com/commondatafactory/helm-charts/react-map) for more information about installation.

## Help us further

As with most things, every bit of help is welcome. Therefore, we ask you to give feedback on the application!

In case something goes wrong, or doesn't behave as expected, but also when you
need/want something that the tool does not provide yet, give us a heads up.
Also, for any questions, you can reach out to us:

paul.suijkerbuijk@vng.nl or
gerdien.vandevreede@vng.nl

## Warning about data quality

We take upmost care in data quality, but our source data does not alway reflect reality. There is no guarantee on completeness, actuality and factuality of the displayed data. We can not show things other than the data that is provided to us. This also means the data shared throughout this project cannot be used for legal grounds, merely as a point of departure.

## Developers

Read our [developer guide lines](./DEVELOPER.md)

## Available Scripts

In the project directory, you can run:

### `yarn`

To install all dependencies.

### `yarn dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

You can also start in development mode with a specific configuration by running `yarn dev:dego` or `yarn dev:dook` for DEGO or DOOK respectively.

### `yarn build`

Builds the app for production. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

The result for on a static file server is available in the `out` directory.

### `yarn start`

Runs the production build.

You will need to run `yarn build` first (see above).

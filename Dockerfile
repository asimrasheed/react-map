FROM nginx:alpine AS web

# Add bash
RUN apk add --no-cache bash tree

# Copy nginx configuration
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf

# npm run build:dev fills the out folder
COPY out /usr/share/nginx/html

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
  chown -R appuser /var/run/nginx.pid && \
  chown -R appuser /var/cache/nginx

USER appuser

# Start Nginx server
EXPOSE 8080
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]

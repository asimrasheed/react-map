// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { GetStaticProps } from 'next'
import { FC } from 'react'
import Head from 'next/head'
import { ThemeProvider } from 'styled-components'

import App from '../src/components/App'
import AppContext from '../src/components/AppContext'
import { LayerProvider } from '../src/providers/layer-provider'
import { MenuPanelProps } from '../src/components/menu-panel/MenuPanel'
import { TabProvider } from '../src/providers/tab-provider'
import { DrawProvider } from '../src/providers/draw-provider'
import { theme } from '../src/theme'
import { MapProvider } from '../src/providers/map-provider'
import { GlobalStyle } from '../src/styling/global-styles'
import { ToasterProvider } from '../src/providers/toaster-provider'

const getConfig = async () => {
  switch (process.env.CONFIG) {
    case 'dego':
      return (await import('../app.dego.json')).default
    case 'dook':
      return (await import('../app.dook.json')).default
    default: {
      return (await import('../app.json')).default
    }
  }
}

export const getStaticProps: GetStaticProps = async () => {
  const config = await getConfig()
  return { props: { config } }
}

export type IndexProps = {
  config: {
    product: string
    about: string
    head: {
      description: string
      title: string
    }
    title: string
    subtitle: string
    tabs: MenuPanelProps['activeTab'][]
    auth?: {
      loginUrl: string
      userEndpoint: string
    }
  }
}

const Index: FC<IndexProps> = ({ config }) => {
  return (
    <>
      <Head>
        <title>{config.head.title}</title>
        <meta name="description" content={config.head.description} />
      </Head>

      <AppContext.Provider value={config}>
        <TabProvider tabs={config.tabs}>
          <LayerProvider>
            <DrawProvider>
              <ThemeProvider theme={theme}>
                <MapProvider>
                  <ToasterProvider>
                    <GlobalStyle />
                    <App product={config.product} />
                  </ToasterProvider>
                </MapProvider>
              </ThemeProvider>
            </DrawProvider>
          </LayerProvider>
        </TabProvider>
      </AppContext.Provider>
    </>
  )
}

export default Index

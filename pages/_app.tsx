// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import Head from 'next/head'
import '../src/styling/style.scss'

type AppProps = {
  Component: React.FC<JSX.ElementAttributesProperty>
  pageProps: JSX.ElementAttributesProperty
}

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  const goatcounterSettings = JSON.stringify({
    allow_local: false,
    path: (path) => location.host + path,
    referrer: (referrer) => document.referrer,
  })

  return (
    <>
      <Head>
        <title>TVW viewer</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
      </Head>

      <script
        data-goatcounter="https://geitjes.commondatafactory.nl/count"
        data-goatcounter-settings={goatcounterSettings}
        async
        src="//geitjes.commondatafactory.nl/count.js"
      />

      <Component {...pageProps} />
    </>
  )
}

export default App

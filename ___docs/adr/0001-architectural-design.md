# Architectural Design

## Naming and hierarchy

- Tenant (Municipality)
- Profile (DOOK, DEGO)
- Category (Gebouwen, Bedrijventerreinen)
- Data layer (Bouwjaar, Bedrijventerreinen uit OSM) – The are the layers as the visitor can see them and turn them on and off.
- Style layer – This is a technical implementation of a layer on the map, with a focus on MapBox.

### Data layer

A data layer has a configuration for:

- Metadata for the user
- One or more style layers
- Legend

#### Ordering style layers

We will allow fine-grained control over the ordering of style layers as displayed on the map by defining the style layers in an array in the data layer configuration. The order of the layers in the array will be the order as they are shown on the map, the last layer in the array will be shown on top of all other layers on the map.

You can simply define one relevant layer in the array and it will be inserted between all other context layers in the default insertion point. If you want to have it inserted elsewhere you can do so by mentioning the context layers as well.

Examples to give an idea (names might not be accurate):

```
[
  { relevant layer }
]
```

```
[
  { relevant layer }
  "roads"
  "admin"
  { extra layer }
  "labels"
]
```

### Style layer

#### Functionality

A style layer can have configuration for the following functionalities:

- A little pop-up on hovering a feature
- Draw tool for counting
- Pinning the layer
- Click functionality for:
  - highlighting the feature
  - showing more details in the details panel

#### Type

We distinguish two types of style layers:

- Relevant layers
- Context layers, which can be from either
  - OSM source, or
  - Custom defined

Context layers can be from the OSM source and are defined in the mapstyle json files, or custom defined which will be defined elsewhere.

## Action items

- Label OSM context layers by use of the metadata property the MapBox documentation allows, to distinguish them from our custom context layers.
- Allow defining the order of all layers in the styleLayers array in the dataLayer configuration. Starting with one data layer to try out. Allowing for default insertion point of the layers defined in case not all context layers are listed.
- Separate our custom context layers from generic style layer generation methods defined in StyleLayersNew.
- Highlight and center a feature on clicking (when showing the detail panel).
- Make the counting functionality generic so it can be enabled per style layer.

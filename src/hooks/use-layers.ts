// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import { LayerContext, LayerContextProps } from '../providers/layer-provider'

export const useLayers = (): LayerContextProps => {
  return useContext(LayerContext)
}

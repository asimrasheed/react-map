// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import useSWR from 'swr'
import { useCallback, useContext, useEffect } from 'react'
import AppContext from '../components/AppContext'

export interface Name {
  last: string
  first: string
}

export interface Traits {
  name: Name
  email: string
}

export interface VerifiableAddress {
  id: string
  value: string
  verified: boolean
  via: string
  status: string
  verified_at: Date
}

export interface RecoveryAddress {
  id: string
  value: string
  via: string
}

export interface Identity {
  id: string
  schema_id: string
  schema_url: string
  traits: Traits
  verifiable_addresses: VerifiableAddress[]
  recovery_addresses: RecoveryAddress[]
}

export interface RootObject {
  id: string
  active: boolean
  expires_at: Date
  authenticated_at: Date
  issued_at: Date
  identity: Identity
  userType: 'super' | 'administrator' | 'analyst'
  permissions: Partial<RelationTuple>[]
}

interface RelationTuple {
  namespace: string
  object: string
  relation: string
  subject_id: string
  subject_set: {
    namespace: string
    object: string
    relation: string
  }
}

type UseUserResponse = {
  user: RootObject
  isLoading: boolean
  isError: boolean
  isLoggedIn: boolean
  refreshPermissions: any
}

const fetcher = async (url: string) => {
  const response = await fetch(url, { credentials: 'include' })
  if (!response.ok) {
    throw response.statusText
  }
  return await response.json()
}

const useUser = (): UseUserResponse => {
  const config = useContext(AppContext)

  const { data: user, error: userError } = useSWR<RootObject>(
    config.auth?.userEndpoint || null,
    fetcher,
    {
      shouldRetryOnError: false,
      revalidateIfStale: false,
    }
  )

  // TODO: A bit confusing, but respones like 401 currently do not give out an error. This

  return {
    user,
    isLoading: !!(userError && user),
    isError: userError,
    isLoggedIn: user?.active,
    refreshPermissions: () => ({}),
    // refreshPermissions: permissionsMutate,
  }
}

export default useUser

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import build from 'next/dist/build'
import useSWR from 'swr'

const fetcher = async (url: string) => {
  const response = await fetch(url)
  return await response.json()
}

export type Building = {
  gemeentenaam: string
  huisletter: string
  huisnummer: string
  huisnummertoevoeging: string
  numid: string
  postcode: string
  straat: string
  adres: string
  buurtcode: string
  buurtnaam: string
  energieklasse: string
  gebruiksdoelen: string
  gemeentecode: string
  labelscore_definitief: string
  labelscore_voorlopig: string
  oppervlakte: string
  p6_gas_aansluitingen_2020: string
  p6_gasm3_2020: string
  p6_kwh_2020: string
  p6_totaal_pandoppervlak_m2: string
  pand_bouwjaar: string
  pand_gas_aansluitingen: string
  pand_gas_ean_aansluitingen: string
  pid: string
  point: string
  provinciecode: string
  provincienaam: string
  vid: string
  wijkcode: string
  wijknaam: string
  woning_type: string
  woningequivalent: string
}

type UseBuildingResponse = {
  building: Building[]
  isLoading: boolean
  isError: boolean
}

const useBuilding = (buildingId: string): UseBuildingResponse => {
  buildingId = buildingId.toString().padStart(16, '0')
  const { data, error } = useSWR(
    `https://ds.vboenergie.commondatafactory.nl/list/?match-pid=${buildingId}`,
    fetcher
  )

  return {
    building: data,
    isLoading: !error && !data,
    isError: error,
  }
}

export default useBuilding

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import { TabContext, TabContextProps } from '../providers/tab-provider'

export const useTabs = (): TabContextProps => {
  return useContext(TabContext)
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { defaultTheme } from '@commonground/design-system'

const tokens = {
  ...defaultTheme.tokens,
}

const theme = {
  ...defaultTheme,
  tokens,
  boxShadow: '0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24)',
}

export { theme }

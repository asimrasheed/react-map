// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

function SvgDropLine(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width="1em"
      height="1em"
      {...props}
    >
      <path
        d="M12 3.1L7.05 8.05a7 7 0 109.9 0L12 3.1zm0-2.828l6.364 6.364a9 9 0 11-12.728 0L12 .272z"
        fill="'none'"
      />
    </svg>
  )
}

export default SvgDropLine

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import {
  createContext,
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react'
import { useRouter } from 'next/router'
import {
  degoDataLayers,
  dookDataLayers,
  DataLayerProps,
  AuthRole,
} from '../data/DataLayers'
import useUser from '../hooks/use-user'
import { setURLParams, UrlParamsKey } from '../utils/url-params'

export interface LayerContextProps {
  activeLayerId: string
  setActiveLayerId: Dispatch<SetStateAction<LayerContextProps['activeLayerId']>>
  activeSublayerKey: string
  setActiveSublayerKey: Dispatch<
    SetStateAction<LayerContextProps['activeSublayerKey']>
  >
  activeDataLayer: Partial<DataLayerProps>
  setActiveDataLayer: Dispatch<
    SetStateAction<LayerContextProps['activeDataLayer']>
  >
  hasAccess: (layer: Partial<DataLayerProps>) => boolean
}

const LayerContext = createContext<LayerContextProps>({
  activeLayerId: 'layer0',
  setActiveLayerId: (e) => e,
  activeSublayerKey: '',
  setActiveSublayerKey: (e) => e,
  activeDataLayer: {},
  setActiveDataLayer: (e) => e,
  hasAccess: () => true,
} as LayerContextProps)

const dataLayers = [...degoDataLayers, ...dookDataLayers]

const LayerProvider = (props) => {
  const router = useRouter()
  const { query } = router || {}
  const layerParam: string = (query?.[UrlParamsKey.Layer] as string) || 'layer0'
  const sublayerParam: string = (query?.[UrlParamsKey.Sublayer] as string) || ''

  const [activeLayerId, setActiveLayerId] = useState('')
  const [activeSublayerKey, setActiveSublayerKey] = useState('')

  const [activeDataLayer, setActiveDataLayer] = useState<
    Partial<DataLayerProps>
  >({} as Partial<DataLayerProps>)
  const { user, isLoggedIn } = useUser()

  const hasAccess = useCallback(
    (datalayer: Partial<DataLayerProps>): boolean => {
      let userRole: AuthRole = null

      user?.permissions.some((permission) => {
        if (permission.relation !== 'access') {
          return
        }
        switch (permission.object) {
          case 'general_dego_data':
            userRole = 'general_dego_data'
            return

          case 'kvk_gm0848':
          case 'kvk_gm0796':
            userRole = 'autobranche'
            return

          case 'bedrijventerreinen_gm0848':
          case 'bedrijventerreinen_gm0796':
            userRole = 'bedrijventerreinen'
            return
        }
      })

      if (
        !datalayer.authRoles ||
        (userRole === null &&
          datalayer.authRoles.some((role) => role === 'no_role'))
      ) {
        return true
      }

      return datalayer.authRoles?.includes(userRole)
    },
    [isLoggedIn, user]
  )

  // from router url
  useEffect(() => {
    if (!layerParam) {
      return
    }

    const foundLayer = dataLayers.find((layer) => layer.id === layerParam)

    if (!foundLayer) {
      setActiveLayerId(dataLayers[0].id)
      setActiveSublayerKey('')
      return
    }

    setActiveLayerId(layerParam)

    if (
      foundLayer.subLayers &&
      sublayerParam &&
      foundLayer.subLayers.some((sublayer) => sublayer.name === sublayerParam)
    ) {
      setActiveSublayerKey(sublayerParam)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layerParam, sublayerParam])

  // NOTE: setting the actual selected layer and url params
  useEffect(() => {
    const currentLayer = dataLayers.find((layer) => layer.id === activeLayerId)

    if (currentLayer && hasAccess(currentLayer)) {
      if (activeSublayerKey && currentLayer.subLayers) {
        const currentSublayer = currentLayer.subLayers.find(
          (sublayer) => sublayer.name === activeSublayerKey
        )
        setActiveDataLayer(currentSublayer)
      } else {
        setActiveDataLayer(currentLayer)
      }

      setURLParams(UrlParamsKey.Layer, currentLayer.id)

      if (!activeSublayerKey) {
        setURLParams(UrlParamsKey.Sublayer, '')
        return
      }

      setURLParams(UrlParamsKey.Sublayer, activeSublayerKey)
    } else {
      setActiveDataLayer(dataLayers[0])
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeLayerId, activeSublayerKey, hasAccess, isLoggedIn])

  return (
    <LayerContext.Provider
      value={{
        activeLayerId,
        setActiveLayerId,
        activeSublayerKey,
        setActiveSublayerKey,
        activeDataLayer,
        setActiveDataLayer,
        hasAccess,
      }}
    >
      {props.children}
    </LayerContext.Provider>
  )
}

export { LayerProvider, LayerContext }

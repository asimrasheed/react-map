// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { Dispatch, FC, useState, SetStateAction } from 'react'
import { useRouter } from 'next/router'
import { FeatureCollection } from 'geojson'
import MapStyleDark from '../data/mapstyle_osm_dark_v2.json'
import MapStyleLight from '../data/mapstyle_osm_grey_v2.json'
import { DataLayerProps } from '../data/DataLayers'

const mapThemes = {
  dark: MapStyleDark,
  light: MapStyleLight,
}

export interface MapContextProps {
  customFilter: Array<any>
  setCustomFilter: Dispatch<SetStateAction<MapContextProps['customFilter']>>
  searchGeometry: any
  currentMapLayers: DataLayerProps[]
  threeDimensional: boolean
  mapLoading: boolean
  mapTheme: typeof mapThemes.light | typeof mapThemes.dark
  relevantMapLayerId: string
  analyzeMode:
    | 'detail'
    | 'buildingDetail'
    | 'findings'
    | 'count'
    | 'findingsInspect'
    | 'findingsDrawer'
  setAnalyzeMode: Dispatch<SetStateAction<MapContextProps['analyzeMode']>>
  setRelevantMapLayerId: Dispatch<SetStateAction<string>>
  setSearchGeometry: Dispatch<SetStateAction<string>>
  setCurrentMapLayers: Dispatch<
    SetStateAction<MapContextProps['currentMapLayers']>
  >
  setMapLoading: Dispatch<SetStateAction<boolean>>
  toggleThreeDimensional: Dispatch<SetStateAction<void>>
  toggleMapTheme: Dispatch<SetStateAction<string>>
  featureCollection?: FeatureCollection | Record<string, never>
  selectedFeatureAndEvent: any
  setSelectedFeatureAndEvent: Dispatch<
    SetStateAction<MapContextProps['selectedFeatureAndEvent']>
  >
  reloadFindingsTiles: boolean
  setReloadFindingsTiles: Dispatch<
    SetStateAction<MapContextProps['reloadFindingsTiles']>
  >
}

const MapContext = React.createContext<MapContextProps>({} as MapContextProps)

// Our ContextAPI, aka MapContextProvider, tracks the state values of all variables that affect the map and its styling directly.
const MapProvider = ({ children }): JSX.Element => {
  const router = useRouter()
  const { asPath } = router || {}

  const [mapTheme, setMapTheme] = useState<
    typeof mapThemes.light | typeof mapThemes.dark
  >(mapThemes.light)
  const [searchGeometry, setSearchGeometry] = useState('')

  // TODO /0/40 werkt niet zo.
  const [threeDimensional, setthreeDimensional] = useState(
    asPath?.includes('/0/40')
  )
  const [mapLoading, setMapLoading] = useState(true)
  const [currentMapLayers, setCurrentMapLayers] = useState([])
  const [relevantMapLayerId, setRelevantMapLayerId] = useState('')
  const [selectedFeatureAndEvent, setSelectedFeatureAndEvent] = useState(null)
  const [customFilter, setCustomFilter] = useState()
  const [reloadFindingsTiles, setReloadFindingsTiles] = useState(false)

  const [analyzeMode, setAnalyzeMode] = useState()
  const toggleMapTheme = () => {
    setMapTheme(mapTheme === mapThemes.light ? mapThemes.dark : mapThemes.light)
  }

  const toggleThreeDimensional = () => {
    setthreeDimensional(!threeDimensional)
  }

  return (
    <MapContext.Provider
      value={{
        customFilter,
        setCustomFilter,
        searchGeometry,
        currentMapLayers,
        threeDimensional,
        mapLoading,
        mapTheme,
        setRelevantMapLayerId,
        relevantMapLayerId,
        setSearchGeometry,
        setCurrentMapLayers,
        setMapLoading,
        toggleThreeDimensional,
        toggleMapTheme,
        analyzeMode,
        setAnalyzeMode,
        selectedFeatureAndEvent,
        setSelectedFeatureAndEvent,
        reloadFindingsTiles,
        setReloadFindingsTiles,
      }}
    >
      {children}
    </MapContext.Provider>
  )
}

export { MapProvider, MapContext }

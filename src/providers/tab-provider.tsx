// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  Dispatch,
  SetStateAction,
  FC,
  createContext,
  useState,
  ReactNode,
} from 'react'

import { MenuPanelProps } from '../components/menu-panel/MenuPanel'
import { DataLayerProps } from '../data/DataLayers'

export interface TabProps {
  id: string
  title: string
  description?: string
  iconSlug?: string
}

export interface TabContextProps {
  activeTab: TabProps
  defaultTab: TabProps
  tabs: TabProps[]
  tabLayers: Partial<DataLayerProps>[]
  setActiveTab: Dispatch<SetStateAction<TabContextProps['activeTab']>>
  setTabLayers: Dispatch<SetStateAction<TabContextProps['tabLayers']>>
}

const TabContext = createContext<TabContextProps>({} as TabContextProps)

const TabProvider: FC<{
  tabs: TabContextProps['tabs']
  children: ReactNode
}> = ({ children, tabs }) => {
  const defaultTab: MenuPanelProps['activeTab'] = null

  const [tabLayers, setTabLayers] = useState<any>([])

  const [activeTab, setActiveTab] = useState(defaultTab)

  return (
    <TabContext.Provider
      value={{
        tabLayers,
        setTabLayers,
        activeTab,
        setActiveTab,
        tabs,
        defaultTab,
      }}
    >
      {children}
    </TabContext.Provider>
  )
}

export { TabContext, TabProvider }

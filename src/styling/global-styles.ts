// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
:root {
  --app-height: 100vh;
  --app-width: 100vw;
}

body {
  background-color: var(--colorBackground);
  color: var(--colorText);
  margin: 0;
  overflow: hidden;
  padding: 0;
}

html {
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale; 
  font-family: "Source Sans Pro", ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";

}

tr > * {
  font-weight: normal;
}

svg {
  flex-shrink: 0;
}

.ReactCollapse--collapse {
  transition: height 500ms;
}

/* error messages for design system inputs */
p[data-testid|='error'] {
  > svg {
    display: initial;
    vertical-align: bottom;
  }
}

/* HEADER */

.titel {
  margin: auto !important;
  display: inline-block;
}

#toaster-root {
  z-index: 1000;
}

.CountCount-active {
  grid-column: 2 / span 1;
  grid-row: 1;
  justify-self: end;
  margin: 1rem;
  z-index: 500;
  width: 577px;
  padding: 15px;
  border-radius: 4px;
  align-self: start;
  z-index: 500;
  height: auto;
  background-color: #f5f5f5;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24) !important;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  color: var(--colorPaletteGray600);
}

.CountCount-active h4 {
  margin: 0;
}

.CountCount-active img {
  width: 26px;
}

.CountCount-active p {
  font-size: 0.7em;
  margin: 0;
}

.CountCount-graph {
  grid-column: 2 / span 1;
  grid-row-start: 2;
  margin-top: var(--spacing03);
  z-index: 500;
  width: 30%;
  padding: 15px;
  align-self: start;
  z-index: 500;
  height: auto;
  background-color: #f5f5f5;
  box-shadow: 4px 4px 4px 2px rgba(0, 0, 0, 0.25);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
}

.dataItem {
  display: flex;
  align-items: baseline;
  flex-wrap: nowrap;
  margin: 0px;
  text-transform: uppercase;
}

.dataItem h3 {
  margin: 10px;
  margin-left: 0px;
  margin-bottom: 0px;
  font-size: 1.4em;
  color: #0b71a1;
}

.dataItemLabel {
  margin: 0;
  padding: 0;
}

.CountCount-active .download {
  text-transform: uppercase;
  text-decoration: none;
  font-size: 0.75rem;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
}
.CountCount-active .download:hover {
  color: var(--colorPaletteGray800);
}



/*FINDINGS DRAWER */

.findingsDrawer {
  [data-drawer='true'] {
    width: 560px;
    z-index: 500;
  }
}

/*LEGEND ON MAP */

.Legend {
  grid-column: 2 / span 1;
  grid-row: 6 / span 1;
  /* max-width: 450px; */
  justify-self: end;
  align-self: end;
  z-index: 2;
}

.Graph {
  grid-column: 2 / span 1;
  grid-row: 3 / span 1;
  justify-self: end;
  align-self: end;
  z-index: 2;
}

/* PANEL RIGHT SIDE */

.generalInfo {
  min-width: 100px;
  grid-column: 3 / span 1;
  grid-row: 1 / span 6;
  display: flex;
  flex-direction: column;
  justify-content: first baseline;
  background-color: var(--colorBackgroundAlt);
  padding: var(--spacing03);
  text-align: left;
  font-size: 0.9rem;
  overflow-y: auto;
  overflow-x: hidden;
}

.generalInfo h4 {
  margin-bottom: var(--spacing03);
  font-weight: normal;
}

.generalInfo .info {
  text-align: left;
}

.generalInfo .info h2 {
  margin-top: 0px;
}

.generalInfo .item {
  white-space: -moz-pre-wrap !important;
  /* Mozilla, since 1999 */
  white-space: -pre-wrap;
  /* Opera 4-6 */
  white-space: -o-pre-wrap;
  /* Opera 7 */
  white-space: pre-wrap;
  /* css-3 */
  word-wrap: break-word;
  /* Internet Explorer 5.5+ */
  white-space: -webkit-pre-wrap;
  /* Newer versions of Chrome/Safari*/
  /* word-break: break-all; */
  white-space: normal;
  width: 100%;
  margin-top: var(--spacing03);
  display: flex;
  align-items: center;
}

.generalInfo label {
  transform: scale(75%);
}

.generalInfo .item h4 {
  margin: 0px;
  font-size: 0.8rem;
}

.generalInfo .center {
  width: 100%;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: var(--spacing03);
  border-top: dotted grey 1px;
}

.generalInfo .labels {
  text-align: left;
  border-bottom: dotted grey 1px;
}

.generalInfo .labels .labelscheckbox {
  cursor: pointer;
  transition: 0.2s all ease-in-out;
}

.generalInfo .labels .labelscheckbox:hover {
  background-color: var(--colorBackgroundButtonSecondaryHover);
}

.generalInfo .labels .active {
  background-color: var(--colorBackgroundButtonSecondaryHover);
  font-weight: bold;
}

.generalInfo .center:last-child {
  border: none;
  margin-top: auto;
}

.collapsable {
  background-color: var(--colorPaletteGray300);
  z-index: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
}

.collapsable:hover {
  background-color: var(--colorPaletteGray600);
}

.notMobile {
  grid-column: 3 / span 1;
  grid-row: 2 / span 3;
}

.colofon {
  height: 50px;
  width: 80%;
  margin-top: var(--spacing09);
  margin-bottom: var(--spacing09);
}

/* TAB ITEMS  */

.tab-item {
  flex-grow: 1;
  text-transform: capitalize;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: var(--spacing01);
  cursor: pointer;
  color: var(--colorPaletteGray800);
  z-index: 1;
  background-color: var(--colorBrand1);
  transition: 250ms ease-in-out;
  border: 1px var(--colorWarningLight) solid;
}

.tab-item:focus {
  outline: 2px var(--colorFocus) dotted;
}

.tab-item:hover {
  color: var(--colorTextLinkHover);
}

.tab-item:hover .icon {
  fill: var(--colorTextLinkHover);
}

.tab-item h4 {
  margin: 0px;
  padding: 0;
  text-align: left;
}

.tab-item-active {
  background-color: rgba(255, 255, 255, 0.5) !important;
  color: var(--colorTextLabel);
}

.tab-item-active .icon {
  fill: var(--colorTextLabel);
}

.tab-item-active:hover {
  color: var(--colorTextLabel);
  cursor: initial;
}

.tab-item-active:hover .icon {
  fill: var(--colorTextLabel);
}

.icon {
  margin: 3px;
  fill: var(--colorPaletteGray700);
  font-size: 1.1rem;
}

/* MOBILE ITEMS NOT VISIBLE */

.upicon {
  display: none;
}

.small {
  display: none;
}

.mobile {
  display: none;
}

.underline {
  text-decoration: underline;
}



/* intermetdiate screen */

@media screen and (max-width: 1250px)  {
 
  .generalInfo {
    min-width: 100px;
    padding: var(--spacing03);
    overflow-wrap: break-word;
    word-wrap: break-word;
  }
  .generalInfo .item {
    text-align: center;
    flex-direction: column; grid-row: 6 / span 2;
    grid-column: 1 / span 1;
    align-items: center;
    margin: 0;
  }
  .generalInfo label {
    transform: scale(60%);
  }
  .generalInfo button {
    font-size: 0.8rem;
    padding: 0;
  } 
}

`

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '@commonground/design-system'
import { Queries, render, RenderResult } from '@testing-library/react'
import AppContext, { ConfigProps } from '../components/AppContext'
import '@testing-library/jest-dom/extend-expect'
import devConfig from '../../app.json'
import { LayerContext, LayerContextProps } from '../providers/layer-provider'
import { TabContext } from '../providers/tab-provider'

interface CustomRenderOptions {
  children?: JSX.Element
  configOverrides?: Partial<ConfigProps>
  layerContextOverrides?: Partial<LayerContextProps>
  // eslint-disable-next-line react/no-unused-prop-types
  testingLibraryOptions?: any
}

// based on https://testing-library.com/docs/react-testing-library/setup#custom-render
const AllTheProviders = ({
  children,
  configOverrides,
  layerContextOverrides,
}: CustomRenderOptions) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <AppContext.Provider
        value={{
          ...(devConfig as ConfigProps),
          ...configOverrides,
        }}
      >
        <TabContext.Provider
          value={
            {
              tabLayers: [],
              setTabLayers: jest.fn(),
              activeTab: '',
              setActiveTab: jest.fn(),
              tabs: [],
              defaultTab: {},
            } as any
          }
        >
          <LayerContext.Provider
            value={{
              activeLayerId: 'layer0',
              setActiveLayerId: jest.fn(),
              activeSublayerKey: '',
              setActiveSublayerKey: jest.fn(),
              hasAccess: () => true,
              activeDataLayer: {},
              setActiveDataLayer: jest.fn(),
              ...layerContextOverrides,
            }}
          >
            {children}
          </LayerContext.Provider>
        </TabContext.Provider>
      </AppContext.Provider>
    </ThemeProvider>
  )
}

const renderWithProviders = (
  ui: React.ReactElement,
  options?: CustomRenderOptions
): RenderResult<Queries, HTMLElement> =>
  render(ui, {
    wrapper: (props: any) => (
      <AllTheProviders
        {...props}
        configOverrides={options?.configOverrides}
        layerContextOverrides={options?.layerContextOverrides}
      />
    ),
    ...options?.testingLibraryOptions,
  })

// re-export everything
export * from '@testing-library/react'

// override render method
export { renderWithProviders }

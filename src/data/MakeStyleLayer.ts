// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FilterSpecification, LayerSpecification } from 'maplibre-gl'
import {
  makeCircleLayer,
  makeFillExtrusionLayer,
  makeFillLayer,
  makeLineLayer,
  makeRasterLayer,
  makeSymbolLayer,
} from '../utils/make-style-layer/get-type-layer'
import { StyleLayer } from './DataLayers'

const makeFilter = (
  styleLayer: Partial<StyleLayer>,
  invert?: boolean
): FilterSpecification => {
  const comparison = invert ? '==' : '!='
  const attr = styleLayer.attrName
  let filter: FilterSpecification

  if (!invert && styleLayer.expressionType !== 'match') {
    filter = ['all', ['has', attr]]
    const nodataValues = [] //'NULL', 'null', 'undefined', 'NA', 'NODATA', 'NaN'
    styleLayer.noDataValue &&
      styleLayer.noDataValue.forEach((element) => nodataValues.push(element))
    nodataValues.push('NA')

    nodataValues?.forEach((value) => {
      filter = (filter as any).concat([
        ['any', [comparison, ['get', attr], value]],
      ])
    })
  } else if (
    !invert &&
    styleLayer.expressionType === 'match' &&
    !styleLayer.userAttrName
  ) {
    filter = ['all', ['has', attr]]
    let filterpart: FilterSpecification = ['any']
    styleLayer.colorScale.domain().forEach((value) => {
      filterpart = (filterpart as any).concat([
        ['==', ['get', styleLayer.attrName], value],
      ])
    })
    filter = filter.concat([filterpart])
  } else if (
    !invert &&
    styleLayer.expressionType === 'match' &&
    styleLayer.userAttrName
  ) {
    filter = ['all', ['has', attr]]
  } else if (invert && !styleLayer.noDataValue) {
    filter = ['!has', attr]
  } else if (invert && styleLayer.noDataValue) {
    const nodataValues = ['NA'] //'NULL', 'null', 'undefined', 'NA', 'NODATA', 'NaN'
    filter = ['any']
    styleLayer.noDataValue &&
      styleLayer.noDataValue.forEach((element) => nodataValues.push(element))

    nodataValues.length > 0 &&
      nodataValues.forEach((value) => {
        filter = (filter as any).concat([[comparison, ['get', attr], value]])
      })
  }

  if (styleLayer.filter && styleLayer.filter === 'eancodes') {
    filter = (filter as any).concat([
      [
        '!=',
        ['length', ['to-string', ['get', 'pand_gas_ean_aansluitingen']]],
        0,
      ],
    ])
  } else if (styleLayer.filter && styleLayer.filter === 'bouwjaar') {
    // need for an always true filter
    filter = ['has', 'bouwjaar']
  } else if (styleLayer.filter) {
    filter = (filter as any).concat([styleLayer.filter])
  }

  return filter
}

export const makeMapLayer = (
  styleLayer: Partial<StyleLayer>,
  threeDimensional: string,
  name?: string
): Partial<LayerSpecification> => {
  let mapLayer = {}
  switch (styleLayer.geomType) {
    case 'line':
      mapLayer = makeLineLayer(styleLayer.id, styleLayer)
      break
    case 'raster':
      mapLayer = makeRasterLayer(styleLayer.id, styleLayer)
      break
    case 'circle':
      mapLayer = makeCircleLayer(styleLayer.id, styleLayer)
      break
    case 'symbol':
      mapLayer = makeSymbolLayer(styleLayer.id, styleLayer, name)
      break
    default:
      // fill = default
      mapLayer =
        threeDimensional && styleLayer.extrusionAttr
          ? makeFillExtrusionLayer(styleLayer.id, styleLayer)
          : makeFillLayer(styleLayer.id, styleLayer)
      break
  }

  // filters
  if (styleLayer.geomType !== 'raster') {
    mapLayer = {
      ...mapLayer,
      filter: makeFilter(styleLayer),
    }
  }
  // custom source-layer
  if (styleLayer['source-layer']) {
    mapLayer = {
      ...mapLayer,
      'source-layer': styleLayer['source-layer'],
    }
  }
  // console.log(mapLayer)
  return mapLayer
}

export const makeInvertLayer = (
  baseLayer: StyleLayer
): Partial<LayerSpecification> => {
  let mapLayer: LayerSpecification

  if (!baseLayer.geomType || baseLayer.geomType === 'fill') {
    mapLayer = makeFillLayer(baseLayer.id, baseLayer, true)
  }

  if (mapLayer.type === 'background') {
    return mapLayer
  }

  mapLayer = {
    ...mapLayer,
    filter: makeFilter(baseLayer, true),
  }
  return mapLayer
}

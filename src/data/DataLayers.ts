// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  ColorScale,
  createColorScale,
} from '../utils/make-style-layer/colorscales'
import { sourceSettings } from './TileSources'

export interface DataLayers {
  [layerName: string]: Partial<DataLayerProps>
}

type ThemeDook =
  | 'bedrijventerreinen'
  | 'bestemmingsplannen'
  | 'adressen-en-gebouwen'
  | 'kvk'
  | 'autobranche'
  | 'informatiebeeld-bedrijventerreinen'
  | 'bevindingen'
  | 'sociaaldook'
  | 'fail'

type ThemeDego = 'gebouw' | 'energie' | 'sociaal' | 'klimaat' | 'grond'
export type AuthRole =
  | 'no_role'
  | 'general_dego_data'
  | 'autobranche'
  | 'bedrijventerreinen'

export interface DataLayerProps {
  average?: number
  authLocked?: boolean
  authRoles: AuthRole[]
  desc: string
  drawType: 'findings' | 'count'
  filterAttrb?: string
  filterNames?: string[]
  filterValues?: number[] | string[] | [string, string[], string]
  highlight?: string
  id: string
  key: string
  lastUpdate: string
  name: string
  note: string
  referenceDate: string
  scale: string
  source: string
  styleLayers: Partial<StyleLayer>[]
  subLayers?: Partial<DataLayerProps>[]
  theme: ThemeDook | ThemeDego | ThemeDook[] | ThemeDego[]
  unit: string
  url?: string
}

export type StyleLayer = {
  'source-layer': string
  attrName: string
  noLegend: boolean
  color?: string
  colorScale: ColorScale
  caseArray: string[]
  detailModal: ({ properties, id }: PopupProps) => {
    title: string
    type?: 'findings' | 'building' | 'admin' | 'fail' | 'postcode'
    content?: Array<Array<string>>
    desc?: string
    url?: string
  }
  expressionType?: 'interpolate' | 'step' | 'match' | 'case' | 'bevindingen'
  extrusionAttr?: string
  hasExtrusion?: boolean
  filter?: string | [string, string[], string]
  firstLabel?: string
  geomType?: 'fill' | 'line' | 'circle' | 'raster' | 'symbol'
  id?: string
  image?: string | string[]
  insert?:
    | 'buildings'
    | 'labels_populated_places'
    | 'roads_casing'
    | 'road_labels'
  isRelevantLayer?: boolean
  lastLabel?: string
  legendStops: number[] | string[]
  noDataValue?: number[] | string[]
  opacity?: number
  popup: ({ properties }: PopupProps) => Popup
  rasterSource: string
  featureRequest?: string
  tileSource: {
    source: string
    'source-layer'?: string
    minZoom: number
    maxZoom: number
  }
  user: { userId: string; identity?: string }
  userAttrName: string
}

interface Popup {
  title: string
  content?: string | Record<string, any>
}

interface PopupProps {
  properties: Record<string, any>
  id: string
}

const energyLabelClasses = [
  '',
  'G (391 < kWh/m2/jaar)',
  'F (346 - 390 kWh/m2/jaar)',
  'E (301 - 345 kWh/m2/jaar)',
  'D (256 - 300 kWh/m2/jaar)',
  'C (196 - 255 kWh/m2/jaar)',
  'B (166 - 195 kWh/m2/jaar)',
  'A (111 - 165 kWh/m2/jaar)',
  'A+ (81 - 110 kWh/m2/jaar)',
  'A++ (51 - 80 kWh/m2/jaar)',
  'A+++ (1 - 50 kWh/m2/jaar)',
  'A++++ (< 0 kWh/m2/jaar)',
]

const staticBuildingsLayer: Partial<DataLayerProps> = {
  id: 'layer0',
  styleLayers: [
    {
      id: 'layer0',
      hasExtrusion: true,
    },
  ],
}

const noDataStyleLayer: Partial<DataLayerProps> = {
  id: 'noDataLayer',
}

const postalCodesStyleLayer: Partial<DataLayerProps> = {
  id: 'postalCodes',
}

const tooltip = (tooltipText, mainText) => {
  return `## ${tooltipText} ## ${mainText} ##`
}

export const dookDataLayers: Partial<DataLayerProps>[] = [
  //
  // no layer selected
  //
  staticBuildingsLayer,
  //
  // bedrijventerreinen
  //
  {
    id: 'layerDook',
    authRoles: ['no_role', 'bedrijventerreinen'],
    theme: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit OSM',
    desc: 'Deze kaartlaag laat de industriële en commerciële gebieden zien vanuit Open Street Map, een wereldkaart die vrij van licentie beschikbaar is. Getoonde datalaag: Landuse.',
    url: 'https://wiki.openstreetmap.org/wiki/Landcover',
    source: 'OSM',
    lastUpdate: '2021-04',
    referenceDate: '2021',
    unit: 'Type gebied',
    filterValues: ['industrial', 'commercial'],
    filterNames: ['Industrieel', 'Commercieel'],
    filterAttrb: 'class',
    styleLayers: [
      {
        geomType: 'fill',
        attrName: 'class',
        tileSource: sourceSettings.openmaptiles,
        legendStops: ['Industrieel', 'Commercieel'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02', '#AD8886'],
          ordinalDomain: ['industrial', 'commercial'],
        }),
        popup: ({ properties }) => ({
          title: `Landgebruik`,
          content:
            properties.class === 'industrial' ? 'Industrieel' : 'Commercieel',
        }),
      },
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'line',
        attrName: 'class',
        tileSource: sourceSettings.openmaptiles,
        legendStops: ['Industrieel', 'Commercieel'],
        expressionType: 'match',
        insert: 'labels_populated_places',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02', '#AD8886'],
          ordinalDomain: ['industrial', 'commercial'],
        }),
      },
    ],
  },
  {
    id: 'layerDook2',
    authRoles: ['no_role', 'bedrijventerreinen'],
    theme: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit topNL',
    desc: 'TOPNL-bestanden worden gebruikt als basis voor het bekijken en bewerken van geo-informatie en hebben als bron de Basisregistratie Topografie (BRT). Getoonde datalaag: Functioneel gebied.',
    url: 'https://www.pdok.nl/introductie/-/article/basisregistratie-topografie-brt-topnl',
    source: 'Kadaster, BRT',
    lastUpdate: '2021-04',
    referenceDate: '2021',
    unit: 'Gebied',
    styleLayers: [
      {
        geomType: 'fill',
        attrName: 'typefunctioneelgebied',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'functioneelgebied',
        legendStops: ['Bedrijventerrein'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['bedrijventerrein'],
        }),
        popup: ({ properties }): Popup => ({
          title: 'Functioneel gebied',
          content: properties.typefunctioneelgebied,
        }),
      },
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'line',
        attrName: 'typefunctioneelgebied',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'functioneelgebied',
        legendStops: ['Bedrijventerrein'],
        expressionType: 'match',
        insert: 'labels_populated_places',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['bedrijventerrein'],
        }),
      },
    ],
  },
  {
    id: 'layerDook3',
    authRoles: ['no_role', 'bedrijventerreinen'],
    theme: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit IBIS',
    desc: 'IBIS bevat beschrijvende data over de 3.800 Nederlandse bedrijventerreinen. IBIS wordt gepubliceerd door het interprovinciaal Overleg (IPO).',
    url: 'https://www.ibis-bedrijventerreinen.nl',
    source: 'IBIS',
    lastUpdate: '2021-04',
    referenceDate: '2021',
    unit: 'Gebied',
    styleLayers: [
      {
        geomType: 'fill',
        attrName: 'status',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'ibis_bedrijventerrein',
        legendStops: ['bedrijventerrein'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['GEACCORDEERD'],
        }),
        popup: ({ properties }): Popup => ({
          title: `IBIS Bedrijventerrein`,
          content: `${properties.plan_naam}`,
        }),
        detailModal: ({ properties }) => ({
          title: `IBIS Bedrijventerrein ${properties.plan_naam}`,
          desc: properties.kern_naam,
          content: [
            ['Oppervlakte', properties.ha_bruto + 'ha bruto'],
            ['Oppervlakte', properties.ha_netto + 'ha netto'],
            ['Status', properties.status],
            ['Millieuzone', properties.milieuzone],
          ],
        }),
      },
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'line',
        attrName: 'status',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'ibis_bedrijventerrein',
        legendStops: ['bedrijventerrein'],
        expressionType: 'match',
        insert: 'labels_populated_places',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['GEACCORDEERD'],
        }),
      },
    ],
  },
  //
  // KVK
  //

  {
    id: 'layerDook100',
    authRoles: ['bedrijventerreinen', 'autobranche'],
    theme: 'kvk',
    name: 'Handelsregister',
    desc: 'In deze kaartlaag kan de Handelsregisterinformatie worden gefilterd op SBI code. Door hieronder een branche te kiezen, verschijnt daardoor een meer specifieke onderverdeling van bedrijfsactiviteiten waar opnieuw op gefilterd kan worden. Het is op dit moment alleen nog mogelijk om op één SBI code tegelijk te filteren.',
    source: 'KVK',
    // scale: 'verblijfsobject',
    lastUpdate: '2021-04',
    referenceDate: '2021-04',
    // unit: 'SBI code- hoofdcategorie',
    drawType: 'count',
    filterAttrb: 'l1_code',
    filterNames: [
      'A. Landbouw, Bosbouw en Visserij',
      'B. Winning van Delfstoffen',
      'C. Industrie',
      'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
      'E. Winning en Distributie van Water',
      'F. Bouwnijverheid',
      'G. Groot- en Detailhandel; Reparatie Van Auto’s',
      'H. Vervoer en Opslag',
      'I. Logies-, Maaltijd- en Drankverstrekking',
      'J. Informatie en Communicatie',
      'K. Financiële Instellingen',
      'L. Verhuur van en Handel in Onroerend Goed',
      'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
      'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
      'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
      'P. Onderwijs',
      'Q. Gezondheids- en Welzijnszorg',
      'R. Cultuur, Sport en Recreatie',
      'S. Overige Dienstverlening',
      'T. Huishoudens als Werkgever',
      'U. Extraterritoriale Organisaties en Lichamen',
    ],
    filterValues: [
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
    ],
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        isRelevantLayer: true,
        attrName: 'l1_code',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        noLegend: true,
        popup: ({ properties }): Popup => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            [
              'Adres',
              `${properties.postcode} 
              ${properties.huisnr} 
              ${properties.huisletter || ''} 
              ${properties.toev_hsnr || ''} 
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
    ],
  },

  //
  // autobranche
  //
  {
    id: 'layer2316',
    theme: 'autobranche',
    authRoles: ['autobranche'],
    name: "Zicht op ondermijning: Branche analyse – Handel in en reparatie van auto's",
    desc: `Voor de branche ‘Handel in en reparatie van auto's’ wordt getoond in welke wijken hoge concentraties van bedrijfsvestigingen voorkomen ten opzichte van het landelijke beeld.`,
    url: 'https://www.zichtopondermijning.nl/',
    note: '',
    scale: 'Gemeente, wijk',
    unit: 'aantal',
    source: 'Zicht op ondermijning',
    lastUpdate: '2022-06-15',
    referenceDate: '2018',
    subLayers: [
      {
        name: 'Aantal branche vestigingen per 10.000 inwoners , concentratie 2018.',
        scale: 'Wijk',
        unit: 'absoluut',
        styleLayers: [
          {
            attrName: 'branche_vst_10k_inw_2018_concentratie',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [3, 15, 35, 50, 100],
            firstLabel: '< 3',
            lastLabel: '100 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [3, 50, 100],
            }),
            popup: ({
              properties: { branche_vst_10k_inw_2018_concentratie },
            }) => ({
              title: `${branche_vst_10k_inw_2018_concentratie}`,
            }),
          },
        ],
      },
      {
        name: 'Aantal branche vestigingen per 10.000 vestigingen , concentratie 2018.',
        scale: 'Wijk',
        unit: 'absoluut',
        styleLayers: [
          {
            attrName: 'branche_vst_1000vst_2018_concentratie',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [3, 15, 35, 50, 100],
            firstLabel: '< 3',
            lastLabel: '100 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [3, 50, 100],
            }),
            popup: ({
              properties: { branche_vst_1000vst_2018_concentratie },
            }) => ({
              title: `${branche_vst_1000vst_2018_concentratie}`,
            }),
          },
        ],
      },
      {
        name: 'Ontwikkeling aantal branch vestigingen per 10.000 inwoners,absoluut, 2015-2018',
        scale: 'Wijk',
        unit: '%',
        styleLayers: [
          {
            attrName: 'branche_vst_10k_inw_2015_2018_abs',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [-5, 0, 5],
            firstLabel: '< -5 ',
            lastLabel: '5 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              domain: [-5, 0, 5],
              type: 'diverging',
              color: 'blue',
            }),
            popup: ({ properties: { branche_vst_10k_inw_2015_2018_abs } }) => ({
              title: `${branche_vst_10k_inw_2015_2018_abs}`,
            }),
          },
        ],
      },
      {
        name: 'Ontwikkeling aantal branch vestigingen per 10.000 inwoners, relatief , 2015-2018',
        scale: 'Wijk',
        unit: '%',
        styleLayers: [
          {
            attrName: 'branche_vst_10k_inw_2015_2018_pct',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [-13, 0, 20],
            firstLabel: '< -13',
            lastLabel: '20 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              domain: [-13, 0, 20],
              type: 'diverging',
              color: 'blue',
            }),
            popup: ({ properties: { branche_vst_10k_inw_2015_2018_pct } }) => ({
              title: `${branche_vst_10k_inw_2015_2018_pct}%`,
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layerDook111',
    authRoles: ['autobranche'],
    theme: 'autobranche',
    name: 'Ondernemingen in de autobranche (volgens SBI code KVK)',
    desc: 'Inschrijvingen uit het Handelsregister gefilterd op SBI branchecodes 45 en 71 (ondernemingen in de autobranche). ',
    url: 'https://wiki.openstreetmap.org/wiki/Housenumber',
    source: 'KVK',
    lastUpdate: '2022-06',
    referenceDate: '2021',
    scale: 'verblijfsobject',
    unit: 'SBI code',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        attrName: 'l2_code',
        tileSource: sourceSettings.kvk2,
        isRelevantLayer: true,
        geomType: 'circle',
        legendStops: [
          "Handel in en reparatie van auto's, motorfietsen en aanhangers",
          "Verhuur en lease van auto's, consumentenartikelen, machines en overige roerende goederen",
        ],
        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#FF7F00', '#ffeda0', '#cc4c02'],
          ordinalDomain: ['45', '77'],
        }),
        popup: ({ properties }): Popup => ({
          title: `SBI code: ${properties.hoofdact}.`,
          content: `${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          title: `SBI Code`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            [
              'Adres',
              `${properties.postcode} 
              ${properties.huisnr} 
              ${properties.huisletter || ''} 
              ${properties.toev_hsnr || ''} 
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: 'hn_1x45',
      },
    ],
  },
  {
    id: 'layerDook112',
    authRoles: ['autobranche'],
    theme: 'autobranche',
    name: 'RDW met RDW erkenning',
    desc: 'Overzicht van de door RDW erkende bedrijven, op adres koppeld aan verblijfsobjecten en staanplaatsen in de BAG. De Match score geeft aan hoe nauwkeurig de koppeling is met de BAG, waar 0 fouten een 100% match betekend met een adres in de BAG. Ook het type erkenning wordt weergegeven.',
    url: 'https://opendata.rdw.nl/browse?category=Erkende+bedrijven&provenance=official',
    source: 'RDW',
    lastUpdate: '2022-06',
    referenceDate: '2022-06',
    scale: 'match score',
    unit: 'BAG adres koppelingsscore',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        isRelevantLayer: true,
        attrName: 'match_score',
        tileSource: sourceSettings.rdw,
        geomType: 'circle',
        legendStops: [0, 10],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#0b71a1', 'black'],
          domain: [0, 10],
        }),
        popup: ({ properties }): Popup => ({
          title: `RDW bedrijf`,
          content: `${properties.naam_bedrijf} - ${properties.gevelnaam}`,
        }),
        detailModal: ({ properties, id }) => ({
          title: 'RDW erkend bedrijf',
          content: [
            ['Bedrijf', properties.naam_bedrijf],
            ['Gevelnaam', properties.gevelnaam],
            ['RDW Adres', properties.rdw_toevoeging],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${
                  properties.huisnummertoevoeging
                    ? properties.huisnummertoevoeging
                    : ''
                }
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: 'gevelnaam',
      },
    ],
  },

  {
    id: 'layerDook113',
    authRoles: ['autobranche'],
    theme: 'autobranche',
    name: 'Ondernemingen met BOVAG-lidmaatschap',
    desc: 'Overzicht van bedrijven met een BOVAG lidmaatschap, op adres gekoppeld aan verblijfsobjecten en staanplaatsen in de BAG. De Match score geeft aan hoe nauwkeurig de koppeling is met de BAG, waar 0 fouten een 100% match betekend met een adres in de BAG.',
    url: 'https://www.bovag.nl/',
    source: 'BOVAG',
    lastUpdate: '2022-06',
    referenceDate: '2022-06',
    scale: 'match score',
    unit: 'BAG adres koppelingsscore',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        attrName: 'match_score',
        isRelevantLayer: true,
        tileSource: sourceSettings.bovag,
        geomType: 'circle',
        legendStops: [0, 10],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#0b71a1', 'black'],
          domain: [0, 10],
        }),
        popup: ({ properties }): Popup => ({
          title: `BOVAG bedrijf - ${properties.name}`,
          content: `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
        }),
        detailModal: ({ properties }) => ({
          title: 'BOVAG bedrijf',
          url: `https://www.bovag.nl/leden/${
            properties.name.charAt(properties.name.length - 1) === '.'
              ? properties.name
                  .replaceAll(' &', '')
                  .replace(/\s/g, '-')
                  .replaceAll("'", '-')
                  .replaceAll('.', '-')
                  .slice(0, -1)
              : properties.name
                  .replaceAll(' &', '')
                  .replace(/\s/g, '-')
                  .replaceAll("'", '-')
                  .replaceAll('.', '-')
          }`,
          urlTitle: 'Bovag website',
          content: [
            ['Bedrijf', properties.name],
            [
              'BOVAG Adres',
              `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
            ],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${properties.bag_toevoeging ? properties.bag_toevoeging : ''}
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            ['Verblijfsobject id', properties.vid.toString().padStart(16, '0')],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: 'name',
      },
    ],
  },
  {
    id: 'layerDook114',
    authRoles: ['autobranche'],
    theme: 'autobranche',
    name: 'Download basisdataset ten behoeve van het informatiebeeld',
    desc: 'Maak hier een download van de basisdataset zoals beschreven in Informatiefase 1 van de Handreiking Informatiebeeld Autobranche voor gemeenten. Hierin is data vanuit de BAG, het HR (KVK), RDW erkenningen en BOVAG lidmaatschappen samengebracht.',
    url: 'https://bagviewer.kadaster.nl/',
    source: 'BAG, RDW, BOVAG, KVK',
    lastUpdate: '2022-05',
    referenceDate: '2021',
    unit: 'Verblijfsobject',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: [
          'verblijfsobject',
          'standplaats',
          'ligplaats',
          'bovag',
          'rdw',
        ],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#fc8d62', '#39870c', '#f02b41'],
          ordinalDomain: ['vid', 'sid', 'lid', 'r'],
        }),
        opacity: 0.6,
        popup: ({ properties }): Popup => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}`,
        }),
      },
      {
        attrName: 'match_score',
        tileSource: sourceSettings.bovag,
        geomType: 'circle',
        color: '#39870c',
      },
      {
        attrName: 'match_score',
        tileSource: sourceSettings.rdw,
        geomType: 'circle',
        color: '#f02b41',
      },
    ],
  },

  //
  // vastgoed
  //
  {
    id: 'layerDook1',
    theme: 'adressen-en-gebouwen',
    authRoles: ['no_role', 'autobranche', 'bedrijventerreinen'],
    name: 'Adressen (verblijfsobjecten, ligplaatsen, standplaatsen)',
    desc: 'Alle adressen in Nederland uit de BAG, onderverdeeld in verblijfsobjecten, ligplaatsen en standplaatsen.',
    url: 'https://bagviewer.kadaster.nl/',
    source: 'BAG',
    lastUpdate: '2022-05',
    referenceDate: '2021',
    unit: 'Verblijfsobjecten, ligplaatsen en standplaatsen',
    drawType: 'count',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: ['verblijfsobject', 'standplaats', 'ligplaats'],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#fc8d62'],
          ordinalDomain: ['vid', 'sid', 'lid'],
        }),
        popup: ({ properties }): Popup => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}`,
        }),

        detailModal: ({ properties, id }) => ({
          title: 'Details Verblijfsobject',
          url: `https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html#?searchQuery=${id
            .toString()
            .padStart(16, '0')}`,
          urlTitle: 'BAG viewer',
          type: 'admin',
          content: [
            properties.openbareruimtenaam
              ? ['openbareruimtenaam', properties.openbareruimtenaam]
              : null,
            properties.huisnummer
              ? ['huisnummer', properties.huisnummer]
              : null,
            properties.huisletter
              ? ['huisletter', properties.huisletter]
              : null,
            properties.huisnumertoevoeging
              ? ['huisnumertoevoeging', properties.huisnumertoevoeging]
              : null,
            id && ['Nummeraanduiding id ', id],
            properties.vid && [
              ' verbrlijfobjectId',
              properties.vid.toString().padStart(16, '0'),
            ],
          ],
        }),
      },
    ],
  },
  {
    id: 'layerDook45',
    authRoles: ['no_role', 'autobranche', 'bedrijventerreinen'],
    theme: 'adressen-en-gebouwen',
    name: 'Oppervlakte verblijfsobject',
    desc: 'Deze kaartlaag biedt inzicht in de oppervlakte in m2 van een verblijfsobject.',
    source: 'BAG, oppervlakte',
    scale: 'Pand',
    unit: 'Bouwjaar',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    url: 'https://bagviewer.kadaster.nl',

    styleLayers: [
      {
        expressionType: 'interpolate',
        attrName: 'pand_area',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [17, 53, 88, 123, 500],
        firstLabel: '< 17',
        lastLabel: '> 500',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'log',
          colorRange: ['#fff7bc', '#fec44f', '#662506', '#000000'],
          domain: [17, 123, 1000, 200000],
        }),
        popup: ({ properties: { pand_area } }) => ({
          title: `Oppervlakte ${pand_area} m2`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
            properties.woonfunctie_score >= 0
              ? [
                  'woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
            properties.pand_area
              ? ['Oppervlakte', properties.pand_area + ' m2']
              : null,
          ],
        }),
      },
    ],
  },

  {
    id: 'layerDook4',
    authRoles: ['no_role', 'autobranche', 'bedrijventerreinen'],
    theme: 'adressen-en-gebouwen',
    name: 'Verblijfsobjecten met woonfunctie',
    desc: 'Een van de mogelijke gebruiksdoelen van een verblijfsobject is de woonfunctie. De procentuele score geeft aan welk deel van het verblijfsobject "wonen" als gebruiksdoel heeft.',
    note: '100 = wonen , 0 = niet wonen',
    source: 'BAG, gebruiksdoel woonfunctie',
    scale: 'Pand',
    unit: 'Percentage woonfunctie',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    url: 'https://bagviewer.kadaster.nl',

    styleLayers: [
      {
        attrName: 'woonfunctie_score',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [0, 20, 100],
        expressionType: 'interpolate',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#ce9e39', '#B19967', '#949494'],
          domain: [0, 20, 100],
        }),
        popup: ({ properties }): Popup => ({
          title: `Woonfunctie score: ${properties.woonfunctie_score}%`,
          content:
            properties.woonfunctie_score === 0
              ? 'Geen woonfunctie'
              : properties.woonfunctie_score === 100
              ? 'Alleen woonfunctie'
              : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
            properties.woonfunctie_score >= 0
              ? [
                  'woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
            properties.pand_area
              ? ['Oppervlakte', properties.pand_area + ' m2']
              : null,
          ],
        }),
      },
    ],
  },

  {
    id: 'layerDook44',
    authRoles: ['no_role', 'autobranche', 'bedrijventerreinen'],
    theme: 'adressen-en-gebouwen',
    name: 'Bouwjaar verblijfsobject',
    desc: 'Deze kaart geeft informatie over het oorsprongelijke bouwjaar van verblijfsobjecten.',
    source: 'BAG, oorspronkelijke bouwjaar',
    scale: 'Pand',
    unit: 'Bouwjaar',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    styleLayers: [
      {
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [1900, 1930, 1945, 1960, 1990, 1998, 2023],
        noDataValue: [1050, 0],
        expressionType: 'interpolate',
        firstLabel: 'voor 1900',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c', '#F1E7AA'],
          domain: [1500, 1900, 1945, 1995, 2015],
        }),
        popup: ({ properties }): Popup => ({
          title: `Bouwjaar: ${properties.bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
            properties.woonfunctie_score
              ? [
                  'woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
          ],
        }),
      },
    ],
    drawType: 'count',
  },

  //
  // adressen-en-gebouwen
  //
  {
    id: 'layerDook00',
    authRoles: ['no_role'],
    theme: 'bestemmingsplannen',
    name: 'Bestemmingsplannen',
    desc: 'Iedere locatie heeft een enkelbestemming en kan meerdere dubbelbestemmingen hebben. In de enkelbestemming staat beschreven of er gebouwd mag worden en wat voor bebouwing dat mag zijn. De enkelbestemming beschrijft voor welke functie de grond bestemd is (bijvoorbeeld woningen of bedrijven).',
    source: 'Selectie uit ruimtelijkeplannen.nl',
    lastUpdate: '2021-04',
    referenceDate: '2021',
    url: 'https://www.ruimtelijkeplannen.nl/',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        rasterSource: 'bedrijvenBestemming',
        geomType: 'raster',
        opacity: 0.5,
        image: '/images/legenda_bedrijventerrein.png',
        featureRequest:
          "https://geodata.nationaalgeoregister.nl/plu/ows?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&STYLES=&LAYERS=Enkelbestemming&QUERY_LAYERS=Enkelbestemming&FORMAT=image/png&INFO_FORMAT=application/json&cql_filter=bestemmingshoofdgroep IN ('bedrijf','bedrijventerrein', 'kantoor', 'detailhandel' )",
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `${properties.bestemmingshoofdgroep}`,
          content: [
            properties.bestemmingshoofdgroep
              ? ['Hoofdgroep', properties.bestemmingshoofdgroep]
              : null,
            properties.dossierid ? ['dossier id', properties.dossierid] : null,
            properties.datum ? ['Datum', properties.datum] : null,
            properties.naam ? ['Naam', properties.naam] : null,
            properties.hoofdfuncties
              ? ['hoofdfuncties', properties.hoofdfuncties]
              : null,
          ],
          url: properties.verwijzingnaarteksturl,
        }),
      },
    ],
  },
  {
    id: 'layerDook40',
    authRoles: ['no_role'],
    theme: 'bestemmingsplannen',
    name: 'Enkelbestemming (test)',
    desc: 'Iedere locatie heeft een enkelbestemming en kan meerdere dubbelbestemmingen hebben. In de enkelbestemming staat beschreven of er gebouwd mag worden en wat voor bebouwing dat mag zijn. De enkelbestemming beschrijft voor welke functie de grond bestemd is (bijvoorbeeld woningen of bedrijven).',
    lastUpdate: '2021-04',
    referenceDate: '',
    url: 'https://www.ruimtelijkeplannen.nl/',
    scale: 'WMS',
    unit: 'Enkelbestemming',
    source: 'ruimtelijkeplannen.nl',

    styleLayers: [
      {
        rasterSource: 'enkelbestemming',
        geomType: 'raster',
        opacity: 0.5,
        image:
          'https://geodata.nationaalgeoregister.nl/plu/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&layer=Enkelbestemming&transparent=true&width=20&height=20',
        featureRequest:
          'https://geodata.nationaalgeoregister.nl/plu/ows?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&STYLES=&LAYERS=Enkelbestemming&QUERY_LAYERS=Enkelbestemming&FORMAT=image/png&INFO_FORMAT=application/json',
        detailModal: ({ properties }) => ({
          title: `${properties.bestemmingshoofdgroep}`,
          content: [
            properties.bestemmingshoofdgroep
              ? ['Hoofdgroep', properties.bestemmingshoofdgroep]
              : null,
            properties.dossierid ? ['dossier id', properties.dossierid] : null,
            properties.datum ? ['Datum', properties.datum] : null,
            properties.naam ? ['Naam', properties.naam] : null,
          ],
          url: properties.verwijzingnaarteksturl,
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layerDook41',
    authRoles: ['no_role'],
    theme: 'bestemmingsplannen',
    name: 'Dubbelbestemming (test)',
    desc: 'Een dubbelbestemming in een bestemmingsplan is een extra bestemming met daarin opgenomen regels. Hier dient dan bij het bouwen rekening mee gehouden te worden. Voorbeelden van een extra bestemming kunnen zijn: waardevolle bomen in het gebied, de aanwezigheid van hoogspanningsleidingen of een beschermd stadsgezicht.',
    scale: 'WMS',
    unit: 'Dubbelbestemming',
    url: 'https://www.ruimtelijkeplannen.nl/',
    source: 'ruimtelijkeplannen.nl',
    lastUpdate: '2021-04',
    referenceDate: '',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        rasterSource: 'Dubbelbestemming',
        geomType: 'raster',
        image:
          'https://geodata.nationaalgeoregister.nl/plu/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=Dubbelbestemming&transparent=true',
        featureRequest:
          'https://geodata.nationaalgeoregister.nl/plu/ows?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&STYLES=&LAYERS=Dubbelbestemming&QUERY_LAYERS=Dubbelbestemming&FORMAT=image/png&INFO_FORMAT=application/json',

        detailModal: ({ properties }) => ({
          title: `${properties.bestemmingshoofdgroep}`,
          content: [
            properties.bestemmingshoofdgroep
              ? ['Hoofdgroep', properties.bestemmingshoofdgroep]
              : null,
            properties.dossierid ? ['dossier id', properties.dossierid] : null,
            properties.datum ? ['Datum', properties.datum] : null,
            properties.naam ? ['Naam', properties.naam] : null,
          ],
        }),
      },
    ],
  },
  //
  // sociaaldook
  //
]

export const degoDataLayers: Partial<DataLayerProps>[] = [
  //
  // no layer selected
  //
  staticBuildingsLayer,
  //
  // gebouw
  //
  {
    id: 'layer1',
    theme: 'gebouw',
    name: 'Bouwjaar',
    desc: 'Het bouwjaar geeft een eerste indicatie van wat de energetische kwaliteit en mogelijkheden van een object zijn, en hoe ingewikkeld dat technisch is. Aan nieuwere objecten hoeft technisch minder aangepast te worden om ze geschikt te maken voor aardgasvrij verwarmen, en dat maakt ze technisch doorgaans eenvoudiger dan oudere objecten. Oudere objecten (voor pakweg 1945) zijn technisch meestal erg lastig, omdat ze moeilijker (en dus duurder) na te isoleren zijn dan recentere objecten.',
    source: 'BAG, bouwjaar',
    scale: 'Pand',
    unit: 'Bouwjaar',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    drawType: 'count',
    url: 'https://bagviewer.kadaster.nl',
    styleLayers: [
      {
        expressionType: 'interpolate',
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [1900, 1930, 1945, 1960, 1990, 1998, 2023],
        noDataValue: [1005, 9999, 0],
        firstLabel: 'voor 1900',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c', '#F1E7AA'],
          domain: [1500, 1900, 1945, 1995, 2015],
        }),
        popup: ({ properties: { bouwjaar } }) => ({
          title: `Bouwjaar ${bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score_2022
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer1b',
    theme: 'gebouw',
    name: 'Bouwjaar op categorie',
    desc: 'Het bouwjaar geeft een eerste indicatie van wat de energetische kwaliteit en mogelijkheden van een gebouw zijn, en hoe ingewikkeld dat technisch is. Aan nieuwe objecten (na ongeveer 1995), hoeft technisch vaak relatief weinig aangepast te worden om ze geschikt te maken voor aardgasvrij verwarmen, en dat maakt ze technisch doorgaans relatief makkelijk. Oude objecten (voor pakweg 1945) zijn technisch meestal erg lastig, omdat ze moeilijker (en dus duurder) na te isoleren zijn dan objecten van na 1945.',
    source: 'BAG, bouwjaar',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    scale: 'Pand',
    unit: 'Bouwjaar',
    url: 'https://bagviewer.kadaster.nl',

    styleLayers: [
      {
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: ['voor 1900', '1900-1944', '1945-1994', 'na 1995'],
        noDataValue: [1005, 0],
        expressionType: 'step',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c'],
          domain: [1000, 1900, 1945, 1995],
        }),
        popup: ({ properties: { bouwjaar } }) => ({
          title: `Bouwjaar ${bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer2c',
    theme: 'gebouw',
    name: 'Energie Klasse - 2022 ',
    desc: `Energieklasse geeft een indicatie van hoe goed een object geisoleerd is, en daarmee ook een eerste indicatie van hoeveel er nog nageisoleerd moet worden om het object geschikt te maken voor een bepaalde warmte-oplossing. Het is betrouwbaarder dan het voorlopige energielabel, maar niet voor alle objecten beschikbaar. Op de kaart zijn alle labels in een pand gemiddeld, in de download/dataselectie zitten alle labels`,
    note: 'Van 32% van de panden is een energieklasse geregistreerd',
    source: 'RVO',
    scale: 'Pand',
    lastUpdate: '2022-09',
    referenceDate: '2022-07',
    unit: `Energielabel klassen -
    Warmtevraag kWh/(m2.jaar)`,
    filterAttrb: 'energieklasse_score',
    filterNames: [
      'A++++',
      'A+++',
      'A++',
      'A+',
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
    ],
    filterValues: [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    styleLayers: [
      {
        expressionType: 'match',
        tileSource: sourceSettings.bag,
        attrName: 'energieklasse_score',
        geomType: 'fill',
        legendStops: [
          'A++++ ( < 0 )',
          'A+++ ( 1 - 50 )',
          'A++ ( 51 - 80 )',
          'A+ ( 81 - 110 )',
          'A ( 111 - 165 )',
          'B ( 166 - 195 )',
          'C ( 196 - 255 )',
          'D ( 256 - 300 )',
          'E ( 301 - 345 )',
          'F ( 346 - 390 )',
          'G ( 391 < )',
        ],
        colorScale: createColorScale({
          type: 'linear',
          colorRange: [
            '#009342',
            '#009342',
            '#009342',
            '#009342',
            '#009342',
            '#1ba943',
            '#9ecf1b',
            '#f8f51c',
            '#f4b003',
            '#df6d14',
            '#db261d',
          ],
          domain: [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
        }),
        extrusionAttr: 'hoogte',

        popup: ({ properties }): Popup => ({
          title: 'Energielabel klasse:',
          content: energyLabelClasses[properties.energieklasse_score],
        }),

        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
    drawType: 'count',
  },

  {
    id: 'layer4',
    theme: 'gebouw',
    name: 'Woonfunctie',
    desc: `De Woonfunctie geeft een indicatie van het woning gebruik in een object. De score geeft het percentage voorkomende woonfuncties in alle verblijfsobjecten per pand aan. 100% betekend alleen woonfunctie en 0% geen woonfunctie. Alle tussenliggende waarde geven aan dat er meerdere verschillende functies voorkomen.`,
    note: '100 = wonen , 0 = niet wonen',
    source: 'BAG Woonfunctie per VBO',
    scale: 'Pand',
    unit: 'Percentage woonfunctie',
    lastUpdate: '2022-09',
    referenceDate: '2022-05',
    url: 'https://bagviewer.kadaster.nl',

    styleLayers: [
      {
        attrName: 'woonfunctie_score',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [0, 50, 100],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#ce9e39', '#B19967', '#949494'],
          domain: [0, 50, 100],
        }),
        extrusionAttr: 'hoogte',
        popup: ({ properties }): Popup => ({
          title:
            properties.woonfunctie_score === 0
              ? 'Geen woonfunctie'
              : properties.woonfunctie_score === 100
              ? 'Alleen woonfunctie'
              : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer3',
    theme: 'gebouw',
    name: 'Gemiddelde WOZ-waarde -2019',
    desc: `De WOZ-geeft een indicatie van de waarde van de objecten in een buurt.`,
    note: '33% van de data mist',
    source: 'CBS, Gemiddelde Woning waarde',
    scale: 'Gemeente, wijk, buurt',
    unit: 'x1000€',
    lastUpdate: '2021-04',
    referenceDate: '2019',
    url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84286NED/table?dl=6A019',
    styleLayers: [
      {
        opacity: 0.8,
        attrName: 'gemiddelde_woningwaarde',
        tileSource: sourceSettings.cbs,
        'source-layer': 'buurtcijfers2019',
        legendStops: [0, 37, 180, 240, 300, 500, 700, 1000],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'inferno',
          domain: [50, 240, 700],
        }),
        popup: ({ properties: { gemiddelde_woningwaarde, name, code } }) => ({
          title: `${name} - ${code}`,
          content: `${gemiddelde_woningwaarde}x1000€`,
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'protectedLayer4',
    theme: 'gebouw',
    name: 'Pilot Eigendoms gegevens',
    authLocked: true,
    desc: 'Eigendoms data Apeldoorn',
    source: 'Eigendomsdata Apeldoorn',
    unit: 'Type eigenaar',
    scale: 'Pand',
    lastUpdate: '2021-05-19',
    referenceDate: '2020-11',
    styleLayers: [
      {
        attrName: 'type_eigenaar',
        tileSource: sourceSettings.eigendom,
        'source-layer': 'eigenaren',
        legendStops: [
          'Grote part verhuurder',
          'Kleine part verhuurder',
          'Woningcorporatie',
          'Overig',
        ],
        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#1b9e77', '#d95f02', '#7570b3', '#e7298a'],
          ordinalDomain: [
            'Grote part verhuurder',
            'Kleine part verhuurder',
            'Woningcorporatie',
            'Overig',
          ],
        }),
        popup: ({
          properties: {
            naam_niet_natuurlijk_persoon,
            openbareruimtenaam,
            type_eigenaar,
            woning_type,
          },
        }) => ({
          title: `${type_eigenaar} - ${woning_type}`,
          content: `${naam_niet_natuurlijk_persoon} , ${openbareruimtenaam}`,
        }),
        extrusionAttr: 'hoogte',
      },
    ],
  },
  {
    id: 'layer206',
    authLocked: true,
    authRoles: ['general_dego_data'],
    theme: 'gebouw',
    name: 'Grote Eigenaren',
    desc: `Deze dataset geeft weer welk percentage van de verblijfsobjecten in bezit is van de 7 grootste eigenaren in een wijk of buurt. Een hoog percentage betekent dat een beperkt aantal eigenaren een grote invloed kan hebben op het aardgasvrij maken van de betreffende wijk of buurt. Door op een wijk te klikken verschijnt een pop-up die weergeeft wie de eigenaren zijn. Zakelijke eigenaren worden bij naam genoemd, particuliere eigenaren staan vanwege de AVG als ‘Particuliere eigenaar’ in de lijst.`,
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',
    source: 'CBS',
    lastUpdate: '2021-10',
    referenceDate: '2018',
    unit: '%',
    styleLayers: [
      {
        tileSource: sourceSettings.eigenaren,
        'source-layer': 'eigenarentop7',
        legendStops: [0, 15, 20, 50, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        attrName: 'grooteigenaarpct',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [0, 100],
        }),
        opacity: 0.8,

        popup: ({ properties: { grooteigenaarpct, name, code } }) => ({
          title: `${name} - ${code}`,
          content: `${grooteigenaarpct}%`,
        }),

        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `Top 7 grote eigenaren van ${properties.name}`,
          desc: properties.code,
          content: [
            properties.naam_1 === 'leeg'
              ? null
              : [`1. ${properties.naam_1}`, `${properties.aantal_vbo_1}`],
            properties.naam_2 === 'leeg'
              ? null
              : [`2. ${properties.naam_2}`, `${properties.aantal_vbo_2}`],
            properties.naam_3 === 'leeg'
              ? null
              : [`3. ${properties.naam_3}`, `${properties.aantal_vbo_3}`],
            properties.naam_4 === 'leeg'
              ? null
              : [`4. ${properties.naam_4}`, `${properties.aantal_vbo_4}`],
            properties.naam_5 === 'leeg'
              ? null
              : [`5. ${properties.naam_5}`, `${properties.aantal_vbo_5}`],
            properties.naam_6 === 'leeg'
              ? null
              : [`6. ${properties.naam_6}`, `${properties.aantal_vbo_6}`],
            properties.naam_7 === 'leeg'
              ? null
              : [`7. ${properties.naam_7}`, `${properties.aantal_vbo_7}`],

            ['Totaal aantal verblijfsobjecten', `${properties.aantal_vbo}`],
            [
              'Percentage verblijfsobjecten in bezit grote eigenaren',
              `${properties.grooteigenaarpct} %`,
            ],
          ],
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  //
  // energie
  //
  {
    id: 'layer21',
    theme: 'energie',
    name: 'Gasverbruik 2020',
    desc: `Het gasverbruik geeft aan hoeveel aardgas een object nu verbruikt. Daarmee kun je - indien gewenst-  bepalen wat de CO2-reductie is als dit object overgaat naar een aardgasvrije warmtevoorziening.`,
    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites',
    average: 1511,
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    url: 'https://data.overheid.nl/datasets?sort=score%20desc%2C%20sys_modified%20desc&search=kleinverbruik&facet_theme%5B0%5D=http%3A//standaarden.overheid.nl/owms/terms/Natuur_en_milieu%7Chttp%3A//standaarden.overheid.nl/owms/terms/Energie',
    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde m3/jaar',
    styleLayers: [
      //  {
      //   legendStops: [0, 750, 1250, 1700, 2500, 3000, 5000],
      //   expressionType: 'interpolate',
      //   attrName: 'gasm3_2020',
      //   tileSource: 'postcode',
      //   'source-layer': 'kv_pc6group_2021',
      //   colorScale: createColorScale({
      //     type: 'diverging',
      //     color: 'red',
      //     domain: [0, 1535, 3400],
      //   }),
      //   opacity: 0.3,
      //  },
      {
        legendStops: [0, 750, 1210, 1700, 3500, 5000],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        lastLabel: '< 5000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 1511, 3400],
        }),
        popup: ({ properties: { p6_gasm3 } }) => ({
          title: `${p6_gasm3} m3/jaar`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer21a',
    theme: 'energie',
    authLocked: true,
    name: 'Gasverbruik 2020 gefilterd op gasaansluiting',
    desc: `Toon het gasverbruik van een postcode, maar alleen wanneer een pand daadwerkelijk een gasaansluiting heeft
	  (en dus bijdraagt aan het gasverbruik van het geheel) volgens het EDSN.
	!! LET OP !!. bij geen aansluiting kan er een groot verbruik aansluiting zijn en/of
	  problemen met toevoegingen. Het EAN code boek volgt helaas niet de BAG.
	  `,
    note: 'Van ~7 miljoen panden zijn EAN codes gevonden',
    source: 'EDSN',

    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde m3/jaar',
    styleLayers: [
      {
        legendStops: [0, 750, 1210, 1700, 3500, 5000],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3',
        tileSource: sourceSettings.bag,
        filter: 'eancodes',
        extrusionAttr: 'hoogte',
        lastLabel: '< 5000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 1511, 3400],
        }),
        popup: ({ properties: { p6_gasm3 } }) => ({
          title: `${p6_gasm3} m3/jaar`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer22',
    theme: 'energie',
    name: 'Elektriciteitsverbruik 2020',
    desc: `Het elektriciteitsverbruik geeft aan hoeveel elektriciteit een object nu verbruikt. Let op: het elektriciteitsverbruik hangt vooral af van gebruikerseigenschappen en gebruikersgedrag, en veel minder van gebouweigenschappen. Denk aan elektrische auto's, gebruik van wasmachines en  wasdrogers, etc.`,
    note: `Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen.

    Vanaf 2019 wordt de zelf opgewekte energie bij het energieverbruik opgeteld i.t.t. de vorige jaren, en zijn de waardes dus hoger!`,
    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde kWh',
    average: 4239,
    styleLayers: [
      {
        expressionType: 'interpolate',
        legendStops: [0, 750, 2000, 3000, 4000, 5000, 6000, 7000, 10000],
        attrName: 'p6_kwh',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        lastLabel: '< 10000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'red',
          domain: [0, 4239, 10000],
        }),
        popup: ({ properties: { p6_kwh } }) => ({
          title: `${p6_kwh} kWh`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer23',
    theme: 'energie',
    name: 'Gasverbruik per m2 2020',
    desc: `Het gasverbruik per vierkante meter pandbeslag (oppervlak zonder verdiepingen) geeft een indicatie van de relatieve energiezuinigheid van panden. Een hoog gasverbruik kan immers veroorzaakt worden door een slechte isolatie, maar ook door de grootte van een pand of omdat er bedrijvigheid is die veel gas nodig heeft. Als het gasverbruik per m2 pandoppervlak laag is, wordt er dus relatief efficient verwarmd, ondanks het hoge gasverbruik.`,
    note: '3% van de data mist',
    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites, gecombineerd met BAG oppervlakte',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde m3 gas per m2 bodembeslag',
    average: 7.2,
    styleLayers: [
      {
        legendStops: [0, 6, 7, 9, 16, 20],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3_per_m2',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',

        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 7.2, 17],
        }),
        popup: ({ properties: { p6_gasm3_per_m2 } }) => ({
          title: `${p6_gasm3_per_m2} m3/m2/jaar`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer24',
    theme: 'energie',
    name: 'Gasverbruik per m3 2020',
    desc: `Het gasverbruik per kubieke meter object geeft een indicatie van de energiezuinigheid van een object. Een hoog gasverbruik kan immers veroorzaakt worden door slechte isolatie, maar ook doordat een object erg groot is.

    De hoogte van een pand komt uit de BAG3D van TU-delft met behulp van laser metingen. Helaas is niet van elk pand
    een hoogte beschikbaar. Samen met het feit dan kleinverbruik gegevens over meerdere panden gaat zitten er grote
    gaten in de kaart. De kaart lijkt overeen te komen met de m2 laag maar in hoogbouw zijn toch aanzienlijke verschillen te zien.`,
    note: '24% van de data mist (of volume van een pand, of het gasverbruik ervan)',

    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites, gecombineerd met BAG oppervlakte maal BAG3D van TU-Delft',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',

    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde m3 gas per m3 object',
    average: 1.2,

    styleLayers: [
      {
        legendStops: [0, 1, 1.2, 1.4, 3],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3_per_m3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',

        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 1.2, 3],
        }),
        popup: ({ properties: { p6_gasm3_per_m3 } }) => ({
          title: `${p6_gasm3_per_m3} m3 gas per m3 object per jaar`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],

    highlight: 'postcode',
  },
  {
    id: 'layer25',
    theme: 'energie',
    name: 'Percentage geleverd vanaf het elektriciteitsnet',
    desc: `Deze indicator geeft weer hoeveel procent van de elektriciteit die in een postcodegebied gebruikt wordt, vanaf het elektriciteitsnet geleverd wordt. Dit percentage is lager naarmate er meer PV panelen in een gebied liggen. Een laag percentage levering vanaf het elektriciteitsnet kan dan ook een indicatie zijn dat mensen welwillend tegenover de energietransitie staan, omdat het een beeld geeft van hoeveel PV-panelen mensen op hun huizen hebben gelegd. Hierbij zijn uiteraard een paar kanttekeningen te maken: het geldt niet bij nieuwbouwwijken waar de panelen al bij de bouw op het dak zijn gelegd of als een woningcorporatie op eigen initiatief PV-panelen heeft neergelegd. Het  geldt wel voor bestaande bouw met veel koopwoningen.`,
    note: 'Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen',

    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'per postcode 6 gebied',
    unit: 'Gemiddelde %',

    styleLayers: [
      {
        legendStops: [0, 25, 50, 75, 80, 90, 100],
        expressionType: 'interpolate',
        attrName: 'p6_kwh_leveringsrichting',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'red',
          domain: [78, 20],
        }),
        popup: ({ properties: { p6_kwh_leveringsrichting } }) => ({
          title: `${p6_kwh_leveringsrichting} %`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer26',
    theme: 'energie',
    name: 'Aantal gasaansluitingen in een postcode gebied 2020  ',
    desc: `Een object zonder aardgasaansluiting hoef je niet meer van het gas
    te halen. Dit getal zegt dus wat over het aantal aansluitingen in het PC6 gebied.
    Bij hoogbouw of in grote samengevoegde postcode 6 gebieden kan het
    over veel aansluitingen gaan. Energie kleinverbruik gegevens worden aangeleverd
    van -> tot postcode en kunnen resulteren in geografisch niet handige of grote gebieden.

    Gebieden kunnen een hoog gemiddeld gasverbuik hebben maar eigenlijk maar een paar
    aansluitingen.`,
    note: 'Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen.',
    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'per postcode 6 gebied',
    unit: 'Aantal gasaansluitingen',
    average: 27,
    styleLayers: [
      {
        legendStops: [0, 16, 30, 50, 100, 200],
        expressionType: 'interpolate',
        attrName: 'p6_gas_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [5, 27, 100],
        }),
        popup: ({ properties: { p6_gas_aansluitingen } }) => ({
          title: `${p6_gas_aansluitingen} aansluitingen in postcode gebied`,
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer27',
    theme: 'energie',
    authLocked: true,
    name: 'Aantal gasaansluitingen per pand - EAN codes',
    desc: `Het aantal EAN codes is een indicatie voor het aantal gas aansluitingen binnen een object. !! LET OP !!. 1) Het gaan om kleinverbruik aansluitingen, bij 0 aansluitingen kan een grootverbruik aansluiting zijn.
    2) Het eancode boek volgt niet de BAG en het kan fout gaan bij toevoegingen en meerdere panden.`,
    note: 'Van ~7 miljoen panden zijn EAN codes gevonden',
    source: 'EDSN',
    lastUpdate: '2022-09-01',
    referenceDate: '2021-10',
    scale: 'per pand',
    unit: 'Aantal aansluitingen',

    styleLayers: [
      {
        legendStops: [1, 2, 4, 6, 12, 24, 30, 50],
        expressionType: 'interpolate',
        attrName: 'pand_gas_ean_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'red',
          domain: [1, 12],
        }),
        popup: ({ properties: { pand_gas_ean_aansluitingen } }) => ({
          title: `${pand_gas_ean_aansluitingen}`,
        }),
        detailModal: ({ properties }) => ({
          title: '',
          desc: ' ',
          type: 'building',
          content: [
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
          ],
        }),
      },
    ],
    drawType: 'count',
  },
  {
    id: 'layer28',
    theme: 'energie',
    authLocked: true,
    name: 'Wel of geen gasaansluiting - volgens EANcode boek',
    desc: `Het aantal EAN codes is een indicatie voor het aantal gasaansluitingen binnen een object.`,
    note: 'Van ~7 miljoen panden zijn EAN codes gevonden',
    source: 'EDSN',
    lastUpdate: '2022-09-01',
    referenceDate: '2021-10',
    scale: 'Pand',
    unit: 'Gasaansluiting aanwezig',
    styleLayers: [
      {
        expressionType: 'case',
        legendStops: ['wel', 'geen'],
        attrName: 'pand_gas_ean_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        filter: 'bouwjaar',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#F1E7AA', '#395065'],
          ordinalDomain: ['0', '1'],
        }),
        popup: ({ properties: { pand_gas_ean_aansluitingen } }) => ({
          title:
            pand_gas_ean_aansluitingen > 0
              ? 'Heeft minimaal 1 gasaansluiting'
              : 'Heeft geen gasaansluiting',
        }),
        detailModal: ({ properties }) => ({
          title: ``,
          desc: ' ',
          type: 'building',
          content: [
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
          ],
        }),
      },
    ],
  },
  {
    id: 'layer29',
    theme: 'energie',
    name: 'Vermoedelijk kookgas',
    authLocked: true,
    desc: `Waar is er vermoedelijk kookgas ?`,
    note: `Geen gas = 0 m3 \n
      kookgas 1-100 m3 \n
      huishouden 100 - 6000 \n
      Van ~7 miljoen panden zijn EAN codes gevonden`,
    source:
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites',
    lastUpdate: '2022-09',
    referenceDate: '2021-01-01',
    scale: 'Gemiddelde per postcode 6 gebied',
    unit: 'Categorie',

    styleLayers: [
      {
        legendStops: ['Geen verbruik', 'Kookgas', 'Regulier'],
        expressionType: 'step',
        attrName: 'p6_gasm3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#66c2a5', '#fc8d62', '#395065'],
          domain: [0, 1, 100],
        }),
        popup: ({ properties: { p6_gasm3 } }) => ({
          title:
            p6_gasm3 <= 0
              ? 'Geen verbruik'
              : p6_gasm3 > 100
              ? 'Regulier verbruik'
              : 'Kookgas',
        }),
        detailModal: ({ properties }) => ({
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          type: 'postcode',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_kwh_leveringsrichting >= 0 && [
              'Percentage geleverd vanaf het elektriciteitsnet',
              `${properties.p6_kwh_leveringsrichting} %`,
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
            properties.p6_gasm3 <= 0
              ? ['Vermoedelijk kookgas', 'Geen verbruik']
              : properties.p6_gasm3 > 100
              ? ['Vermoedelijk kookgas', 'Regulier verbruik']
              : ['Vermoedelijk kookgas', 'Kookgas'],
          ],
        }),
      },
      postalCodesStyleLayer,
    ],
    highlight: 'postcode',
  },
  {
    id: 'layer30',
    theme: 'energie',
    name: 'Buurten met kookgasaansluitingen, 2020',
    desc: `In een beperkt aantal buurten in Nederland komt kookgas voor. Dat betekent dat woningen zijn aangesloten op een warmtenet, maar wel een aardgasaansluiting hebben voor hun kooktoestel (gaskookplaat en/of gasoven). Die woningen zijn technisch eenvoudig aardgasvrij te maken.

    In deze dataset wordt weergegeven in welke buurten meer dan 20% van de woningen een kookgasaansluiting heeft. Deze buurten zijn te vinden in de gemeenten Alkmaar, Almelo, Amsterdam, Bergen op Zoom, Breda, Delft, Heerlen, Helmond, Leiden, Leiderdorp, Tilburg, Utrecht en Westervoort.

    In verband met AVG kunnen we niet weergeven welke exacte objecten kookgas hebben, alleen welk percentage van de woningen in een buurt kookgas heeft. `,
    source: 'Woningen met kookgasaansluitingen, 2020 (cbs.nl)',
    lastUpdate: '2022-01',
    referenceDate: '2020-01-01',
    scale: 'Buurten met kookgas',
    unit: '%',
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/47/woningen-met-kookgasaansluitingen-2020',
    styleLayers: [
      {
        opacity: 0.5,
        legendStops: [20, 100],
        attrName: 'percentage_kookgaswoningen',
        tileSource: sourceSettings.cbs,
        'source-layer': 'kookgasaansluitingen2020',
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'red',
          domain: [10, 90],
        }),
        popup: ({ properties: { percentage_kookgaswoningen } }) => ({
          title: percentage_kookgaswoningen + ' %',
        }),
      },
    ],
  },
  {
    id: 'layer31',
    theme: 'energie',
    name: 'Gemiddeld verbruik per buurt, uitgesplitst naar huur en koop',
    desc: `Binnen een wijk kunnen er grote verschillen zitten tussen de energieverbruiken van verschillende segmenten (denk aan huur vs koop, woningtype, etc). Deze kaartlagen tonen daarom een aantal uitsplitsingen van het energieverbruik op buurtniveau. Als er grote verschillen in energieverbruik zijn tussen verschillende segmenten, loont het de moeite om na te gaan waar die verschillen vandaan komen: zijn de woningen in een bepaald segment groter of kleiner dan gemiddeld, van een ander type (bv rijtjeshuis vs appartement), of is de isolatiegraad anders? `,
    source: 'CBS 2019',
    lastUpdate: '2022-06',
    referenceDate: '2019',
    subLayers: [
      {
        name: 'Gemiddeld gasverbruik huurwoning',
        scale: 'per gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        average: 1217,
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_huurwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1700, 2500, 3500],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1217, 3190],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_huurwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gemiddeld gasverbruik Koopwoning',
        scale: 'per gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        average: 1570,
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_gasverbruikkoopwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1700, 2500, 3500],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1570, 2940],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruikkoopwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gemiddeld elektriciteitsgebruik huurwoning',
        scale: 'per gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        average: 2444,
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_elektriciteitsverbruik_huurwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 6860],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 2444, 4000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_huurwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gemiddeld elektriciteitsgebruik koopwoning',
        scale: 'per gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        average: 3454,
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_elektriciteitsverbruikkoopwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 1080, 2000, 3000, 4000, 7830],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [1080, 3454, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruikkoopwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gemiddeld elektriciteitsverbruik totaal',
        scale: 'per gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        average: 3212,
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_totaal',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer32',
    theme: 'energie',
    name: 'Gemiddeld verbruik per buurt, uitgesplitst naar type woning',
    desc: `Binnen een wijk kunnen er grote verschillen zitten tussen de energieverbruiken van verschillende segmenten (denk aan huur vs koop, woningtype, etc). Deze kaartlagen tonen daarom een aantal uitsplitsingen van het energieverbruik op buurtniveau. Als er grote verschillen in energieverbruik zijn tussen verschillende segmenten, loont het de moeite om na te gaan waar die verschillen vandaan komen: zijn de woningen in een bepaald segment groter of kleiner dan gemiddeld, van een ander type (bv rijtjeshuis vs appartement), of is de isolatiegraad anders? `,
    source: 'CBS 2019',
    lastUpdate: '2022-06',
    referenceDate: '2019',
    subLayers: [
      {
        name: 'Gasverbruik appartement',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_appartement',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_appartement + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'Gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'Elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gasverbruik tussenwoning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_tussenwoning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_tussenwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gasverbruik hoekwoning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_hoekwoning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_hoekwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },

      {
        name: 'Gasverbruik 2 onder 1 kap woning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_2_onder_1_kap_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_gasverbruik_2_onder_1_kap_woning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Gasverbruik vrijstaande woning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde m2',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_vrijstaande_woning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_gasverbruik_vrijstaande_woning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },

      {
        name: 'Elektriciteitsverbruik appartement',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_appartement',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_appartement +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },

      {
        name: 'Elektriciteitsverbruik tussenwoning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_tussenwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_tussenwoning +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Elektriciteitsverbruik hoekwoning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_hoekwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_hoekwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Elektriciteitsverbruik 2 onder 1 kap woning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gem_elektriciteitsverbruik_2_onder_1_kap_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning +
                ' kwh',
            }),

            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Elektriciteitsverbruik vrijstaande woning',
        scale: 'Gemeente, wijk, buurt',
        unit: 'Gemiddelde kwh',
        styleLayers: [
          {
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gem_elektriciteitsverbruik_vrijstaande_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gem_elektriciteitsverbruik_vrijstaande_woning +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              type: 'admin',
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
    ],
  },
  //
  // sociaal
  //
  {
    id: 'layer100',
    theme: 'sociaal',
    name: 'Percentage huishoudens die behoren tot de landelijke 20% huishoudens met het hoogste inkomen',
    desc: `Het gemiddelde inkomen zegt iets over de bestedingsruimte die mensen hebben, en geeft daarmee een eerste indicatie van de financiële gevolgen van de energietransitie voor deze mensen. Een lager gemiddeld inkomen betekent vaak minder bestedingsruimte, maar betekent ook dat een stijging van de (fossiele) energieprijzen meer impact heeft.Een hoger gemiddeld inkomen betekent dat de mensen die graag iets willen doen aan de energietransitie, daar waarschijnlijk ook de financiële ruimte voor hebben. Moet uiteraard samen met andere indicatoren bekeken worden, met name of er sprake is van huur of koop. `,
    note: '86% van de data mist',

    source: 'CBS',
    lastUpdate: '2021-04',
    referenceDate: '2018',

    scale: 'Gemeente, wijk, buurt',
    unit: '%',
    url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84286NED/table?dl=6A021',
    styleLayers: [
      {
        opacity: 0.8,
        legendStops: [0, 10, 20, 30, 50, 80, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        'source-layer': 'buurtcijfers2018',
        attrName: 'percentage_huishoudens_met_hoog_inkomen',
        tileSource: sourceSettings.cbs,
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [0, 60],
        }),
        popup: ({
          properties: { percentage_huishoudens_met_hoog_inkomen, name, code },
        }) => ({
          title: `${name} - ${code}`,
          content: `${percentage_huishoudens_met_hoog_inkomen}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8%< van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer101',
    theme: 'sociaal',
    name: 'Percentage huishoudens die behoren tot de landelijke 40% huishoudens met het laagste inkomen',
    desc: `Het percentage lage inkomens geeft een eerste indicatie van de aanwezigheid van armoedeproblematiek in een wijk. Moet bekeken worden met andere indicatoren, bij voorkeur % inkomen naar aardgas, omdat dan zichtbaar wordt waar mensen in de knel dreigen te komen door hoge gasrekeningen.`,
    note: '38% van de data mist',
    source: 'CBS',
    lastUpdate: '2021-04',
    referenceDate: '2018',

    scale: 'Gemeente, wijk, buurt',
    unit: '%',
    url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84286NED/table?dl=6A021',

    styleLayers: [
      {
        opacity: 0.8,
        legendStops: [0, 10, 20, 30, 40, 50, 80, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        'source-layer': 'buurtcijfers2018',
        attrName: 'percentage_huishoudens_met_laag_inkomen',
        tileSource: sourceSettings.cbs,
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [10, 80],
        }),
        popup: ({
          properties: { percentage_huishoudens_met_laag_inkomen, name, code },
        }) => ({
          title: `${name} - ${code}`,
          content: `${percentage_huishoudens_met_laag_inkomen}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8%< van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer203',
    theme: 'sociaal',
    name: 'Laag inkomen, hoog gasverbruik',
    desc: `Deze indicator geeft het percentage huishoudens in een buurt weer dat een laag inkomen heeft en een hoog gasverbruik. Een huishouden wordt meegeteld als het in de laagste 25% inkomens valt en tegelijkertijd een gasverbruik heeft dat in de hoogste 50% gasverbruiken valt. Meer weten over de details van deze indicator? Ga dan naar de externe bron.`,
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',

    source: 'CBS',
    lastUpdate: '2021-04',
    referenceDate: '2018',

    scale: 'Gemeente, wijk, buurt',
    unit: '%',

    styleLayers: [
      {
        opacity: 0.8,
        legendStops: [0, 3, 7, 10, 13, 18, 30],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        tileSource: sourceSettings.cbs,
        'source-layer': 'armoedebuurtcijfers2020',
        attrName: 'h50_i25_wpa',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [1, 18],
        }),
        popup: ({ properties: { h50_i25_wpa, name, code } }) => ({
          title: `${name} - ${code}`,
          content: `${h50_i25_wpa}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8% < van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer204',
    theme: 'sociaal',
    name: 'Hoge energiequote',
    desc: `Deze indicator geeft het percentage huishoudens in een buurt weer
    dat 8% of meer van hun inkomen besteedt aan de gas- en elektriciteitsrekening.
    Meer weten over de details van deze indicator? Ga dan naar de externe bron.`,
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',
    source: 'CBS',
    lastUpdate: '2021-04',
    referenceDate: '2018',

    scale: 'Gemeente, wijk, buurt',
    unit: '%',

    styleLayers: [
      {
        opacity: 0.8,
        legendStops: [0, 1, 5, 8, 15, 20, 50],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        tileSource: sourceSettings.cbs,
        'source-layer': 'armoedebuurtcijfers2020',
        attrName: 'er8p_i_wpa',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [1, 23],
        }),
        popup: ({ properties: { er8p_i_wpa, name, code } }) => ({
          title: `${name} - ${code}`,
          content: `${er8p_i_wpa}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8% < van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer205',
    theme: 'sociaal',
    name: 'Financiële draagkracht van huishoudens met een koopwoning',
    desc: `Deze dataset geeft aan hoeveel procent van de huishoudens in een wijk of buurt een financiële buffer heeft van minimaal het aangevinkte getal. `,
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',
    source: 'CBS',
    lastUpdate: '2021-10',
    referenceDate: '2018',
    subLayers: [
      {
        name: 'Minimaal 5000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            opacity: 0.8,
            legendStops: [0, 100],
            expressionType: 'interpolate',
            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm5000',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            popup: ({ properties: { m5000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 5000 €: ${m5000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen && [
                  'Aandeel koopwoningen',
                  `${properties.aandeel_koopwoningen}%`,
                ],
                properties.financieel_wpg && [
                  'Aandeel met gegevens',
                  `${properties.financieel_wpg}%`,
                ],
                properties.m5000 && [
                  'min vermogen van 5000€',
                  `${properties.m5000}%`,
                ],
                properties.m10000 && [
                  'min vermogen van 10.000€',
                  `${properties.m5000}%`,
                ],
                properties.m15000 && [
                  'min vermogen van 15.000€',
                  `${properties.m15000}%`,
                ],
                properties.m20000 && [
                  'min vermogen van 20.000€',
                  `${properties.m20000}%`,
                ],
                properties.m25000 && [
                  'min vermogen van 25.000€',
                  `${properties.m25000}%`,
                ],
                properties.m30000 && [
                  'min vermogen van 30.000€',
                  `${properties.m30000}%`,
                ],
                properties.m35000 && [
                  'min vermogen van 35.000€',
                  `${properties.m35000}%`,
                ],
                properties.m40000 && [
                  'min vermogen van 40.000€',
                  `${properties.m40000}%`,
                ],
                properties.m45000 && [
                  'min vermogen van 45.000€',
                  `${properties.m45000}%`,
                ],
                properties.m50000 && [
                  'min vermogen van 50.000€',
                  `${properties.m50000}%`,
                ],
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 10.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm10000',
            popup: ({ properties: { m10000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 10.000€: ${m10000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 15.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm15000',
            popup: ({ properties: { m15000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 15.000€ : ${m15000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 20.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm20000',
            popup: ({ properties: { m20000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 20.000€: ${m20000}%`,
            }),

            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 25.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm25000',
            popup: ({ properties: { m25000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 25.000€ : ${m25000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 30.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm30000',
            popup: ({ properties: { m30000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 30.000€ : ${m30000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 35.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm35000',
            popup: ({ properties: { m35000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 35.000€ : ${m35000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 40.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm40000',
            popup: ({ properties: { m40000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 40.000€ : ${m40000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 45.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm45000',
            popup: ({ properties: { m45000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 45.000€ : ${m45000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        name: 'Minimaal 50.000 euro',
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        styleLayers: [
          {
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm50000',
            popup: ({ properties: { m50000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 50.000€ : ${m50000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer207',
    theme: 'sociaal',
    name: 'Bereidheid Energietransitiemaatregelen',
    desc: `Om beter zicht te krijgen op de drijfveren van mensen om wel of niet te verduurzamen, heeft CBS onderzoek gedaan naar de verschillende factoren die meespelen bij de bereidheid van mensen om energietransitiemaatregelen te treffen aan hun woning. De Vereniging Nederlandse Gemeenten (VNG) heeft het CBS gevraagd om gegevens over bereidheid om energiemaatregelen te treffen uit een landelijke enquête te vertalen naar informatie op gemeente- en wijkniveau. Het gaat hierbij om sociale informatie die niet op basis van registraties in beeld gebracht kan worden, en met dit onderzoek via een combinatie van enquêtedata en statistische modellen geschat wordt. Meer informatie over de werkwijze is te vinden in het Excelbestand van CBS (Bereidheid energietransitiemaatregelen 2018 (cbs.nl)).

    Uit het onderzoek zijn verschillende factoren naar voren gekomen die een beeld geven van de bereidheid om maatregelen te treffen. Omdat het om een verkennend onderzoek ging, konden niet alle factoren worden uitgewerkt tot een landsdekkend beeld. Voor twee factoren is dit wel gedaan:`,
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2022/26/bereidheid-energietransitiemaatregelen-2018',
    authLocked: true,
    source: 'CBS',
    lastUpdate: '2022-11',
    referenceDate: '2018',
    unit: 'Bereidheid',
    subLayers: [
      {
        name: 'Bereid om te investeren in een zuinigere woning, mits dat terugverdient',
        desc: 'Een hoge score op deze indicator betekent dat mensen in deze wijk het belangrijk vinden om hun investering terug te verdienen. Het financiële aspect is daarmee dus een drijfveer. In wijken met een lage score vinden mensen het minder belangrijk om hun investering terug te verdienen (bijvoorbeeld omdat comfort een belangrijkere rol speelt bij de investeringsbeslissing)',
        unit: 'Score op de indicator',
        styleLayers: [
          {
            tileSource: sourceSettings.cbsenergiebereidheid,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'bereidmits',
            colorScale: createColorScale({
              type: 'ordinal',

              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({ properties: { bereidmits, naam, regio_code } }) => ({
              title: `${naam} - ${regio_code}`,
              content: `${bereidmits}`,
            }),

            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Bereidheid Energietransitiemaatregelen van ${properties.naam}`,
              desc: 'Mate waarin een indicator voor de houding over energiebesparing voorkomt',
              content: [
                [
                  'Persoon wil investeren in zuinigere woning, mits dat terugverdient',
                  `${properties.bereidmits}`,
                ],
                [
                  'Persoon wil niet investeren in zuinigere woning, weet niet hoe, vindt het te duur, en/of is er nog niet aan toegekomen',
                  `${properties.nietbereid}`,
                ],
              ],
            }),
          },
          staticBuildingsLayer.styleLayers[0],
        ],
      },
      {
        name: 'Om uiteenlopende redenen nog geen actie ondernomen',
        desc: 'Een hoge score op deze indicator betekent dat mensen in deze wijk nog weinig energiebesparende maatregelen hebben uitgevoerd. Daar kunnen verschillende redenen voor zijn, bijvoorbeeld omdat ze het niet kunnen betalen, niet weten wat de mogelijkheden zijn, vinden dat het niet genoeg besparing oplevert, of er simpelweg nog niet aan toegekomen zijn. Een lage score op deze indicator betekent dat er al relatief veel mensen in deze wijk iets hebben gedaan om hun woning te verduurzamen. ',
        unit: 'Score op de indicator',
        styleLayers: [
          {
            tileSource: sourceSettings.cbsenergiebereidheid,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'nietbereid',
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({ properties: { nietbereid, naam, regio_code } }) => ({
              title: `${naam} - ${regio_code}`,
              content: `${nietbereid}`,
            }),

            detailModal: ({ properties }) => ({
              type: 'admin',
              title: `Bereidheid Energietransitiemaatregelen van ${properties.naam}`,
              desc: 'Mate waarin een indicator voor de houding over energiebesparing voorkomt',
              content: [
                [
                  'Persoon wil investeren in zuinigere woning, mits dat terugverdient',
                  `${properties.bereidmits}`,
                ],
                [
                  'Persoon wil niet investeren in zuinigere woning, weet niet hoe, vindt het te duur, en/of is er nog niet aan toegekomen',
                  `${properties.nietbereid}`,
                ],
              ],
            }),
          },
          staticBuildingsLayer.styleLayers[0],
        ],
      },
    ],
  },

  //
  // climate
  //
  {
    id: 'layer103',
    theme: 'klimaat',
    name: 'Stedelijk Hitte Eiland effect',
    desc: "Deze kaart geeft het stedelijke hitte-eiland-effect weer. Dit is het temperatuurverschil tussen het stedelijke gebied en omliggende landelijke gebieden (gemiddeld over de zomer). Het stedelijk hitte-eiland effect is 's nachts het sterkst. Het zorgt er voor dat de luchttemperatuur 's nachts weinig daalt, waardoor het erg warm blijft en er nadelige gezondheidseffecten optreden. Omdat hittestress de leefbaarheid van een plek nadelig kan beïnvloeden, is het waardevol om hittestress mee te nemen als criterium bij de fasering van de wijken. De kaart is gemaakt mbv een model dat met jaargemiddelde temperaturen werkt, waardoor het niet geschikt is voor extreem warme zomerdagen of nachten.",
    url: 'https://www.klimaateffectatlas.nl/nl/',
    source: 'Klimaateffectatlas',
    scale: 'resolutie 100x100meter',
    lastUpdate: '2021-09',
    referenceDate: '2017-01-01',
    unit: 'Temperatuur',
    styleLayers: [
      {
        rasterSource: 'wms',
        geomType: 'raster',
        opacity: 0.8,
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=hitteeiland&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer104',
    theme: 'klimaat',
    name: 'Lokale gevoelstemperatuur',
    desc: 'De kaart laat de lokale gevoelstemperatuur zien op een extreem hete zomermiddag. Hoe warm mensen het hebben, ligt niet alleen aan de luchttemperatuur. Ook andere weersfactoren en de omgeving hebben invloed op de gevoelstemperatuur. Stedelijk gebied warmt vaak nog meer op door de bebouwing. Bij een hoge gevoelstemperatuur kunnen mensen last krijgen van hittestress. Hittestress zorgt voor vermindering van comfort, gezondheidsproblemen en kan zelfs leiden tot overlijden. De kaart helpt bij het nadenken over het verkoelend inrichten van de buitenruimte',
    url: 'https://www.klimaateffectatlas.nl/nl/hittekaart-gevoelstemperatuur',

    source: 'Klimaateffectatlas',
    scale: 'resolutie 2x2 meter',
    lastUpdate: '2021-09',
    referenceDate: '2018',
    unit: 'Temperatuur',

    styleLayers: [
      {
        rasterSource: 'wms2',
        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=hittekaart_gevoelstemp_huidig&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer105',
    theme: 'klimaat',
    name: 'Wateroverlast - Waterdiepte bij intense neerslag - 1:1000 jaar',
    desc: 'Deze kaart geeft een indicatie van de maximale waterdiepte die op een plek kan optreden als gevolg van kortdurende intense neerslag. Voor de modellering is een bui gebruikt van 140 mm in 2 uur. Onder het huidige klimaat komt deze bui circa 1 keer in de 1000 jaar voor. De kaart is ontwikkeld door Deltares in het kader van de voorlopige overstromingsrisicobeoordeling van de Europese Richtlijn Overstromingsrisico’s.',
    url: 'https://www.klimaateffectatlas.nl/nl/',
    source: 'Klimaateffectatlas',
    scale: 'resolutie 2x2 meter',
    lastUpdate: '2021-09',
    referenceDate: '2018',
    unit: 'Waterdiepte',

    styleLayers: [
      {
        rasterSource: 'wms3',
        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=waterdiepte_neerslag_140mm_2uur&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer106',
    theme: 'klimaat',
    name: 'Wateroverlast - Waterdiepte bij intense neerslag - 1:100 jaar',
    desc: 'Deze kaart geeft een indicatie van de maximale waterdiepte die op een plek kan optreden als gevolg van kortdurende intense neerslag. Voor de modellering is een bui gebruikt van 70 mm in 2 uur. Onder het huidige klimaat komt deze bui circa 1 keer in de 100 jaar voor. De kaart is ontwikkeld door Deltares in het kader van de voorlopige overstromingsrisicobeoordeling van de Europese Richtlijn Overstromingsrisico’s.',
    url: 'https://www.klimaateffectatlas.nl/nl/',
    source: 'Klimaateffectatlas',
    scale: 'resolutie 2x2 meter',
    lastUpdate: '2021-09',
    referenceDate: '2018',
    unit: 'Waterdiepte',

    styleLayers: [
      {
        rasterSource: 'wms4',
        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=waterdiepte_neerslag_70mm_2uur&TRANSPARENT=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer107',
    theme: 'klimaat',
    name: 'Geluidsoverlast (Lcum)',
    desc: 'Op deze kaart zie je hoeveel geluid verschillende bronnen samen veroorzaken. Het gaat hier om het gemiddelde geluidsniveau van wegverkeer, treinverkeer, vliegtuigen, industrie en windturbines. Ongewenst geluid kan hinder en gezondheidsklachten geven. Lden (Level Day-Evening-Night).',
    url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
    source: 'Atlas voor de Leefomgeving RIVM',
    lastUpdate: '2021-04',
    referenceDate: '',
    scale: 'resolutie 10 meter',
    unit: 'Geluid Lden',
    styleLayers: [
      {
        rasterSource: 'geluid',
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&transparent=true',
        featureRequest:
          'https://data.rivm.nl/geo/alo/ows?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&STYLES=&FORMAT=image/png&QUERY_LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&INFO_FORMAT=application/json',
        detailModal: ({ properties }) => ({
          type: 'admin',
          title: 'Geluid Lden',
          content: [
            properties.GRAY_INDEX
              ? ['Waarde', properties.GRAY_INDEX + ' dB']
              : null,
          ],
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer108',
    theme: 'klimaat',
    name: 'Fijnstof (PM10)',
    desc: 'Op deze kaart zie je hoeveel fijnstof er gemiddeld in de lucht zat in Nederland in 2019. Het gaat om PM10: deeltjes die kleiner zijn dan 10 micrometer. Hoe minder fijnstof hoe beter de luchtkwaliteit. ',
    url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
    source: 'Atlas voor de Leefomgeving RIVM',
    lastUpdate: '2021-04',
    referenceDate: '2019',
    scale: 'resolutie 25 meter',
    unit: 'Fijnstof',

    styleLayers: [
      {
        rasterSource: 'luchtkwaliteit1',
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_PM102019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer109',
    theme: 'klimaat',
    name: 'Fijnstof (PM2,5)',
    desc: 'Op deze kaart zie je hoeveel fijnstof er gemiddeld in de lucht zat in 2019. Het gaat om PM2,5: deeltjes die kleiner zijn dan 2,5 micrometer. Hoe minder fijnstof hoe beter de luchtkwaliteit.',
    url: 'https://geodata.rivm.nl/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20200715_gm_pm252018',
    source: 'Atlas voor de Leefomgeving RIVM',
    lastUpdate: '2021-04',
    referenceDate: '2019',
    scale: 'resolutie 25 meter',
    unit: 'Fijnstof',

    styleLayers: [
      {
        rasterSource: 'luchtkwaliteit2',
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_PM252019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer110',
    theme: 'klimaat',
    name: 'Stikstofdioxide (NO2)',
    desc: 'Op deze kaart zie je hoeveel stikstofdioxide er gemiddeld in de lucht zat in Nederland in 2019. Hoe minder stikstofdioxide hoe beter de luchtkwaliteit is.',
    url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
    source: 'Atlas voor de Leefomgeving RIVM',
    lastUpdate: '2021-04',
    referenceDate: '2019',
    scale: 'resolutie 25 meter',
    unit: 'NO2',

    styleLayers: [
      {
        rasterSource: 'luchtkwaliteit3',
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_NO22019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  //
  // ground
  //
  {
    id: 'layer111',
    theme: 'grond',
    name: 'Liander Elektriciteitsnetten',
    desc: 'Deze dataset bevat de liggingsgegevens van de elektriciteitsnetten binnen het werkgebied van Liander. Het betreft de laag- (LS), midden- (MS) en hoogspanningsnetten (HS), inclusief laagspanningskasten, middenspanningsruimtes (MSR) en stations. Er wordt gewerkt aan een landsdekkende kaart.',
    url: 'https://www.pdok.nl/introductie/-/article/liander-elektriciteitsnetten-1',
    source: 'Liander, via PDOK',
    lastUpdate: '2021-11-17',
    referenceDate: '2019',
    scale: 'wms',
    unit: '',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        rasterSource: 'lianderNetten',
        geomType: 'raster',
        image: [
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=hoogspanningskabels&STYLE=elektriciteitsnetten:hoogspanningskabels',
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=middenspanningkabels&STYLE=elektriciteitsnetten:middenspanningkabels',
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=laagspanningkabels&STYLE=elektriciteitsnetten:laagspanningkabels',
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=onderstations&STYLE=elektriciteitsnetten:onderstations',
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=middenspanningsinstallaties&STYLE=elektriciteitsnetten:middenspanningsinstallaties',
          'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?service=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=laagspanningsverdeelkasten&STYLE=elektriciteitsnetten:laagspanningsverdeelkasten',
        ],
      },
    ],
  },
  {
    id: 'layer112',
    theme: 'grond',
    name: 'Riolering',
    desc: 'Op deze kaart is de riolering weergegeven. Het gaat hierbij om verschillende aspecten van de riolering zoals verschillende soorten leidingen, putten en kolken. Heb je vragen over deze kaartlaag? Neem dan contact op met stichting Rioned via info@rioned.org',
    url: 'https://nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search?uuid=e30528cf-9964-4741-b0f3-5814c8b0ade4#/metadata/e30528cf-9964-4741-b0f3-5814c8b0ade4?tab=general',
    source: 'Stichting RIONED',
    lastUpdate: '2021-11-17',
    referenceDate: '2019',
    scale: 'wms',
    unit: '',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        rasterSource: 'rioned',
        geomType: 'raster',
        image: [
          'https://geodata.nationaalgeoregister.nl/rioned/gwsw/wms/v1_0?SERVICE=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=beheer_leiding&STYLE=gwsw:beheer_leiding',
          'https://geodata.nationaalgeoregister.nl/rioned/gwsw/wms/v1_0?SERVICE=WMS&language=dut&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&format=image/png&transparent=true&layer=beheer_put&Style=gwsw:beheer_put',
        ],
      },
    ],
  },
]

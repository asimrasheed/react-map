// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const dookTabs = [
  {
    id: 'bedrijventerreinen', // bedrijf
    title: 'Bedrijventerreinen',
    description:
      "Er is geen éénduidige definitie van wat een 'bedrijventerrein' is. De focus van de Datavoorziening Onregelmatigheden op de Kaart is op bestuurlijke handhaving van vergunningen op bedrijventerreinen.",
    iconSlug: '/images/categories/beoogd.svg',
  },
  {
    id: 'bestemmingsplannen', // beoogd
    title: 'Bestemmingsplannen',
    intro:
      'De ruimtelijke plannen worden als één beeld weergegeven zodat in één oogopslag duidelijk wordt welke ruimtelijke plannen er voor een bepaald gebied gemaakt zijn. Op Ruimtelijkeplannen.nl vindt u bestemmingsplannen, structuurvisies en algemene regels die gemaakt zijn door gemeentes, provincies en het Rijk. Dit is een directe kopie van deze bron.',
    iconSlug: '/images/categories/beoogd.svg',
  },
  {
    id: 'adressen-en-gebouwen', // gev_bag
    title: 'Adressen en gebouwen (BAG)',
    intro: 'Ontdek hier de eigenschappen van de bebouwde omgeving.',
    iconSlug: '/images/categories/vastgoed.svg',
  },
  {
    id: 'kvk',
    title: 'KVK Handelsregister data',
    intro:
      'Alle Handelsregister data van de Kamer Van Koophandel is beschikbaar van bedrijven geregistreerd binnen uw gemeente.',
    iconSlug: '/images/categories/sbi.svg',
  },
  {
    id: 'autobranche', // auto
    title: 'Informatiebeeld autobranche',
    intro:
      'De onderstaande datalagen zijn nodig voor het opstellen van de basisdataset en het doorlopen van Informatiefase 1 uit de Handreiking Informatiebeeld Autobranche voor gemeenten.',
    iconSlug: '/images/categories/car-line.svg',
  },
  {
    id: 'informatiebeeld-bedrijventerreinen', // sbi?
    title: 'Informatiebeeld bedrijventerreinen',
    intro:
      'De onderstaande datalagen zijn nodig voor het opstellen van de basisdataset uit de Handreiking Informatiebeeld Bedrijventerreinen voor gemeenten.',
    iconSlug: '/images/categories/sbi.svg',
  },
]

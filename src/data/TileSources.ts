// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const sourceSettings = {
  cbs: {
    source: 'cbs',
    'source-layer': 'buurtcijfers2018',
    minZoom: 8,
    maxZoom: 22,
  },
  cbsgrid: {
    source: 'cbsgrid',
    minZoom: 10,
    maxZoom: 22,
  },
  cbsenergiebereidheid: {
    source: 'cbsenergiebereidheid',
    'source-layer': 'bereidheid.energiemaatregelen_wijk',
    minZoom: 8,
    maxZoom: 22,
  },
  bag: {
    source: 'bag',
    'source-layer': 'panden2022',
    minZoom: 12,
    maxZoom: 22,
  },
  verblijfsobjecten: {
    source: 'verblijfsobjecten',
    'source-layer': 'bagpuntjes',
    minZoom: 12,
    maxZoom: 22,
  },
  openmaptiles: {
    source: 'openmaptiles',
    'source-layer': 'landuse',
    minZoom: 6,
    maxZoom: 22,
  },
  bedrijven: {
    source: 'bedrijven',
    minZoom: 6,
    maxZoom: 22,
  },
  kvk: {
    source: 'kvk',
    'source-layer': 'kvk.kvk_expand_sbi',
    minZoom: 6,
    maxZoom: 22,
  },
  kvk2: {
    source: 'kvk2',
    'source-layer': 'kvk.kvk_expand_sbi_gm0796',
    minZoom: 6,
    maxZoom: 22,
  },
  sbi: {
    source: 'sbi',
    'source-layer': 'sbicodes',
    minZoom: 6,
    maxZoom: 22,
  },
  eigendom: {
    source: 'eigendom',
    'source-layer': 'eigenaren',
    minZoom: 6,
    maxZoom: 22,
  },
  faillissementen: {
    source: 'faillissementen',
    'source-layer': 'cir.insolvency_locations_resolved_epoch',
    minZoom: 6,
    maxZoom: 22,
  },
  bevindingen: {
    source: 'bevindingen',
    minZoom: 6,
    maxZoom: 22,
  },
  eigenaren: {
    source: 'eigenaren',
    minZoom: 6,
    maxZoom: 22,
  },
  rdw: {
    source: 'rdw',
    'source-layer': 'bovag.rdw_bag_matched_202205',
    minZoom: 7,
    maxZoom: 22,
  },
  bovag: {
    source: 'bovag',
    'source-layer': 'bovag.garages202205v2',
    minZoom: 7,
    maxZoom: 22,
  },
}

export const sources = {
  bag: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/epanden_202209/epanden.json',
    minzoom: 12,
    maxzoom: 15,
  },
  verblijfsobjecten: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/bagpuntjes/puntjes.json',
    minzoom: 12,
    maxzoom: 15,
  },
  cbs: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/cbs_20220113/cbs.json',
    minzoom: 8,
  },
  cbsenergiebereidheid: {
    type: 'vector',
    tiles: [
      'https://tileserver.commondatafactory.nl/bereidheid.energiemaatregelen_wijk/{z}/{x}/{y}.pbf',
    ],
    minzoom: 8,
  },
  eigendom: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/apeldoorn_20211006/apeldoorn.json',
  },
  cbsgrid: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/cbs_grid_20210512/cbs_grid.json',
    minzoom: 10,
  },
  postcode: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/pc6kv2021/pc6.json',
    promoteId: 'group_id_2021',
  },
  bedrijven: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/bedrijventerrein_20210330/bedrijventerrein.json',
  },
  sbi: {
    type: 'vector',
    url: 'https://files.commondatafactory.nl/tiles/sbicodes_20210422/sbicodes.json',
  },
  kvk: {
    type: 'vector',
    tiles: ['http://localhost:7800/kvk.kvk_expand_sbi/{z}/{x}/{y}.pbf'],
  },
  kvk2: {
    type: 'vector',
    tiles: [
      process.env.NEXT_PUBLIC_ENV === 'prod'
        ? 'https://authorized.tiles.commondatafactory.nl/kvk.kvk_expand_sbi_{gm}/{z}/{x}/{y}.pbf'
        : process.env.NEXT_PUBLIC_ENV === 'acc'
        ? 'https://acc.authorized.tiles.commondatafactory.nl/kvk.kvk_expand_sbi_{gm}/{z}/{x}/{y}.pbf'
        : 'http://authorized.tiles.example.com/kvk.kvk_expand_sbi_{gm}/{z}/{x}/{y}.pbf',
    ],
  },
  faillissementen: {
    type: 'vector',
    tiles: [
      'https://tileserver.commondatafactory.nl/cir.insolvency_locations_resolved_epoch/{z}/{x}/{y}.pbf',
    ],
    promoteId: 'insolvency_id',
  },
  eigenaren: {
    type: 'vector',
    url:
      process.env.NEXT_PUBLIC_ENV === 'prod'
        ? 'https://auth.files.commondatafactory.nl/tiles/eigenaarstop7/eigenaarstop7.json'
        : 'https://acc.auth.files.commondatafactory.nl/tiles/eigenaarstop7/acc.eigenaarstop7.json',
  },
  rdw: {
    type: 'vector',
    tiles: [
      'https://tileserver.commondatafactory.nl/bovag.rdw_bag_matched_202205/{z}/{x}/{y}.pbf',
    ],
    minzoom: 7,
    maxzoom: 22,
  },
  bovag: {
    type: 'vector',
    tiles: [
      'https://tileserver.commondatafactory.nl/bovag.garages202205v2/{z}/{x}/{y}.pbf',
    ],
    minzoom: 7,
    maxzoom: 22,
  },
  wms: {
    type: 'raster',
    tiles: [
      'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=hitteeiland&STYLE=hitteeiland_r_hitte&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  wms2: {
    type: 'raster',
    tiles: [
      'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=hittekaart_gevoelstemp_huidig&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  wms3: {
    type: 'raster',
    tiles: [
      'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=waterdiepte_neerslag_140mm_2uur&STYLE=waterdiepte_neerslag_1-1000_r_wateroverlast&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  wms4: {
    type: 'raster',
    tiles: [
      'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=waterdiepte_neerslag_70mm_2uur&STYLE=waterdiepte_neerslag_1-100_r_wateroverlast&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  geluid: {
    type: 'raster',
    tiles: [
      'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  luchtkwaliteit1: {
    type: 'raster',
    tiles: [
      'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_PM102019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  luchtkwaliteit2: {
    type: 'raster',
    tiles: [
      'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_PM252019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
  },
  luchtkwaliteit3: {
    type: 'raster',
    tiles: [
      'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_NO22019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
  },
  enkelbestemming: {
    type: 'raster',
    tiles: [
      'https://geodata.nationaalgeoregister.nl/plu/wms?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=plu:Enkelbestemming&LAYERS=Enkelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
    minzoom: 14,
  },
  Dubbelbestemming: {
    type: 'raster',
    tiles: [
      'https://geodata.nationaalgeoregister.nl/plu/wms?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=plu:Dubbelbestemming&LAYERS=Dubbelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
    minzoom: 14,
  },
  bedrijvenBestemming: {
    type: 'raster',
    tiles: [
      "https://geodata.nationaalgeoregister.nl/plu/wms?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=plu:Enkelbestemming&LAYERS=Enkelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}&cql_filter=bestemmingshoofdgroep IN ('bedrijf','bedrijventerrein', 'kantoor' ,'detailhandel')",
    ],
    tileSize: 256,
    minzoom: 14,
  },
  lianderNetten: {
    type: 'raster',
    tiles: [
      'https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=' +
        'elektriciteitsnetten:hoogspanningskabels,' +
        'elektriciteitsnetten:middenspanningkabels,' +
        'elektriciteitsnetten:laagspanningkabels,' +
        'elektriciteitsnetten:laagspanningsverdeelkasten,' +
        'elektriciteitsnetten:middenspanningsinstallaties,' +
        'elektriciteitsnetten:onderstations' +
        '&LAYERS=' +
        'hoogspanningskabels,' +
        'middenspanningkabels,' +
        'laagspanningkabels,' +
        'laagspanningsverdeelkasten,' +
        'middenspanningsinstallaties,' +
        'onderstations' +
        '&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
    minzoom: 14,
  },
  rioned: {
    type: 'raster',
    tiles: [
      'https://geodata.nationaalgeoregister.nl/rioned/gwsw/wms/v1_0?&SERVICE=WMS&language=dut&VERSION=1.3.0&transparent=true&REQUEST=GetMap&FORMAT=image/png&' +
        'LAYERS=' +
        'beheer_leiding,' +
        'beheer_bouwwerk,' +
        'beheer_put,' +
        'beheer_pomp,' +
        'beheer_lozing' +
        '&STYLES=' +
        'gwsw:beheer_leiding,' +
        'gwsw:beheer_bouwwerk,' +
        'gwsw:beheer_put,' +
        'gwsw:beheer_pomp,' +
        'gwsw:beheer_lozing' +
        '&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
    ],
    tileSize: 256,
    minzoom: 13.6,
  },
  searchGeometry: {
    type: 'geojson',
    data: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            name: 'Null Island',
          },
          geometry: {
            type: 'Point',
            coordinates: [0, 0],
          },
        },
      ],
    },
  },
  bevindingen: {
    type: 'vector',
    promoteId: 'id',
    tiles: [
      process.env.NEXT_PUBLIC_ENV === 'prod'
        ? `https://tileserver.commondatafactory.nl/dook.finding_point/{z}/{x}/{y}.pbf?dt=${Date.now()}`
        : process.env.NEXT_PUBLIC_ENV === 'acc'
        ? `https://acc.tileserver.commondatafactory.nl/dook.finding_point/{z}/{x}/{y}.pbf?dt=${Date.now()}`
        : `http://localhost:7800/dook.finding_point/{z}/{x}/{y}.pbf?dt=${Date.now()}`,
    ],
  },
  luchtfotos: {
    type: 'raster',
    tileSize: 256,
    tiles: [
      'https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0?request=GetMap&service=wms&VERSION=1.3.0&TRANSPARENT=true&FORMAT=image/png&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}&Layers=Actueel_ortho25',
    ],
  },
}

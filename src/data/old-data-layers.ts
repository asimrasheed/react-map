// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

// {
//   id: 'layerDook323',
//   theme: 'informatiebeeld-bedrijventerreinen',
//   desc: 'De Standaard Bedrijfsindeling (SBI) is een hiërarchische indeling van economische activiteiten. Filter hier alle sbi codes op hoofdthema. De telling en visualisatie is op sub categorie. ',
//   name: 'Filter SBI codes op hoofdthema',
//   source: 'SBI codes uit RVO energielabels',
//   url: 'https://www.rvo.nl/onderwerpen/wetten-en-regels-gebouwen/energielabel-woningen',
//   scale: 'verblijfsobject',
//   lastUpdate: '2021-04',
//   referenceDate: '2021-04',
//   unit: 'SBI code- hoofdcategorie',
//   drawType: 'count',
//   filterAttrb: 'l1_code',
//   filterValues: [
//     'A',
//     'B',
//     'C',
//     'D',
//     'E',
//     'F',
//     'G',
//     'H',
//     'I',
//     'J',
//     'K',
//     'L',
//     'M',
//     'N',
//     'O',
//     'P',
//     'Q',
//     'R',
//     'S',
//     'T',
//     'U',
//   ],
//   filterNames: [
//     'A. Landbouw, Bosbouw en Visserij',
//     'B. Winning van Delfstoffen',
//     'C. Industrie',
//     'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
//     'E. Winning en Distributie van Water',
//     'F. Bouwnijverheid',
//     'G. Groot- en Detailhandel; Reparatie Van Auto’s',
//     'H. Vervoer en Opslag',
//     'I. Logies-, Maaltijd- en Drankverstrekking',
//     'J. Informatie en Communicatie',
//     'K. Financiële Instellingen',
//     'L. Verhuur van en Handel in Onroerend Goed',
//     'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
//     'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
//     'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
//     'P. Onderwijs',
//     'Q. Gezondheids- en Welzijnszorg',
//     'R. Cultuur, Sport en Recreatie',
//     'S. Overige Dienstverlening',
//     'T. Huishoudens als Werkgever',
//     'U. Extraterritoriale Organisaties en Lichamen',
//   ],

//   styleLayers: [
//     staticBuildingsLayer.styleLayers[0],
//     {
//       isRelevantLayer: true,
//       attrName: 'l2_code',
//       tileSource: sourceSettings.sbi,
//       geomType: 'circle',
//       legendStops: [
//         'A. Landbouw, jacht en dienstverlening voor de landbouw en jacht',
//         'A. Bosbouw, exploitatie van bossen en dienstverlening voor de bosbouw',
//         'A. Visserij en kweken van vis en schaaldieren',
//         'B. Winning van aardolie en aardgas',
//         'B. Winning van delfstoffen (geen olie en gas)',
//         'B. Dienstverlening voor de winning van delfstoffen',
//         'C. voedingsmiddelen',
//         'C. dranken',
//         'C. tabaksproducten',
//         'C. textiel',
//         'C. kleding',
//         'C. leer, lederwaren en schoenen',
//         'C. artikelen van hout, kurk, riet',
//         'C. papier, karton en papier- en kartonwaren',
//         'C. Drukkerijen, reproductie van opgenomen media',
//         'C. cokesovenproducten en aardolieverwerking',
//         'C. chemische producten',
//         'C. farmaceutische grondstoffen en producten',
//         'C. producten van rubber en kunststof',
//         'C. overige niet-metaalhoudende minerale producten',
//         'C. metalen in primaire vorm',
//         'C. producten van metaal (geen machines en apparaten)',
//         'C. computers en van elektronische en optische apparatuur',
//         'C. elektrische apparatuur',
//         'C. overige machines en apparaten',
//         "C. auto's, aanhangwagens en opleggers",
//         'C. overige transportmiddelen',
//         'C. meubels',
//         'C. overige goederen',
//         'C. Reparatie en installatie van machines en apparaten',
//         'D. Productie en distributie van en handel in elektriciteit, aardgas, stoom en gekoelde lucht',
//         'E. Winning en distributie van water',
//         'E. Afvalwaterinzameling en -behandeling',
//         'E. Afvalinzameling en –behandeling; voorbereiding tot recycling',
//         'E. Sanering en overig afvalbeheer',
//         'F. Algemene burgerlijke en utiliteitsbouw en projectontwikkeling',
//         'F. Grond-, water- en wegenbouw (geen grondverzet)',
//         'F. Gespecialiseerde werkzaamheden in de bouw',
//         "G. Handel in en reparatie van auto's, motorfietsen en aanhangers",
//         "G. Groothandel en handelsbemiddeling (niet in auto's en motorfietsen)",
//         "G. Detailhandel (niet in auto's)",
//         'H.Vervoer over land',
//         'H.Vervoer over water',
//         'H.Luchtvaart',
//         'H.Opslag en dienstverlening voor vervoer',
//         'H.Post en koeriers',
//         'I. Logiesverstrekking',
//         'I. Eet- en drinkgelegenheden',
//         'J. Uitgeverijen',
//         "J. Productie en distributie van films en televisieprogramma's; maken en uitgeven van geluidsopnamen",
//         "J. Verzorgen en uitzenden van radio- en televisieprogramma's",
//         'J. Telecommunicatie',
//         'J. Dienstverlenende activiteiten op het gebied van informatietechnologie',
//         'J. Dienstverlenende activiteiten op het gebied van informatie',
//         'K. Financiële instellingen (geen verzekeringen en pensioenfondsen)',
//         'K. Verzekeringen en pensioenfondsen (geen verplichte sociale verzekeringen)',
//         'K. Overige financiële dienstverlening',
//         'L. Verhuur van en handel in onroerend goed',
//         'M. Rechtskundige dienstverlening, accountancy, belastingadvisering en administratie',
//         'M. Holdings (geen financiële), concerndiensten binnen eigen concern en managementadvisering',
//         'M. Architecten, ingenieurs en technisch ontwerp en advies; keuring en controle',
//         'M. Speur- en ontwikkelingswerk',
//         'M. Reclame en marktonderzoek',
//         'M. Industrieel ontwerp en vormgeving, fotografie, vertaling en overige consultancy',
//         'M. Veterinaire dienstverlening',
//         "N. Verhuur en lease van auto's, consumentenartikelen, machines en overige roerende goederen",
//         'N. Arbeidsbemiddeling, uitzendbureaus en personeelsbeheer',
//         'N. Reisbemiddeling, reisorganisatie, toeristische informatie en reserveringsbureaus',
//         'N. Beveiliging en opsporing',
//         'N. Facility management, reiniging en landschapsverzorging',
//         'N. Overige zakelijke dienstverlening',
//         'O. Openbaar bestuur, overheidsdiensten en verplichte sociale verzekeringen',
//         'P. Onderwijs',
//         'Q. Gezondheidszorg',
//         'Q. Verpleging, verzorging en begeleiding met overnachting',
//         'Q. Maatschappelijke dienstverlening zonder overnachting',
//         'R. Kunst',
//         'R. Culturele uitleencentra, openbare archieven, musea, dieren- en plantentuinen, natuurbehoud',
//         'R. Loterijen en kansspelen',
//         'R. Sport en recreatie',
//         'S. Levensbeschouwelijke en politieke organisaties, belangen- en ideële organisaties, hobbyclubs',
//         'S. Reparatie van computers en consumentenartikelen',
//         'S. Wellness en overige dienstverlening; uitvaartbranche',
//         'T. Huishoudens als werkgever van huishoudelijk personeel',
//         'T. Niet-gedifferentieerde productie van goederen en diensten door particuliere huishoudens voor eigen gebruik',
//         'U. Extraterritoriale organisaties en lichamen',
//       ],
//       expressionType: 'match',
//       colorScale: createColorScale({
//         type: 'ordinal',
//         colorRange: [
//           '#b15928',
//           '#AB7326',
//           '#70401E',
//           '#a6d854',
//           '#33a02c',
//           '#1b9e77',
//           '#59a14f',
//           '#a6d854',
//           '#b2df8a',
//           '#33a02c',
//           '#1b9e77',
//           '#99d8c9',
//           '#66c2a4',
//           '#41ae76',
//           '#238b45',
//           '#006d2c',
//           '#00441b',
//           '#7bccc4',
//           '#4eb3d3',
//           '#0868ac',
//           '#084081',
//           '#67a9cf',
//           '#02818a',
//           '#016c59',
//           '#014636',
//           '#7FDB23',
//           '#b2df8a',
//           '#73824F',
//           '#33a02c',
//           '#043D00',
//           '#33a02c',
//           '#b2df8a',
//           '#73824F',
//           '#33a02c',
//           '#043D00',
//           '#b2df8a',
//           '#33a02c',
//           '#043D00',
//           '#FF7F00',
//           '#ffeda0',
//           '#cc4c02',
//           '#fc9272',
//           '#ED5A5F',
//           '#ef3b2c',
//           '#A50F15',
//           '#852226',
//           '#ffeda0',
//           '#cc4c02',
//           '#fb6a4a',
//           '#ef3b2c',
//           '#67000d',
//           '#a50f15',
//           '#e31a1c',
//           '#fb9a99',
//           '#fb9a99',
//           '#6a3d9a',
//           '#bc80bd',
//           '#6a3d9a',
//           '#bdbdbd',
//           '#969696',
//           '#737373',
//           '#525252',
//           '#252525',
//           '#000000',
//           '#000000',
//           '#000000',
//           '#252525',
//           '#525252',
//           '#737373',
//           '#969696',
//           '#bdbdbd',
//           '#e6ab02',
//           '#e6ab02',
//           '#e6ab02',
//           '#ffff33',
//           '#ffff99',
//           '#46bcce',
//           '#a6cee3',
//           '#0000ff',
//           '#377eb8',
//           '#a6cee3',
//           '#0000ff',
//           '#377eb8',
//           '#0000ff',
//           '#377eb8',
//           '#377eb8',
//         ],
//         ordinalDomain: [
//           '01',
//           '02',
//           '03',
//           '06',
//           '08',
//           '09',
//           '10',
//           '11',
//           '12',
//           '13',
//           '14',
//           '15',
//           '16',
//           '17',
//           '18',
//           '19',
//           '20',
//           '21',
//           '22',
//           '23',
//           '24',
//           '25',
//           '26',
//           '27',
//           '28',
//           '29',
//           '30',
//           '31',
//           '32',
//           '33',
//           '35',
//           '36',
//           '37',
//           '38',
//           '39',
//           '41',
//           '42',
//           '43',
//           '45',
//           '46',
//           '47',
//           '49',
//           '50',
//           '51',
//           '52',
//           '53',
//           '55',
//           '56',
//           '58',
//           '59',
//           '60',
//           '61',
//           '62',
//           '63',
//           '64',
//           '65',
//           '66',
//           '68',
//           '69',
//           '70',
//           '71',
//           '72',
//           '73',
//           '74',
//           '75',
//           '77',
//           '78',
//           '79',
//           '80',
//           '81',
//           '82',
//           '84',
//           '85',
//           '86',
//           '87',
//           '88',
//           '90',
//           '91',
//           '92',
//           '93',
//           '94',
//           '95',
//           '96',
//           '97',
//           '98',
//           '99',
//         ],
//       }),

//       popup: ({ properties }): Popup => ({
//         title: `SBI code: ${properties.l1_code}. ${properties.sbicode}`,
//         content: `${properties.l1_title} - ${properties.l2_title}`,
//       }),
//       detailModal: ({ properties }) => ({
//         title: `SBI Code`,
//         content: [
//           ['Code', properties.sbicode],
//           ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
//           ['Sub thema', properties.l2_title],
//           [
//             'Adres',
//             properties.postcode +
//               ' ' +
//               properties.huisnummer +
//               (properties.huisletter ? properties.huisletter : ''),
//           ],
//           ['Pand id', properties.pid],
//           ['Verblijfsobject id', properties.vid],
//         ],
//       }),
//     },
//   ],
// },

// {
//   id: 'layerDook33',
//   theme: 'informatiebeeld-bedrijventerreinen',
//   name: 'SBI codes onderverdeeld naar hoofdthema',
//   desc: "SBI codes onderverdeeld naar hoofdthema met subthema's",
//   lastUpdate: '2021-04',
//   referenceDate: '2021-04',
//   source: 'SBI codes uit RVO energielabels',
//   url: 'https://www.rvo.nl/onderwerpen/wetten-en-regels-gebouwen/energielabel-woningen',
//   subLayers: [
//     {
//       name: 'A. Landbouw, Bosbouw En Visserij',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Landbouw, jacht en dienstverlening voor de landbouw en jacht',
//             'Bosbouw, exploitatie van bossen en dienstverlening voor de bosbouw',
//             'Visserij en kweken van vis en schaaldieren',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'A'],

//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#b15928', '#AB7326', '#70401E'],
//             ordinalDomain: ['01', '02', '03'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'B. Winning Van Delfstoffen',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Winning van aardolie en aardgas',
//             'Winning van delfstoffen (geen olie en gas)',
//             'Dienstverlening voor de winning van delfstoffen',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'B'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#a6d854', '#33a02c', '#1b9e77'],
//             ordinalDomain: ['06', '08', '09'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'C. Industrie',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'voedingsmiddelen',
//             'dranken',
//             'tabaksproducten',
//             'textiel',
//             'kleding',
//             'leer, lederwaren en schoenen',
//             'artikelen van hout, kurk, riet',
//             'papier, karton en papier- en kartonwaren',
//             'Drukkerijen, reproductie van opgenomen media',
//             'cokesovenproducten en aardolieverwerking',
//             'chemische producten',
//             'farmaceutische grondstoffen en producten',
//             'producten van rubber en kunststof',
//             'overige niet-metaalhoudende minerale producten',
//             'metalen in primaire vorm',
//             'producten van metaal (geen machines en apparaten)',
//             'computers en van elektronische en optische apparatuur',
//             'elektrische apparatuur',
//             'overige machines en apparaten',
//             "auto's, aanhangwagens en opleggers",
//             'overige transportmiddelen',
//             'meubels',
//             'overige goederen',
//             'Reparatie en installatie van machines en apparaten',
//           ],
//           expressionType: 'match',
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: [
//               '#59a14f',
//               '#a6d854',
//               '#b2df8a',
//               '#33a02c',
//               '#1b9e77',
//               '#99d8c9',
//               '#66c2a4',
//               '#41ae76',
//               '#238b45',
//               '#006d2c',
//               '#00441b',
//               '#7bccc4',
//               '#4eb3d3',
//               '#0868ac',
//               '#084081',
//               '#67a9cf',
//               '#02818a',
//               '#016c59',
//               '#014636',
//               '#7FDB23',
//               '#b2df8a',
//               '#73824F',
//               '#33a02c',
//               '#043D00',
//             ],
//             ordinalDomain: [
//               '10',
//               '11',
//               '12',
//               '13',
//               '14',
//               '15',
//               '16',
//               '17',
//               '18',
//               '19',
//               '20',
//               '21',
//               '22',
//               '23',
//               '24',
//               '25',
//               '26',
//               '27',
//               '28',
//               '29',
//               '30',
//               '31',
//               '32',
//               '33',
//             ],
//           }),

//           filter: ['==', ['get', 'l1_code'], 'C'],
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'D. Productie En Distributie Van En Handel In Elektriciteit, Aardgas, Stoom En Gekoelde Lucht',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Productie en distributie van en handel in elektriciteit, aardgas, stoom en gekoelde lucht',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'D'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#33a02c'],
//             ordinalDomain: ['35'],
//           }),

//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'E. Winning En Distributie Van Water; Afval- En Afvalwaterbeheer En Sanering',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Winning en distributie van water',
//             'Afvalwaterinzameling en -behandeling',
//             'Afvalinzameling en –behandeling; voorbereiding tot recycling',
//             'Sanering en overig afvalbeheer',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'E'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#b2df8a', '#73824F', '#33a02c', '#043D00'],
//             ordinalDomain: ['36', '37', '38', '39'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'F. Bouwnijverheid',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Algemene burgerlijke en utiliteitsbouw en projectontwikkeling',
//             'Grond-, water- en wegenbouw (geen grondverzet)',
//             'Gespecialiseerde werkzaamheden in de bouw',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'F'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#b2df8a', '#33a02c', '#043D00'],
//             ordinalDomain: ['41', '42', '43'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'G. Groot- En Detailhandel; Reparatie Van Auto’s',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             "Handel in en reparatie van auto's, motorfietsen en aanhangers",
//             "Groothandel en handelsbemiddeling (niet in auto's en motorfietsen)",
//             "Detailhandel (niet in auto's)",
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'G'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#FF7F00', '#ffeda0', '#cc4c02'],
//             ordinalDomain: ['45', '46', '47'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'H. Vervoer En Opslag',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Vervoer over land',
//             'Vervoer over water',
//             'Luchtvaart',
//             'Opslag en dienstverlening voor vervoer',
//             'Post en koeriers',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'H'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: [
//               '#fc9272',
//               '#ED5A5F',
//               '#ef3b2c',
//               '#A50F15',
//               '#852226',
//             ],
//             ordinalDomain: ['49', '50', '51', '52', '53'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'I. Logies-, Maaltijd- En Drankverstrekking',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: ['Logiesverstrekking', 'Eet- en drinkgelegenheden'],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'I'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#ffeda0', '#cc4c02'],
//             ordinalDomain: ['55', '56'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'J. Informatie En Communicatie',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Uitgeverijen',
//             "Productie en distributie van films en televisieprogramma's; maken en uitgeven van geluidsopnamen",
//             "Verzorgen en uitzenden van radio- en televisieprogramma's",
//             'Telecommunicatie',
//             'Dienstverlenende activiteiten op het gebied van informatietechnologie',
//             'Dienstverlenende activiteiten op het gebied van informatie',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'J'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: [
//               '#fb6a4a',
//               '#ef3b2c',
//               '#67000d',
//               '#a50f15',
//               '#e31a1c',
//               '#fb9a99',
//             ],
//             ordinalDomain: ['58', '59', '60', '61', '62', '63'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'K. Financiële Instellingen',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Financiële instellingen (geen verzekeringen en pensioenfondsen)',
//             'Verzekeringen en pensioenfondsen (geen verplichte sociale verzekeringen)',
//             'Overige financiële dienstverlening',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'K'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#fb9a99', '#6a3d9a', '#bc80bd'],
//             ordinalDomain: ['64', '65', '66'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'L. Verhuur Van En Handel In Onroerend Goed',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: ['Verhuur van en handel in onroerend goed'],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'L'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#6a3d9a'],
//             ordinalDomain: ['68'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'M. Advisering, Onderzoek En Overige Specialistische Zakelijke Dienstverlening',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Rechtskundige dienstverlening, accountancy, belastingadvisering en administratie',
//             'Holdings (geen financiële), concerndiensten binnen eigen concern en managementadvisering',
//             'Architecten, ingenieurs en technisch ontwerp en advies; keuring en controle',
//             'Speur- en ontwikkelingswerk',
//             'Reclame en marktonderzoek',
//             'Industrieel ontwerp en vormgeving, fotografie, vertaling en overige consultancy',
//             'Veterinaire dienstverlening',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'M'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: [
//               '#bdbdbd',
//               '#969696',
//               '#737373',
//               '#525252',
//               '#252525',
//               '#000000',
//               '#000000',
//             ],
//             ordinalDomain: ['69', '70', '71', '72', '73', '74', '75'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'N. Verhuur Van Roerende Goederen En Overige Zakelijke Dienstverlening',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             "Verhuur en lease van auto's, consumentenartikelen, machines en overige roerende goederen",
//             'Arbeidsbemiddeling, uitzendbureaus en personeelsbeheer',
//             'Reisbemiddeling, reisorganisatie, toeristische informatie en reserveringsbureaus',
//             'Beveiliging en opsporing',
//             'Facility management, reiniging en landschapsverzorging',
//             'Overige zakelijke dienstverlening',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'N'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: [
//               '#000000',
//               '#252525',
//               '#525252',
//               '#737373',
//               '#969696',
//               '#bdbdbd',
//             ],
//             ordinalDomain: ['77', '78', '79', '80', '81', '82'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'O. Openbaar Bestuur, Overheidsdiensten En Verplichte Sociale Verzekeringen',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Openbaar bestuur, overheidsdiensten en verplichte sociale verzekeringen',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'O'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#e6ab02'],
//             ordinalDomain: ['84'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'P. Onderwijs',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: ['Onderwijs'],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'P'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#e6ab02'],
//             ordinalDomain: ['85'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'Q. Gezondheids- En Welzijnszorg',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Gezondheidszorg',
//             'Verpleging, verzorging en begeleiding met overnachting',
//             'Maatschappelijke dienstverlening zonder overnachting',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'Q'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#e6ab02', '#ffff33', '#ffff99'],
//             ordinalDomain: ['86', '87', '88'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'R. Cultuur, Sport En Recreatie',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Kunst',
//             'Culturele uitleencentra, openbare archieven, musea, dieren- en plantentuinen, natuurbehoud',
//             'Loterijen en kansspelen',
//             'Sport en recreatie',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'R'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#46bcce', '#a6cee3', '#0000ff', '#377eb8'],
//             ordinalDomain: ['90', '91', '92', '93'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'S. Overige Dienstverlening',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Levensbeschouwelijke en politieke organisaties, belangen- en ideële organisaties, hobbyclubs',
//             'Reparatie van computers en consumentenartikelen',
//             'Wellness en overige dienstverlening; uitvaartbranche',
//           ],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'S'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#a6cee3', '#0000ff', '#377eb8'],
//             ordinalDomain: ['94', '95', '96'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'T. Huishoudens Als Werkgever; Niet-Gedifferentieerde Productie Van Goederen En Diensten Door Huishoudens Voor Eigen Gebruik',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           expressionType: 'match',
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: [
//             'Huishoudens als werkgever van huishoudelijk personeel',
//             'Niet-gedifferentieerde productie van goederen en diensten door particuliere huishoudens voor eigen gebruik',
//           ],
//           filter: ['==', ['get', 'l1_code'], 'T'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#0000ff', '#377eb8'],
//             ordinalDomain: ['97', '98'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//     {
//       name: 'U. Extraterritoriale Organisaties En Lichamen',
//       scale: 'verblijfsobject',
//       unit: 'SBI code hoofd categorie',
//       styleLayers: [
//         staticBuildingsLayer.styleLayers[0],
//         {
//           attrName: 'l2_code',
//           tileSource: sourceSettings.sbi,
//           geomType: 'circle',
//           legendStops: ['Extraterritoriale organisaties en lichamen'],
//           expressionType: 'match',
//           filter: ['==', ['get', 'l1_code'], 'U'],
//           colorScale: createColorScale({
//             type: 'ordinal',
//             colorRange: ['#377eb8'],
//             ordinalDomain: ['99'],
//           }),
//           popup: ({ properties }): Popup => ({
//             title: `SBI code: ${properties.sbicode}`,
//             content: `${properties.l1_title} - ${properties.l2_title}`,
//           }),
//           detailModal: ({ properties }) => ({
//             title: `SBI Code`,
//             content: [
//               ['Code', properties.sbicode],
//               ['Hoofd thema', properties.l1_title],
//               ['Sub thema', properties.l2_title],
//               [
//                 'Adres',
//                 properties.postcode +
//                   ' ' +
//                   properties.huisnummer +
//                   (properties.huisletter ? properties.huisletter : ''),
//               ],
//               ['Pand id', properties.pid],
//               ['Verblijfsobject id', properties.vid],
//             ],
//           }),
//         },
//       ],
//     },
//   ],
// }

// {
//     id: 'layerDook114',
//     theme: 'autobranche',
//     name: 'OSM POIs in de autobranche ',
//     desc: 'OSM points of interest met auto bedrijven ',
//     url: 'https://wiki.openstreetmap.org/wiki/Tag:shop%3Dcar',
//     source: 'OSM',
//     lastUpdate: '2022-06',
//     referenceDate: '2021',
//     scale: 'POI',
//     unit: 'Locatie',
//     filterValues: ['car'],
//     filterNames: ['car'],
//     filterAttrb: 'class',
//     styleLayers: [
//       staticBuildingsLayer.styleLayers[0],
//       {
//         attrName: 'class',
//         isRelevantLayer: true,
//         tileSource: sourceSettings.openmaptiles,
//         'source-layer': 'poi',
//         geomType: 'circle',
//         legendStops: ['Auto gerelateerde locaties'],
//         expressionType: 'match',
//         colorScale: createColorScale({
//           type: 'ordinal',
//           colorRange: ['#BD0B02'],
//           ordinalDomain: ['car'],
//         }),
//         detailModal: ({ properties }) => ({
//           title: 'Openstreet map locatie',
//           url: properties.name
//             ? `https://www.openstreetmap.org/search?query=${properties.name}`
//             : null,
//           content: [
//             properties.name ? ['Naam', `${properties.name}`] : null,
//             properties.class ? ['Class', `${properties.class}`] : null,
//             properties.subclass ? ['subclass', `${properties.subclass}`] : null,
//           ],
//         }),
//       },
//       {
//         geomType: 'symbol',
//         attrName: 'name',
//       },
//     ],
//   },

// {
//   id: 'layer231',
//   theme: 'sociaaldook',
//   name: 'Gemiddeld inkomen per inwoner',
//   desc: `Het rekenkundig gemiddeld ${tooltip(
//     'bruto gemiddelde',
//     'persoonlijk inkomen'
//   )} per persoon op basis van de totale bevolking in particuliere huishoudens.
//   De waarde is vermeld bij minimaal 100 inwoners per regio.`,
//   scale: 'Gemeente, wijk, buurt',
//   average: 25,
//   unit: 'x1000€',
//   source: 'CBS',
//   lastUpdate: '2021-05',
//   referenceDate: '2018',
//   styleLayers: [
//     {
//       attrName: 'gemiddeld_inkomen_per_inwoner',
//       tileSource: sourceSettings.cbs,
//       'source-layer': 'buurtcijfers2018',
//       legendStops: [0, 13, 25, 40, 80],
//       noDataValue: [-99999999, -99999998],
//       expressionType: 'interpolate',
//       colorScale: createColorScale({
//         type: 'sequentional',
//         color: 'cividis',
//         domain: [13, 40],
//       }),
//       popup: ({
//         properties: { gemiddeld_inkomen_per_inwoner, name, code },
//       }) => ({
//         title: `${name} - ${code}`,
//         content: `${gemiddeld_inkomen_per_inwoner} x1000€`,
//       }),
//     },
//   ],
// },
// {
//   id: 'layer232',
//   theme: 'sociaaldook',
//   name: 'Gemiddeld inkomen per inkomensontvanger',
//   desc: `Het rekenkundig gemiddeld persoonlijk inkomen per persoon op basis van personen met persoonlijk inkomen die deel uitmaken van particuliere huishoudens.
//   De waarde is vermeld bij minimaal 100 personen met persoonlijk inkomen in particuliere huishoudens per regio.`,
//   source: 'CBS',
//   scale: 'Gemeente, wijk, buurt',
//   unit: 'x1000€',
//   average: 31,
//   lastUpdate: '2021-05',
//   referenceDate: '2018',
//   styleLayers: [
//     {
//       attrName: 'gemiddeld_inkomen_per_inkomensontvanger',
//       tileSource: sourceSettings.cbs,
//       'source-layer': 'buurtcijfers2018',
//       legendStops: [0, 14, 28, 32, 40, 75, 98],
//       noDataValue: [-99999999, -99999998],
//       expressionType: 'interpolate',
//       colorScale: createColorScale({
//         type: 'sequentional',
//         color: 'cividis',
//         domain: [14, 48],
//       }),
//       popup: ({
//         properties: { gemiddeld_inkomen_per_inkomensontvanger, name, code },
//       }) => ({
//         title: `${name} - ${code}`,
//         content: `${gemiddeld_inkomen_per_inkomensontvanger} x1000€`,
//       }),
//     },
//   ],
// },
// {
//   id: 'layer2314',
//   theme: 'sociaaldook',
//   name: 'Mediaan inkomen per huishouden - Grid',
//   desc: `Per gebied wordt de mediaan van het gestandaardiseerde inkomen per huishouden vergeleken met de verdeling van dit inkomen voor alle huishoudens in het land, en op basis hiervan ingedeeld in een groep; laag, onder midden, midden, boven midden, of hoog.
//   De vierkanten bevatten waarden waarin minimaal 5 inwoners of 5 woningen voorkomen. `,
//   scale: 'Grid 100x100m',
//   unit: 'Klassificatie',
//   source: 'CBS',
//   lastUpdate: '2021-05-19',
//   referenceDate: '2018',
//   styleLayers: [
//     {
//       attrName: 'mediaan_inkomen_huishouden',
//       tileSource: sourceSettings.cbsgrid,
//       'source-layer': 'cbs_grid_2018',
//       legendStops: [
//         '80-100 hoog- 37.800€ en hoger',
//         '60-80 boven midden - tussen 29.500€ en 37.800€',
//         '60-100 boven midden tot hoog',
//         '40-80 midden tot boven midden',
//         '40-60 midden -  tussen 23.200€ en 29.500€',
//         '20-80 onder midden tot boven midden',
//         '20-60 onder midden tot midden',
//         '20-40 onder midden -  tussen 17.600€ en 23.200€',
//         '00-60 laag tot midden',
//         '00-40 laag tot onder midden',
//         '00-20 laag - beneden 17.600€',
//         'onclassificeerbaar',
//       ],
//       expressionType: 'match',
//       colorScale: createColorScale({
//         type: 'ordinal',
//         colorRange: [
//           'rgb(253, 234, 69)',
//           'rgb(234, 209, 86)',
//           'rgb(202, 186, 106)',
//           'rgb(187, 175, 113)',
//           'rgb(160, 153, 120)',
//           'rgb(127, 124, 117)',
//           'rgb(105, 105, 112)',
//           'rgb(92, 95, 110)',
//           'rgb(43, 68, 110)',
//           'rgb(10, 50, 106)',
//           'rgb(0, 32, 81)',
//           '#939393',
//         ],
//         ordinalDomain: [
//           '80-100 hoog',
//           '60-80 boven midden',
//           '60-100 boven midden tot hoog',
//           '40-80 midden tot boven midden',
//           '40-60 midden',
//           '20-80 onder midden tot boven midden',
//           '20-60 onder midden tot midden',
//           '20-40 onder midden',
//           '00-60 laag tot midden',
//           '00-40 laag tot onder midden',
//           '00-20 laag',
//           'onclassificeerbaar',
//         ],
//         tickValues: [
//           '80-100 hoog- 37.800€ en hoger',
//           '60-80 boven midden - tussen 29.500€ en 37.800€',
//           '60-100 boven midden tot hoog',
//           '40-80 midden tot boven midden',
//           '40-60 midden -  tussen 23.200€ en 29.500€',
//           '20-80 onder midden tot boven midden',
//           '20-60 onder midden tot midden',
//           '20-40 onder midden -  tussen 17.600€ en 23.200€',
//           '00-60 laag tot midden',
//           '00-40 laag tot onder midden',
//           '00-20 laag - beneden 17.600€',
//           'onclassificeerbaar',
//         ],
//         firstTick: 'voor',
//         lastTick: '>',
//       }),
//       popup: ({
//         properties: { mediaan_inkomen_huishouden, c28992r100 },
//       }) => ({
//         title: `Grid cell: ${c28992r100}`,
//         content: `${mediaan_inkomen_huishouden}`,
//       }),
//     },
//   ],
// },
//
// fail
//
// {
//   id: 'layerDook100',
//   theme: 'fail',
//   desc: 'Locaties van faillissementen / insolventies, de dataset wordt 3x per dag op weekdagen actueel gehouden. Data blijft maximaal 6 maanden staan.',
//   name: 'Insolventie Register Faillissementen',
//   source: 'https://insolventies.rechtspraak.nl',
//   scale: 'Tijdsindicatie',
//   unit: 'Faillissementen',
//   lastUpdate: '2022-01',
//   referenceDate: '2021-10',
//   styleLayers: [
//     staticBuildingsLayer.styleLayers[0],
//     {
//       attrName: 'pt',
//       tileSource: sourceSettings.faillissementen,
//       geomType: 'circle',
//       expressionType: 'interpolate',
//       legendStops: getCurrentAndPrevDate(6),
//       firstLabel: '> 6 maanden geleden',
//       lastLabel: 'heden',
//       colorScale: createColorScale({
//         type: 'sequentional',
//         color: 'red',
//         domain: getCurrentAndPrevDate(6, 'middle'),
//       }),
//       popup: ({ properties }): Popup => ({
//         title: `${properties.description}`,
//       }),
//       detailModal: ({ properties }) => ({
//         type: 'fail',
//         title: `Details Faillisement ${properties.name}`,
//         desc: properties.description,
//         url: `https://insolventies.rechtspraak.nl/#!/details/${properties.insolvency_id}`,
//         urlTitle: 'Bekijk dit item op het Centraal Insolventieregister',
//         content: [
//           ['Naam', properties.name + ' ' + properties.description],
//           ['Datum', `${new Date(properties.pt * 1000)}`],
//           ['Register ID', properties.insolvency_id],
//           ['KvK-nummer', properties.kvk_number],
//           [
//             'Vestigingsadres',
//             `${properties.street} ${properties.street_number} ${properties.postal_code} ${properties.town}`,
//           ],
//         ],
//       }),
//     },
//   ],
// },
//
// findings
//
// {
//   id: 'layerDook200',
//   theme: 'bevindingen',
//   desc: 'Bevindingen zijn alleen zichtbaar na inloggen. De notities bij de bevindingen zijn alleen zichtbaar voor de auteur. Gebruik deze functionaliteit om meer inzicht te geven aan het publiek in uw projecten. Let op: Dit is de Bèta versie. Alle ingevoerde bevindingen worden niet definitief opgeslagen en binnenkort verwijderd!',
//   name: 'Bevindingen registratie',
//   authLocked: true,
//   source: 'bevindingen',
//   scale: 'Locatie van bevindingen',
//   lastUpdate: '2021-11',
//   referenceDate: '2021-11',
//   unit: '',
//   drawType: 'findings',
//   styleLayers: [
//     staticBuildingsLayer.styleLayers[0],
//     {
//       attrName: 'user_id',
//       userAttrName: 'user_id',
//       tileSource: sourceSettings.bevindingen,
//       'source-layer': 'dook.finding_point',
//       geomType: 'circle',
//       legendStops: ['Eigen Bevinding', 'Bevinding van anderen'],
//       colorScale: createColorScale({
//         type: 'ordinal',
//         colorRange: ['#ffbc2c', '#0b71a1'],
//         ordinalDomain: ['0', '1'],
//       }),
//       expressionType: 'bevindingen',
//       popup: ({ properties }): Popup => ({
//         title: `Bevinding: ${properties.title}`,
//       }),
//       detailModal: () => ({
//         title: 'Bevindingen',
//         type: 'findings',
//       }),
//     },
//   ],
// },

// {
//   id: 'layer2',
//   theme: 'gebouw',
//   name: 'Energie Label - voorlopig',
//   desc: `Het voorlopige energielabel geeft een eerste indicatie van hoe goed een object geisoleerd is, en daarmee ook een eerste indicatie van hoeveel er nog nageisoleerd moet worden om het object geschikt te maken voor een bepaalde warmte-oplossing.`,
//   note: '11% van de panden heeft geen voorlopig energielabel',
//   source: 'RVO',
//   scale: 'Pand',
//   unit: 'Energielabel klassen',
//   lastUpdate: '2020-06',
//   referenceDate: '2020-06',
//   filterAttrb: 'elabel_voorlopig',
//   filterNames: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
//   filterValues: [1, 2, 3, 4, 5, 6, 7],
//   drawType: 'count',
//   url: 'https://www.rvo.nl/onderwerpen/wetten-en-regels-gebouwen/energielabel-woningen',
//   styleLayers: [
//     {
//       tileSource: sourceSettings.bag,
//       attrName: 'elabel_voorlopig',
//       geomType: 'fill',
//       legendStops: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
//       expressionType: 'match',
//       extrusionAttr: 'hoogte',
//       colorScale: createColorScale({
//         type: 'linear',
//         colorRange: [
//           '#009342',
//           '#1ba943',
//           '#9ecf1b',
//           '#f8f51c',
//           '#f4b003',
//           '#df6d14',
//           '#db261d',
//         ],
//         domain: [1, 2, 3, 4, 5, 6, 7],
//       }),

//       popup: ({ properties }): Popup => ({
//         title: 'Energie label:',
//         content: energyLabels[properties.elabel_voorlopig],
//       }),
//       detailModal: ({ properties }) => ({
//         title: 'Details Pand',
//         type: 'building',
//         content: [
//           properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
//           properties.elabel_voorlopig
//             ? [
//               'Energie Label voorlopig',
//               energyLabels[properties.elabel_voorlopig],
//             ]
//             : null,
//           properties.elabel_definitief
//             ? [
//               'Energie Label definitief',
//               energyLabels[properties.elabel_definitief],
//             ]
//             : null,
//           properties.energieklasse_score
//             ? [
//               'Energie klasse',
//               energyLabelClasses[properties.energieklasse_score],
//             ]
//             : null,
//           properties.woonfunctie_score >= 0
//             ? [
//               'Woonfunctie',
//               properties.woonfunctie_score === 0
//                 ? 'Geen woonfunctie'
//                 : properties.woonfunctie_score === 100
//                   ? 'Alleen woonfunctie'
//                   : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
//             ]
//             : null,
//         ],
//       }),
//     },
//     noDataStyleLayer,
//   ],
// },
// {
//   id: 'layer2b',
//   theme: 'gebouw',
//   name: 'Energie Label - definitief',
//   desc: `Het definitieve energielabel geeft een eerste indicatie van hoe goed een object geisoleerd is, en daarmee ook een eerste indicatie van hoeveel er nog nageisoleerd moet worden om het object geschikt te maken voor een bepaalde warmte-oplossing. Het is betrouwbaarder dan het voorlopige energielabel, maar niet voor alle objecten beschikbaar.`,
//   note: 'Van 62% van de panden is geen definitief energielabel geregistreerd',
//   source: 'RVO',
//   scale: 'Pand',
//   unit: 'Energielabel klassen',
//   lastUpdate: '2020-06',
//   referenceDate: '2020-06',
//   filterAttrb: 'elabel_definitief',
//   filterNames: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
//   filterValues: [1, 2, 3, 4, 5, 6, 7],
//   url: 'https://www.rvo.nl/onderwerpen/wetten-en-regels-gebouwen/energielabel-woningen',

//   styleLayers: [
//     {
//       tileSource: sourceSettings.bag,
//       attrName: 'elabel_definitief',
//       geomType: 'fill',
//       legendStops: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
//       expressionType: 'match',
//       colorScale: createColorScale({
//         type: 'linear',
//         colorRange: [
//           '#009342',
//           '#1ba943',
//           '#9ecf1b',
//           '#f8f51c',
//           '#f4b003',
//           '#df6d14',
//           '#db261d',
//         ],
//         domain: [1, 2, 3, 4, 5, 6, 7],
//       }),
//       extrusionAttr: 'hoogte',
//       popup: ({ properties }) => ({
//         title: 'Energie label:',
//         content: energyLabels[properties.elabel_definitief],
//       }),
//       detailModal: ({ properties }) => ({
//         title: 'Details Pand',
//         type: 'building',
//         content: [
//           properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
//           properties.elabel_voorlopig
//             ? [
//               'Energie Label voorlopig',
//               energyLabels[properties.elabel_voorlopig],
//             ]
//             : null,
//           properties.elabel_definitief
//             ? [
//               'Energie Label definitief',
//               energyLabels[properties.elabel_definitief],
//             ]
//             : null,
//           properties.energieklasse_score
//             ? [
//               'Energie klasse',
//               energyLabelClasses[properties.energieklasse_score],
//             ]
//             : null,
//           properties.woonfunctie_score >= 0
//             ? [
//               'Woonfunctie',
//               properties.woonfunctie_score === 0
//                 ? 'Geen woonfunctie'
//                 : properties.woonfunctie_score === 100
//                   ? 'Alleen woonfunctie'
//                   : `Gemixte functies waarvan ${properties.woonfunctie_score}% woonfunctie`,
//             ]
//             : null,
//         ],
//       }),
//     },
//     noDataStyleLayer,
//   ],
//   drawType: 'count',
// },

// {
//   id: 'layerDook32',
//   authRoles: ['autobranche'],
//   theme: 'autobranche',
//   desc: 'De Standaard Bedrijfsindeling (SBI) is een hiërarchische indeling van economische activiteiten die het CBS onder meer gebruikt om bedrijfseenheden in te delen naar hun hoofdactiviteit. Hier vind u alle codes in een laag.',
//   name: 'Alle SBI codes',
//   source: 'SBI codes uit RVO energielabels',
//   url: 'https://www.rvo.nl/onderwerpen/wetten-en-regels-gebouwen/energielabel-woningen',
//   scale: 'verblijfsobject',
//   lastUpdate: '2021-04',
//   referenceDate: '2021-04',
//   unit: 'SBI code- hoofdcategorie',
//   drawType: 'count',
//   styleLayers: [
//     staticBuildingsLayer.styleLayers[0],
//     {
//       isRelevantLayer: true,
//       attrName: 'l1_code',
//       tileSource: sourceSettings.sbi,
//       geomType: 'circle',
//       legendStops: [
//         'A. Landbouw, Bosbouw en Visserij',
//         'B. Winning van Delfstoffen',
//         'C. Industrie',
//         'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
//         'E. Winning en Distributie van Water',
//         'F. Bouwnijverheid',
//         'G. Groot- en Detailhandel; Reparatie Van Auto’s',
//         'H. Vervoer en Opslag',
//         'I. Logies-, Maaltijd- en Drankverstrekking',
//         'J. Informatie en Communicatie',
//         'K. Financiële Instellingen',
//         'L. Verhuur van en Handel in Onroerend Goed',
//         'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
//         'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
//         'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
//         'P. Onderwijs',
//         'Q. Gezondheids- en Welzijnszorg',
//         'R. Cultuur, Sport en Recreatie',
//         'S. Overige Dienstverlening',
//         'T. Huishoudens als Werkgever',
//         'U. Extraterritoriale Organisaties en Lichamen',
//       ],
//       expressionType: 'match',
//       colorScale: createColorScale({
//         type: 'ordinal',
//         colorRange: [
//           '#70401E',
//           '#7FDB23',
//           '#b2df8a',
//           '#73824F',
//           '#33a02c',
//           '#043D00',
//           '#ff7f00',
//           '#e31a1c',
//           '#d95f02',
//           '#fb9a99',
//           '#6a3d9a',
//           '#bc80bd',
//           '#000000',
//           '#666666',
//           '#e6ab02',
//           '#ffff33',
//           '#ffff99',
//           '#46bcce',
//           '#a6cee3',
//           '#0000ff',
//           '#377eb8',
//         ],
//         ordinalDomain: [
//           'A',
//           'B',
//           'C',
//           'D',
//           'E',
//           'F',
//           'G',
//           'H',
//           'I',
//           'J',
//           'K',
//           'L',
//           'M',
//           'N',
//           'O',
//           'P',
//           'Q',
//           'R',
//           'S',
//           'T',
//           'U',
//         ],
//       }),

//       popup: ({ properties }): Popup => ({
//         title: `SBI code: ${properties.l1_code}. ${properties.sbicode}`,
//         content: `${properties.l1_title} - ${properties.l2_title}`,
//       }),
//       detailModal: ({ properties }) => ({
//         title: `SBI Code`,
//         content: [
//           ['Code', properties.sbicode],
//           ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
//           ['Sub thema', properties.l2_title],
//           [
//             'Adres',
//             properties.postcode +
//               `${properties.huisnummer} ${properties.huisletter || ''}`,
//           ],
//           ['Pand id', properties.pid],
//           ['Verblijfsobject id', properties.vid],
//         ],
//       }),
//     },
//   ],
// },

export const layers = ''

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const polygonDrawStyle = [
  {
    id: 'gl-draw-line',
    type: 'line',
    filter: [
      'all',
      ['==', '$type', 'LineString'],
      ['==', 'mode', 'draw_polygon'],
    ],
    layout: {
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': [
        'case',
        ['==', ['get', 'active'], 'true'],
        '#212121',
        '#212121',
      ],
      'line-dasharray': [0.2, 2],
      'line-width': 5,
    },
  },
  // polygon fill while drawing
  {
    id: 'gl-draw-polygon-fill',
    type: 'fill',
    filter: ['all', ['==', '$type', 'Polygon'], ['==', 'mode', 'draw_polygon']],
    paint: {
      'fill-color': '#ffbc2c',
      'fill-outline-color': '#212121',
      'fill-opacity': ['case', ['==', ['get', 'active'], 'true'], 0.75, 0.2],
    },
  },
  // polygon outline while drawing
  {
    id: 'gl-draw-polygon-stroke',
    type: 'line',
    filter: ['all', ['==', '$type', 'Polygon'], ['==', 'mode', 'draw_polygon']],
    layout: {
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': [
        'case',
        ['==', ['get', 'active'], 'true'],
        '#212121',
        '#212121',
      ],
      'line-dasharray': [0.2, 2],
      'line-width': 4,
    },
  },

  // NOT DRAWING
  // Polygon not static
  // polygon fill
  {
    id: 'gl-draw-polygon',
    type: 'fill',
    filter: [
      'all',
      ['==', '$type', 'Polygon'],
      ['==', 'mode', 'direct_select'],
    ],
    paint: {
      'fill-color': '#ffbc2c',
      'fill-opacity': ['case', ['==', ['get', 'active'], 'true'], 0.75, 0.25],
    },
  },
  // polygon outline not static
  {
    id: 'gl-polygon-stroke-edit',
    type: 'line',
    filter: ['all', ['==', '$type', 'Polygon'], ['!=', 'mode', 'static']],
    layout: {
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': '#212121',
      'line-dasharray': [0.2, 2],
      'line-width': 4,
    },
  },

  // SELECT polygon

  {
    id: 'gl-polygon-fill-edit',
    type: 'fill',
    filter: [
      'all',
      ['==', '$type', 'Polygon'],
      ['==', 'mode', 'simple_select'],
    ],
    paint: {
      'fill-color': '#ffbc2c',

      'fill-opacity': ['case', ['==', ['get', 'active'], 'true'], 0.75, 0.25],
    },
  },
  {
    id: 'gl-polygon-stroke',
    type: 'line',
    filter: [
      'all',
      ['==', '$type', 'Polygon'],
      ['==', 'mode', 'simple_select'],
    ],
    layout: {
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': '#212121',
      'line-width': 4,
    },
  },

  // Vertex point while not static
  // vertex point halos
  {
    id: 'gl-draw-polygon-and-line-vertex-halo',
    type: 'circle',
    filter: [
      'all',
      ['==', 'meta', 'vertex'],
      ['==', '$type', 'Point'],
      ['!=', 'mode', 'static'],
    ],
    paint: {
      'circle-radius': 8,
      'circle-color': '#212121',
    },
  },
  // vertex points
  {
    id: 'gl-draw-polygon-and-line-vertex',
    type: 'circle',
    filter: [
      'all',
      ['==', 'meta', 'vertex'],
      ['==', '$type', 'Point'],
      ['!=', 'mode', 'static'],
    ],
    paint: {
      'circle-radius': ['case', ['==', ['get', 'active'], 'true'], 5, 4],
      'circle-color': [
        'case',
        ['==', ['get', 'active'], 'true'],
        '#ffbc2c',
        '#212121',
      ],
    },
  },
]

export const pointDrawStyle = [
  {
    id: 'highlight-active-points',
    type: 'circle',
    paint: {
      'circle-stroke-width': [
        'interpolate',
        ['linear'],
        ['zoom'],
        8,
        0.5,
        12,
        1,
        16,
        3,
      ],
      'circle-radius': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        1.5,
        12,
        3,

        16,
        15,
      ],
      'circle-color': 'black',
      'circle-stroke-color': '#ffbc2c',
    },
    filter: ['==', '$type', 'Point'],
  },
  {
    id: 'gl-draw-line',
    type: 'line',
    filter: [
      'all',
      ['==', '$type', 'LineString'],
      ['==', 'mode', 'draw_polygon'],
    ],
    layout: {
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': [
        'case',
        ['==', ['get', 'active'], 'true'],
        '#212121',
        '#212121',
      ],
      'line-dasharray': [0.2, 2],
      'line-width': 5,
    },
  },
]

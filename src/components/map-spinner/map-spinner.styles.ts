// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Spinner } from '@commonground/design-system'

export const StyledSpinnerContainer = styled.div`
  width: 36px;
  height: 36px;
  background-color: ${(p) => p.theme.tokens.colorBackground};
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: ${(p) => p.theme.tokens.spacing02};
  box-shadow: ${(p) => p.theme.boxShadow};
`

export const StyledSpinner = styled(Spinner)`
  border: none;
  outline: none;
  box-shadow: none;
  background: none;
  pointer-events: none !important;
  margin: 0 !important;
  margin-right: 0 !important;
`

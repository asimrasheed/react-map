// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const FooterContainer = styled.div`
    grid-row: 6 / span 2;
    grid-column: 1 / span 1;
    align-items: center;
    background: #757575;
    /* bottom: 0; */

    display: flex;
    flex-direction: column;
   
    /* height: 5rem; */
    justify-content: center;
    /* left: 0; */
    /* position: absolute; */
    /* width: 30rem; */
    z-index: 10;

    box-shadow: 0 0 #0000,  0 0 #0000,  0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);

    ${mediaQueries.mdDown`
    display: none;
    `}
   
    & a, .asA {
        align-items: center;
        cursor: pointer;
        display: flex;
        margin-left: 1rem;
        flex-wrap: nowrap;
    }
    & p {
        font-size: 14px;
        color: white;
    }
    & svg {
        color: white;
        margin-right: 0.5rem;
    }
   }
`

export const LinksContainer = styled.div`
  display: flex;
  margin-top: 0.5rem;
`

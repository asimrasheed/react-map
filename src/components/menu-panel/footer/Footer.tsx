// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useContext, useState } from 'react'
import { Icon, Drawer, CGLogo, GitLabLogo } from '@commonground/design-system'
import ReactMarkdown from 'react-markdown'
import AppContext from '../../AppContext'
import * as Styled from './Footer.styled'

const Info = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="14"
    height="14"
  >
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      fill="currentColor"
      d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM11 7h2v2h-2V7zm0 4h2v6h-2v-6z"
    />
  </svg>
)
const Database = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="14"
    height="14"
  >
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      fill="currentColor"
      d="M5 12.5c0 .313.461.858 1.53 1.393C7.914 14.585 9.877 15 12 15c2.123 0 4.086-.415 5.47-1.107 1.069-.535 1.53-1.08 1.53-1.393v-2.171C17.35 11.349 14.827 12 12 12s-5.35-.652-7-1.671V12.5zm14 2.829C17.35 16.349 14.827 17 12 17s-5.35-.652-7-1.671V17.5c0 .313.461.858 1.53 1.393C7.914 19.585 9.877 20 12 20c2.123 0 4.086-.415 5.47-1.107 1.069-.535 1.53-1.08 1.53-1.393v-2.171zM3 17.5v-10C3 5.015 7.03 3 12 3s9 2.015 9 4.5v10c0 2.485-4.03 4.5-9 4.5s-9-2.015-9-4.5zm9-7.5c2.123 0 4.086-.415 5.47-1.107C18.539 8.358 19 7.813 19 7.5c0-.313-.461-.858-1.53-1.393C16.086 5.415 14.123 5 12 5c-2.123 0-4.086.415-5.47 1.107C5.461 6.642 5 7.187 5 7.5c0 .313.461.858 1.53 1.393C7.914 9.585 9.877 10 12 10z"
    />
  </svg>
)
const Download = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="14"
    height="14"
  >
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      fill="currentColor"
      d="M3 19h18v2H3v-2zm10-5.828L19.071 7.1l1.414 1.414L12 17 3.515 8.515 4.929 7.1 11 13.17V2h2v11.172z"
    />
  </svg>
)

const MenuPanelFooter = (): JSX.Element => {
  const config = useContext(AppContext)
  const [showDrawer, setShowDrawer] = useState(false)

  return (
    <>
      <Styled.FooterContainer>
        <p>In opdracht van VNG Realisatie</p>
        <Styled.LinksContainer>
          <div className="asA" onClick={() => setShowDrawer(true)}>
            <Icon as={Info} />
            <p>Colofon & Uitleg</p>
          </div>
          <div>
            <a target="blank" href="https://commondatafactory.nl/docs/metadata">
              <Icon as={Database} />
              <p>Bron beschrijving</p>
            </a>
          </div>
          <div>
            <a target="blank" href="https://commondatafactory.nl/docs/databron">
              <Icon as={Download} />
              <p>Download data</p>
            </a>
          </div>
        </Styled.LinksContainer>
      </Styled.FooterContainer>
      {showDrawer && (
        <Drawer
          closeHandler={() => setShowDrawer(false)}
          autoFocus
          style={{ zIndex: 1500 }}
        >
          <Drawer.Header
            title="Over deze applicatie"
            style={{ fontSize: '2rem' }}
          />
          <Drawer.Content>
            <div className="info">
              <ReactMarkdown>{config.about}</ReactMarkdown>
              <br />
              <p>
                Deze data toepassing bevat geen enkele interpretatie. De
                informatie kan niet worden gebruikt in plaats van advies.
                Beslissingen op basis van deze informatie zijn voor uw eigen
                rekening en risico. De open data op de commondatafactory.nl zijn
                met grote zorg samengesteld. Desondanks kan geen enkele garantie
                gegeven worden met betrekking tot de volledigheid, juistheid of
                actualiteit van de open data.
              </p>
              <br />
              <h3>
                Deze applicatie is ontwikkeld door{' '}
                <a
                  style={{ textDecorationLine: 'underline' }}
                  target="blank"
                  href="https://commondatafactory.nl/"
                >
                  Commondatafactory
                </a>
                .
              </h3>

              <br />
              <h3>
                In opdracht van{' '}
                <img
                  style={{ display: 'inline-block', marginLeft: '0.5rem' }}
                  src="/images/vng-logo.svg"
                  alt="Logo van VNG Realisatie"
                />
              </h3>

              <br />
              <h3>
                De code kunt u vinden op{' '}
                <a
                  style={{ textDecorationLine: 'underline' }}
                  target="blank"
                  href="https://gitlab.com/commondatafactory/"
                >
                  Gitlab
                  <GitLabLogo
                    style={{
                      height: '24px',
                      display: 'inline-block',
                      marginLeft: '0.5rem',
                    }}
                  />
                </a>
              </h3>

              <br />

              <h3>
                Ontwikkeling is volgens de implementatie van{' '}
                <a
                  target="blank"
                  href="https://commonground.nl/"
                  style={{ textDecorationLine: 'underline' }}
                >
                  Commonground.nl
                  <CGLogo
                    style={{
                      height: '24px',
                      display: 'inline-block',
                      marginLeft: '0.5rem',
                    }}
                  />
                </a>
                en de{' '}
                <a
                  href="https://standard.publiccode.net/"
                  style={{ textDecorationLine: 'underline' }}
                >
                  standard for public code
                </a>
                .
              </h3>
            </div>
          </Drawer.Content>
        </Drawer>
      )}
    </>
  )
}

export default MenuPanelFooter

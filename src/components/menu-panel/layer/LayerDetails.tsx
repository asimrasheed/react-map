// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent, useContext } from 'react'
import { Icon } from '@commonground/design-system'

import type { DataLayerProps } from '../../../data/DataLayers'

import { parseDate } from '../../../utils/parse-date'
import { useIsMobile } from '../../../hooks/use-is-mobile'

import { MapContext } from '../../../providers/map-provider'
import useUser from '../../../hooks/use-user'

import AddIcon from '../../../../public/images/add.svg'
import { DrawContext } from '../../../providers/draw-provider'
import { PolygonIcon } from './PolygonIcon'
import { CustomFilter } from './Filter'
import { FilterInputBar } from './FilterHierarchy/FilterInputBar'

import * as Styled from './Layer.styled'

interface LayerDetailsProps {
  dataLayer: Partial<DataLayerProps>
}
export const LayerDetails: FunctionComponent<LayerDetailsProps> = (
  props
): JSX.Element => {
  const { dataLayer } = props
  const { setAnalyzeMode, setSelectedFeatureAndEvent } = useContext(MapContext)
  const { setDrawMode } = useContext(DrawContext)
  const isMobile = useIsMobile()

  const { isLoggedIn } = useUser()

  const handleClick = () => {
    // TODO: Layer is passed from Categories.tsx. Should probably come through useLayers hook
    // TODO waarom hier deze leeg maken ?
    setSelectedFeatureAndEvent(null)
    setAnalyzeMode(dataLayer?.drawType)
    if (dataLayer.theme === 'bevindingen') {
      setDrawMode('initPoint')
    }
  }

  return (
    <Styled.LayerDetails>
      {/* tooltip toevoegd ##  */}
      <p>{dataLayer?.desc}</p>
      {!isMobile && (
        <>
          {dataLayer.filterValues &&
            (dataLayer.theme as any).includes('sbi') && (
              <CustomFilter
                filterValues={dataLayer.filterValues}
                attrName={dataLayer.filterAttrb}
                filterNames={dataLayer.filterNames}
              />
            )}
          {dataLayer.filterValues &&
            (dataLayer.theme as any).includes('kvk') && <FilterInputBar />}

          {dataLayer?.drawType &&
            dataLayer?.drawType === 'findings' &&
            isLoggedIn && (
              <Styled.CustomButton
                variant="link"
                size="small"
                onClick={handleClick}
              >
                <Icon as={AddIcon} inline />
                Bevinding toevoegen
              </Styled.CustomButton>
            )}
          {dataLayer?.drawType && dataLayer?.drawType === 'count' && (
            <Styled.CustomButton
              variant="link"
              size="small"
              onClick={handleClick}
            >
              <PolygonIcon inline />
              Gebied analyseren
            </Styled.CustomButton>
          )}
          <Styled.MetaDataItem>
            <div>Verwerkt in deze applicatie</div>
            <div>{parseDate(dataLayer?.lastUpdate)}</div>
          </Styled.MetaDataItem>
          <Styled.MetaDataItem>
            <div>Bronnen</div>
            <div>
              {dataLayer?.source}{' '}
              {dataLayer?.referenceDate && (
                <span className="smallGrey">
                  {parseDate(dataLayer?.referenceDate)}
                </span>
              )}
            </div>
          </Styled.MetaDataItem>
          {dataLayer?.url && (
            <>
              <Styled.MetaDataItem>
                <div>Verwerkt in deze applicatie</div>
                <div>{parseDate(dataLayer?.lastUpdate)}</div>
              </Styled.MetaDataItem>
              <Styled.MetaDataItem>
                <div>Bronnen</div>
                <div>
                  {dataLayer?.source}{' '}
                  {dataLayer?.referenceDate && (
                    <span className="smallGrey">
                      {parseDate(dataLayer?.referenceDate)}
                    </span>
                  )}
                </div>
              </Styled.MetaDataItem>
            </>
          )}
          {dataLayer?.url && (
            <Styled.MetaDataItem>
              <a href={dataLayer?.url} target="_blank" rel="noreferrer">
                Externe bron
              </a>
            </Styled.MetaDataItem>
          )}
        </>
      )}
    </Styled.LayerDetails>
  )
}

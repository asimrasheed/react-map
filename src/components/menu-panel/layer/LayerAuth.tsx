// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent } from 'react'
import { Button } from '@commonground/design-system'
import LockIcon from '../../../../public/images/lock.svg'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import * as Styled from './Layer.styled'

interface LayerAuthProps {
  description?: string
  isLoggedIn?: boolean
}

const permissionsUrl = `${process.env.NEXT_PUBLIC_AUTH_URL}/permissies`
const loginUrl = `${process.env.NEXT_PUBLIC_AUTH_URL}/login`

export const LayerAuth: FunctionComponent<LayerAuthProps> = ({
  description,
  isLoggedIn,
}): JSX.Element => {
  const isMobile = useIsMobile()

  return (
    <Styled.LayerDetails onClick={(e) => e.stopPropagation()}>
      <p>{description}</p>
      <Styled.AuthenticationDetails>
        <div>
          <LockIcon />{' '}
          {isLoggedIn ? (
            <span>Onvoldoende permissie om deze laag in te zien </span>
          ) : !isMobile ? (
            <span> Log in om deze kaartlaag te zien </span>
          ) : (
            <span> Log in alleen beschikbaar op dekstop versie </span>
          )}
        </div>
        {!isMobile && (
          <Button
            variant="secondary"
            as="a"
            href={!isLoggedIn ? permissionsUrl : loginUrl}
          >
            {isLoggedIn ? 'Permissies wijzigen' : 'Inloggen/Registreren'}
          </Button>
        )}
      </Styled.AuthenticationDetails>
    </Styled.LayerDetails>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useCallback } from 'react'
import { Icon } from '@commonground/design-system'

import RadioButton from '../../asset-components/layer-radio-button/LayerRadioButton'
import { useLayers } from '../../../hooks/use-layers'

import { DataLayerProps } from '../../../data/DataLayers'

import AddIcon from '../../../../public/images/add.svg'
import useUser from '../../../hooks/use-user'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import { SublayerDetails } from './SublayerDetails'
import { LayerDetails } from './LayerDetails'
import { LayerAuth } from './LayerAuth'
import { LockIcon } from './LockIcon'
import { PolygonIcon } from './PolygonIcon'
import * as Styled from './Layer.styled'

interface MenuLayerProps {
  dataLayer: Partial<DataLayerProps>
}

// Layer renders the buttons to change the activeLayer, which will be shown on the map
const Layer: FunctionComponent<MenuLayerProps> = ({ dataLayer }) => {
  const {
    activeLayerId,
    setActiveLayerId,
    setActiveSublayerKey,
    activeSublayerKey,
    hasAccess,
  } = useLayers()
  const { isLoggedIn } = useUser()
  const isMobile = useIsMobile()
  const isActive = dataLayer.id === activeLayerId

  // handleClick calls the function to change the active dataLayer for the entire application.
  const handleClick = useCallback(
    (e) => {
      e.preventDefault()

      setActiveLayerId(isActive ? 'layer0' : dataLayer.id)

      if (dataLayer.subLayers?.length) {
        setActiveSublayerKey(dataLayer.subLayers[0].name)
      }

      if (typeof window !== 'undefined') {
        ;(window as any).goatcounter?.count({
          path: (path) => `${dataLayer.name} path: ${path}`,
          event: true,
        })
      }
    },
    [
      isActive,
      dataLayer.id,
      dataLayer.name,
      dataLayer.subLayers,
      setActiveLayerId,
      setActiveSublayerKey,
    ]
  )

  const handleSublayerClick = useCallback(
    (e, input) => {
      e.preventDefault()
      setActiveSublayerKey(isActive ? input : '')
    },
    [setActiveSublayerKey, isActive]
  )

  return (
    <Styled.Layer
      // tabIndex={0}
      key={dataLayer.id}
      className={isActive ? 'active' : ''}
    >
      <Styled.LayerHeading
        className={isActive ? 'active' : ''}
        onClick={handleClick}
      >
        <h3>{dataLayer.name}</h3>
        <div className={isActive ? 'active' : ''}>
          {dataLayer.drawType === 'count' && !isMobile && <PolygonIcon />}
          {dataLayer.drawType === 'findings' && <Icon as={AddIcon} />}

          {hasAccess(dataLayer) ? (
            <RadioButton
              value={dataLayer.id}
              checked={isActive}
              name={dataLayer.name}
              tabIndex={-1}
            />
          ) : (
            <LockIcon />
          )}
        </div>
      </Styled.LayerHeading>

      {isActive && (
        <Styled.LayerContent>
          {hasAccess(dataLayer) ? (
            <>
              {dataLayer.subLayers ? (
                <SublayerDetails
                  dataLayer={dataLayer}
                  subLayers={dataLayer.subLayers}
                  handleSublayerClick={handleSublayerClick}
                  activeSublayerKey={activeSublayerKey}
                />
              ) : (
                <LayerDetails dataLayer={dataLayer} />
              )}
            </>
          ) : (
            <LayerAuth description={dataLayer.desc} isLoggedIn={isLoggedIn} />
          )}
        </Styled.LayerContent>
      )}
    </Styled.Layer>
  )
}

export default Layer

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent } from 'react'
import { Button } from '@commonground/design-system'
import type { DataLayerProps } from '../../../data/DataLayers'
import { parseDate } from '../../../utils/parse-date'
import { RadioButton } from '../../asset-components/radio-button/RadioButton'
import { PolygonIcon } from './PolygonIcon'
import * as Styled from './Layer.styled'

interface LayerDetailsProps {
  dataLayer: Partial<DataLayerProps>
  subLayers: Partial<DataLayerProps['subLayers']>
  handleSublayerClick: any
  activeSublayerKey: string
}
export const SublayerDetails: FunctionComponent<LayerDetailsProps> = (
  props
): JSX.Element => {
  const { dataLayer, subLayers, handleSublayerClick, activeSublayerKey } = props

  const handleClick = () => {
    // TODO: if count on subLayers
  }
  return (
    <Styled.LayerDetails>
      <p>{dataLayer?.desc}</p>
      {dataLayer?.drawType && dataLayer?.drawType === 'count' && (
        <Styled.CustomButton variant="link" size="small" onClick={handleClick}>
          <PolygonIcon inline />
          Gebied analyseren
        </Styled.CustomButton>
      )}
      <Styled.SubLayerDetails>
        {dataLayer.name !== 'Bereidheid Energietransitiemaatregelen' && (
          <p>Kies een andere weergave: </p>
        )}
        {subLayers?.map((sublayer) => {
          return (
            <div
              key={sublayer.name}
              onClick={(e) => handleSublayerClick(e, sublayer.name)}
              style={{ flexDirection: 'column' }}
            >
              <div>
                <RadioButton
                  value={sublayer.name}
                  checked={sublayer.name === activeSublayerKey}
                  onChange={() => ({})}
                  name={sublayer.name}
                />

                <span
                  style={{ fontWeight: '600' }}
                  key={'label' + sublayer.name}
                >
                  {sublayer.name}
                </span>
              </div>
              {sublayer.desc ? (
                <p key={'desc' + sublayer.name}>{sublayer.desc}</p>
              ) : (
                ''
              )}
            </div>
          )
        })}
      </Styled.SubLayerDetails>
      <Styled.MetaDataItem>
        <div>Verwerkt in deze applicatie</div>
        <div>{parseDate(dataLayer?.lastUpdate)}</div>
      </Styled.MetaDataItem>
      <Styled.MetaDataItem>
        <div>Bronnen</div>
        <div>
          {dataLayer?.source}{' '}
          {dataLayer?.referenceDate && (
            <span className="smallGrey">
              {parseDate(dataLayer?.referenceDate)}
            </span>
          )}
        </div>
      </Styled.MetaDataItem>
      {dataLayer?.url && (
        <Styled.MetaDataItem>
          <a href={dataLayer?.url} target="_blank" rel="noreferrer">
            Externe bron
          </a>
        </Styled.MetaDataItem>
      )}
    </Styled.LayerDetails>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Button } from '@commonground/design-system'

export const Layer = styled.div`
  :first-child {
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
  }

  :last-child {
    border-bottom-width: 1px;
  }
  :last-child {
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
  }
  background-color: rgba(245, 245, 245, 1);
  border-color: rgba(189, 189, 189, 1);
  border-width: 1px 1px 0 1px;
  border-style: solid;
  justify-content: flex-start;
  flex-direction: column;
  user-select: none;
  display: flex;
  color: rgba(66, 66, 66, 1);

  &.active {
    background-color: #ffffff;
  }
`

export const LayerHeading = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  color: #757575;

  h3 {
    padding-right: 2rem;
    color: rgb(66, 66, 66);
  }
  &.active {
    color: #ffffff !important;
    background-color: #0b71a1;
  }
  &.active > h3 {
    color: #ffffff;
  }

  div {
    display: flex;
    cursor: pointer;
    align-items: center;
    justify-content: flex-start;
    margin-left: auto;
    gap: 1rem;
  }
  input {
    cursor: pointer;
  }
`

export const LayerContent = styled.div`
  gap: 0.25rem;
  padding: 1rem;
  border-color: rgba(11, 113, 161, 1);
  border-width: 1px;
  width: 100%;
`

export const LayerDetails = styled.div`
  p {
    margin-bottom: 1rem;
  }

  .smallGrey {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
  }
`

export const CustomButton = styled(Button)`
  margin-bottom: 1rem;
  margin-top: 0.25rem;
`

export const MetaDataItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 1rem;

  div:first-child {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    margin-bottom: 0.125rem;
  }

  a {
    color: rgba(37, 99, 235, 1);
  }
`
export const AuthenticationDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 1rem;

  & div {
    display: flex;
    flex-direction: row;
    margin-bottom: 0.75rem;
    margin-top: 0.75rem;
  }

  span {
    margin-left: 0.5rem;
  }
`

export const SubLayerDetails = styled.div`
  display: flex;
  gap: 0.25rem;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  width: 100%;
  margin-bottom: 1rem;
  & div {
    gap: 0.5rem;
    align-items: flex-start;
    cursor: pointer;
    width: 100%;
    display: flex;
    margin: 0.25rem;
  }
`

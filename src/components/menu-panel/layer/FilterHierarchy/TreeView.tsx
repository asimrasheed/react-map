// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useEffect, useState } from 'react'
import { Icon } from '@commonground/design-system'
import { sbiTree } from './section_tree'
import arrow from './arrow-down-s-line.svg'
import * as Styled from './FilterHierarchy.styled'

export const TreeView = ({ mainThemeValue, makeMapFilter }) => {
  interface Tree {
    [id: string]: Partial<TreeNode>
  }

  interface TreeNode {
    id: string
    title: string
    code: string
    children: Partial<TreeNode>
    collapsed: boolean
    hasChildren: boolean
  }
  const [currentTree, setCurrentTree] = useState<Tree>({})
  const [activeChild, setActivechild] = useState('')

  // Set up first tree root
  useEffect(() => {
    if (!mainThemeValue) {
      setCurrentTree({})
      return
    }
    const list = sbiTree[mainThemeValue].children.filter((child) => {
      if (
        child.ParentId.toString() === sbiTree[mainThemeValue].id &&
        child.IsRoot
      ) {
        return child
      }
    })
    list.forEach((element) => {
      setCurrentTree((currentTree) => ({
        ...currentTree,
        [element.Id]: {
          id: element.Id,
          title: element.Title,
          code: element.Code,
          children: {},
          hasChildren: element.HasChildren,
          collapsed: false,
        },
      }))
    })
  }, [mainThemeValue])

  function findVal(object, key) {
    let value
    Object.keys(object).some(function (k) {
      if (k === key) {
        value = object[k]
        return true
      }
      if (object[k] && typeof object[k] === 'object') {
        value = findVal(object[k], key)
        return value !== undefined
      }
    })
    return value
  }

  const addChildren = (parent, parentId) => {
    const childrenList = sbiTree[mainThemeValue].children.filter((child) => {
      if (child.ParentId.toString() === parentId) {
        return child
      }
    })
    parent.children = childrenList.reduce((acc, element) => {
      return {
        ...acc,
        [element.Id]: {
          id: element.Id,
          title: element.Title,
          code: element.Code,
          children: {},
          hasChildren: element.HasChildren,
          collapsed: false,
        },
      }
    }, {})
  }

  const removeChildren = (parent: Partial<TreeNode>) => {
    parent.children = {}
  }

  const onHandleClick = (item: string, e) => {
    e.preventDefault()
    e.stopPropagation()
    const activeChild: Partial<TreeNode> = findVal(currentTree, item)
    activeChild.collapsed = !activeChild.collapsed
    makeMapFilter(activeChild.code)
    setActivechild(item)
    if (activeChild.collapsed) {
      addChildren(activeChild, item)
    } else {
      removeChildren(activeChild)
    }
  }

  const createItemsFromTree = (parent: Partial<TreeNode>) => {
    if (Object.keys(parent.children).length) {
      return Object.entries(parent.children as TreeNode).map(([key, value]) => {
        return (
          <Styled.ResultListItem
            key={key}
            onClick={(e) => onHandleClick(key, e)}
          >
            <Styled.ResultListItemTitle
              key={key}
              className={`${activeChild === key ? 'active' : ''} ${
                value.collapsed ? 'collapsed' : ''
              }`}
            >
              {value.hasChildren && <Icon as={arrow} />}
              <span
                style={{
                  fontWeight: 'bold',
                  backgroundColor: '#ece2cf',
                  padding: '3px',
                  margin: '3px',
                  color: 'rgba(117, 117, 117, 1)',
                }}
              >
                {value.code}
              </span>
              {value.title}
            </Styled.ResultListItemTitle>
            {value.hasChildren && (
              <Styled.ResultList>
                {' '}
                {createItemsFromTree(value)}
              </Styled.ResultList>
            )}
          </Styled.ResultListItem>
        )
      })
    }
  }

  return (
    <Styled.ResultList>
      {currentTree &&
        Object.keys(currentTree).map((key, i) => (
          <Styled.ResultListItem
            onClick={(e) => onHandleClick(key, e)}
            key={i}
            style={{ paddingLeft: '0rem' }}
          >
            <Styled.ResultListItemTitle
              className={`${activeChild === key ? 'active' : ''} ${
                currentTree[key].collapsed ? 'collapsed' : ''
              }`}
            >
              {currentTree[key].hasChildren && <Icon as={arrow} />}
              <span
                style={{
                  fontWeight: 'bold',
                  backgroundColor: '#ece2cf',
                  padding: '3px',
                  margin: '3px',
                  color: 'rgba(117, 117, 117, 1)',
                }}
              >
                {currentTree[key].code}
              </span>
              {currentTree[key].title}
            </Styled.ResultListItemTitle>
            {currentTree[key].hasChildren && (
              <Styled.ResultList>
                {' '}
                {createItemsFromTree(currentTree[key])}
              </Styled.ResultList>
            )}
          </Styled.ResultListItem>
        ))}
    </Styled.ResultList>
  )
}

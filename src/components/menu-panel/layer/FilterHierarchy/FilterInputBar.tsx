// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useCallback, useContext, useEffect, useState } from 'react'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import { useMap } from '../../../../hooks/use-map'
import { useDraw } from '../../../../hooks/use-draw'
import * as Styled from './../../locationNavBar/LocationNavBar.styled'
import { sbiTree } from './section_tree'
import { TreeView } from './TreeView'

export const FilterInputBar = () => {
  const { setCustomFilter } = useMap()
  const { setCustomCountFilter } = useDraw()
  const [displayName, setDisplayName] = useState('')
  const [cursorPosition, setCursorPosition] = useState(-1)
  const [resultList, setResultList] = useState([])
  const [mainThemeValue, setMainThemeValue] = useState('')

  // const getSBItextSearch = async (text: string): Promise<any> => {
  //     try {
  //         const response = await fetch(
  //             `	https://sbi.cbs.nl/CBS.TypeerModule.TypeerServiceWebAPI/api/SBISearch/search/${text}`,
  //         )

  //         if (!response.ok) {
  //             throw new Error(response?.statusText)
  //         }
  //         const sbicodes = await response.json()
  //         return { sbicodes }

  //     } catch (e) {
  //         console.warn(`Could not fetch finding ${text}. Message:`, e)
  //         return null
  //     }
  // }

  // const lookup = (value: string) => {
  //     const i = Object.keys(sbiOverzicht).indexOf(value)
  //     console.log(i)
  //     if (i === -1) {
  //         Promise.all(
  //             [getSBItextSearch(value)]).then((data) => {
  //                 if (data.length >= 1) {
  //                     console.log(data)
  //                     // return setResultList(data)
  //                 }

  //             })

  //     } else {
  //         setResultList(
  //             Object.entries(sbiOverzicht).slice(i, i + 5)
  //         )
  //     }

  // }

  // const suggestService = (value: string) => {
  //     const re = /^[a-zA-Z]/
  //     if (re.test(value) && value.length === 1) {

  //     }
  // }

  const makeMapFilter = (filterValue) => {
    if (!filterValue) {
      return
    }
    const mapFilter: Array<any> = ['any']

    const makeFilter = (attrb: string, value: string) => {
      mapFilter.push(['==', ['get', attrb], value])
    }
    const re = /^[a-zA-Z]/

    if (re.test(filterValue)) {
      if (filterValue.length === 1) {
        makeFilter('l1_code', filterValue.toUpperCase())
      } else if (filterValue.length === 2) {
        return
      } else if (filterValue.length <= 5 && filterValue.length > 2) {
        makeFilter(`l${filterValue.length - 1}_code`, filterValue.substring(1))
      } else {
        makeFilter(`hoofdact`, filterValue.substring(1))
      }
    } else {
      if (filterValue.length === 1) {
        return
      } else if (filterValue.length <= 4 && filterValue.length >= 2) {
        makeFilter(`l${filterValue.length}_code`, filterValue)
      } else {
        makeFilter(`hoofdact`, filterValue)
      }
    }
    setCustomFilter(mapFilter)
    setCustomCountFilter('')
  }

  const handleClear = () => {
    setMainThemeValue('')
    setDisplayName('')
    setResultList([])
    setCustomFilter(null)
    setCustomCountFilter(null)
  }

  const handleChange = async (e) => {
    setDisplayName(e.target.value || '')
    if (!e.target.value?.length) {
      handleClear()
      setResultList(Object.keys(sbiTree))
      // setResultList(Object.keys(sbiTree).reduce((acc, key) => {
      //     acc[key.toLowerCase()] = sbiTree[key];
      //     return acc;
      // },[]) )
    }
  }

  const clearList = useCallback(() => {
    setResultList([])
    setCursorPosition(-1)
  }, [setCursorPosition, setResultList])

  const onHandleClick = (item, e) => {
    e.preventDefault()
    setDisplayName(item + ' ' + sbiTree[item].title || '')
    makeMapFilter(item)
    setMainThemeValue(item)
    clearList()
  }

  const handleKeyDown = useCallback(
    async (e) => {
      if (e.key === 'Enter') {
        e.preventDefault()
      }
      if (resultList.length > 0) {
        switch (e.key) {
          case 'Enter':
            if (cursorPosition >= 0) {
              const item = resultList[cursorPosition]
              onHandleClick(item, e)
              clearList()
              e.target.blur()
            }
            break
          case 'ArrowDown':
            if (resultList.length - 1 > cursorPosition) {
              setCursorPosition(cursorPosition + 1)
            }
            break
          case 'ArrowUp':
            if (cursorPosition !== 0) {
              setCursorPosition(cursorPosition - 1)
            }
            break
          case 'Backspace':
            clearList()
            break
          case 'Escape':
            clearList()
            break
          default:
            break
        }
      }
    },
    [clearList, cursorPosition, resultList]
  )

  useEffect(() => {
    return function cleanup() {
      setCustomFilter(null)
      setCustomCountFilter(null)
    }
  }, [])

  return (
    <Styled.NavBarContainer>
      <Input
        placeholder="Kies een hoofd thema"
        type="text"
        value={displayName}
        onChange={handleChange}
        onFocus={handleChange}
        onKeyDown={handleKeyDown}
        autoComplete="off"
        autoFocus="off"
        spellCheck="false"
        handleClear={handleClear}
        style={{ width: '100%' }}
        maxlength={6}
      />
      {resultList.length ? (
        <Styled.ResultList>
          {resultList.map((item, i) => (
            <Styled.ResultListItem
              className={i === cursorPosition ? 'focus' : ''}
              key={item.code}
              value={item.code}
              onClick={(e) => onHandleClick(item, e)}
            >
              <span
                style={{
                  fontWeight: 'bold',
                  backgroundColor: '#ece2cf',
                  padding: '3px',
                }}
              >
                {item}
              </span>
              {sbiTree[item].title}
            </Styled.ResultListItem>
          ))}
        </Styled.ResultList>
      ) : null}
      {mainThemeValue && (
        <TreeView
          mainThemeValue={mainThemeValue}
          makeMapFilter={makeMapFilter}
        />
      )}
    </Styled.NavBarContainer>
  )
}

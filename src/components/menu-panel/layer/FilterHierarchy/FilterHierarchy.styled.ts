// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const NavBarContainer = styled.div`
  position: relative;
  display: flex;

  flex-direction: column;

  ${mediaQueries.mdDown`
    margin: 1rem 1rem auto 1rem;
  `};
`

export const ResultList = styled.div`
  margin: 0;
  padding: 0;
  // border-width: 1px;
  z-index: 10;
  /* position:absolute; */
  shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  background-color: rgba(255, 255, 255, 1);
  border-color: rgba(158, 158, 158, 1);
  width: 100%;
  margin-top: 0.25rem;
  display: flex;
  flex-direction: column;
`

export const ResultListItem = styled.div`
    border-width: 0px;
    line-height: 1rem;
    padding-left: 1.5rem;
    cursor: pointer;
    width: 100%;
    }
`

export const ResultListItemTitle = styled.div`
  display: flex;
  align-items: center;
  padding: 0.2rem;
  min-height: 1rem;

  > svg {
    transform: rotate(-90deg);
    fill: ${(p) => p.theme.tokens.boxShadow};
  }
  &.collapsed {
    > svg {
      transform: rotate(0deg);
    }
  }

  &:hover {
    background-color: rgba(238, 238, 238, 1);
  }
  &.active {
    background-color: #0b71a1;
    color: white;
  }
`

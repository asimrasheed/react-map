// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent, useState, useEffect } from 'react'

import { Button } from '@commonground/design-system'
import { Checkbox } from '../../asset-components/checkbox/Checkbox'
import { useMap } from '../../../hooks/use-map'
import { useDraw } from '../../../hooks/use-draw'
import * as Styled from './Filter.styled'

interface LayerDetailsProps {
  filterValues: Array<any>
  attrName: string
  filterNames: Array<string>
}

export const CustomFilter: FunctionComponent<LayerDetailsProps> = ({
  filterValues,
  attrName,
  filterNames,
}) => {
  const { setCustomFilter } = useMap()
  const { setCustomCountFilter } = useDraw()

  const [filterElements, setFilterElements] = useState(null)
  const [toggleAll, setToggleAll] = useState(false)

  const makeNewCustomFilter = () => {
    if (!filterElements) {
      return
    }
    const mapFilter: Array<any> = ['any']

    for (const [key, value] of Object.entries(filterElements)) {
      if (value) mapFilter.push(['==', ['to-string', ['get', attrName]], key])
    }
    setCustomFilter(mapFilter)

    let countFilter = ''
    for (const [key, value] of Object.entries(filterElements)) {
      if (value) {
        countFilter = countFilter.concat('')
      } else {
        const n =
          attrName === 'energieklasse_score' ? 'energieklasse' : attrName
        const string = `&!contains-${n}=${key}`
        countFilter = countFilter.concat(string)
      }
    }
    setCustomCountFilter(countFilter)
  }

  const handleChange = (e, value) => {
    setFilterElements(
      Object.assign(filterElements, { [value]: !!e.target.checked })
    )
    makeNewCustomFilter()
  }

  const handleToggle = () => {
    const updatedInputs = filterValues.reduce(
      (acc, value) => ({ ...acc, [value]: toggleAll }),
      filterValues
    )
    setFilterElements(updatedInputs)
    setToggleAll(!toggleAll)
    makeNewCustomFilter()
  }

  useEffect(() => {
    setFilterElements(
      filterValues.reduce((a, element) => ({ ...a, [element]: true }), {})
    )
    return function cleanup() {
      setCustomFilter(null)
      setCustomCountFilter(null)
    }
  }, [])

  useEffect(() => {
    makeNewCustomFilter()
  }, [filterElements])

  return (
    <>
      <div>
        <Button
          variant="secondary"
          key="alloff"
          onClick={handleToggle}
          checked={toggleAll}
        >
          Zet alle waarden {toggleAll ? 'aan' : 'uit'}
        </Button>
      </div>
      <Styled.Fieldset>
        {filterValues?.map((element, i) => (
          <Checkbox
            key={element}
            name={filterNames[i]}
            value={element}
            type="checkbox"
            onChange={(e) => handleChange(e, element)}
            checked={filterElements?.[element] === false ? false : true}
          >
            {filterNames[i]}
          </Checkbox>
        ))}
      </Styled.Fieldset>
    </>
  )
}

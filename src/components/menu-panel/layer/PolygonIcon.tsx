// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Icon } from '@commonground/design-system'
import { FC } from 'react'
import Polygon from './polygon.svg'

type PolygonIconProps = {
  inline?: boolean
}

export const PolygonIcon: FC<PolygonIconProps> = ({
  inline = false,
}): JSX.Element => {
  return (
    <span data-testid="polygonIcon">
      <Icon as={Polygon} inline={inline} />
    </span>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Fieldset = styled.fieldset`
  margin-top: 1rem;
`

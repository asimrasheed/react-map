// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useState } from 'react'
import { Collapse } from 'react-collapse'
import { IconFlippingChevron } from '@commonground/design-system'
import * as Styled from './Tabs.styled'

type TabProps = {
  tab: Record<string, any>
  changeActiveTab: (...args: any[]) => any
  isOpen: boolean
  title: string
  iconSlug: string
  slug: string
  children: React.ReactNode
}

const Tab: FC<TabProps> = ({
  tab,
  changeActiveTab,
  children,
  isOpen = false,
  title,
  iconSlug,
  slug,
}) => {
  const [id] = useState(crypto.randomUUID())

  return (
    <Styled.TabContainer>
      <div
        className="heading"
        aria-haspopup="true"
        aria-expanded={isOpen}
        aria-controls={id}
        aria-label={title}
        role="button"
        onClick={() => changeActiveTab(!isOpen && tab)}
      >
        <img
          src={iconSlug || `/images/categories/${slug}.svg`}
          width="24"
          height="24"
        />
        <h2>{title}</h2>
        <IconFlippingChevron flipHorizontal={isOpen} animationDuration={300} />
      </div>
      <Collapse isOpened={isOpen}>
        <div id={id} className="collapseContent">
          {children}
        </div>
      </Collapse>
    </Styled.TabContainer>
  )
}

export default Tab

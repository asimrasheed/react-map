// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC } from 'react'

import { useIsMobile } from '../../../hooks/use-is-mobile'
import { TabContextProps, TabProps } from '../../../providers/tab-provider'
import Layer from '../layer'
import Tab from './Tab'
import * as Styled from './Tabs.styled'

export interface TabsProps {
  activeTab: { id: string }
  changeActiveTab: (...args: any[]) => any
  tabThemes: TabProps[]
  tabLayers: TabContextProps['tabLayers']
}

const Tabs: FC<TabsProps> = ({
  activeTab,
  changeActiveTab,
  tabThemes,
  tabLayers,
}) => {
  const isMobile = useIsMobile()

  return (
    <Styled.Container>
      <div>
        {tabThemes.map((tab) => (
          <Tab
            isOpen={tab.id === activeTab?.id}
            iconSlug={tab.iconSlug}
            tab={tab}
            key={tab.id}
            slug={tab.id}
            title={tab.title}
            changeActiveTab={changeActiveTab}
          >
            {!isMobile && (
              <>
                <p>{tab.description}</p>
                <h3 className="headingSmall">Beschikbare kaartlagen:</h3>
              </>
            )}

            {tabLayers.reduce(
              (acc, dataLayer) =>
                dataLayer.theme === tab.id
                  ? [
                      ...acc,
                      <Layer key={dataLayer.name} dataLayer={dataLayer} />,
                    ]
                  : acc,
              []
            )}
          </Tab>
        ))}
      </div>
    </Styled.Container>
  )
}

export default Tabs

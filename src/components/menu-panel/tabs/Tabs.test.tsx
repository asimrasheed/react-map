// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '@commonground/design-system'
import { render, screen } from '@testing-library/react'
import { LayerProvider } from '../../../providers/layer-provider'
import Tabs from './Tabs'

const tabs = [
  {
    id: 'id1',
    title: 'Title 1',
    description: 'Description 1',
  },
  {
    id: 'id2',
    title: 'Title 2',
    description: 'Description 2',
  },
]
const layers = [
  {
    id: 'Layer 1',
    name: 'Layer 1',
    theme: 'id1',
  },
  {
    id: 'Layer 2',
    name: 'Layer 2',
    theme: 'id1',
  },
  {
    id: 'Layer 3',
    name: 'Layer 3',
    theme: 'id2',
  },
  {
    id: 'Layer 4',
    name: 'Layer 4',
    theme: 'id2',
  },
  {
    id: 'Layer 5',
    name: 'Layer 5',
    theme: 'id3',
  },
  {
    id: 'Layer 6',
    name: 'Layer 6',
    theme: 'id3',
  },
]

const setup = (overrides?: any) => {
  const crypto = require('crypto')
  global.crypto = crypto
  render(
    <ThemeProvider theme={defaultTheme}>
      <LayerProvider>
        <Tabs
          activeTab={{ id: 'id1' }}
          changeActiveTab={() => null}
          tabThemes={tabs}
          tabLayers={layers}
          {...overrides}
        />
      </LayerProvider>
    </ThemeProvider>
  )
}

describe('Tabs', () => {
  it('should render tabs', () => {
    setup()
    expect(screen.getByRole('button', { expanded: true, name: tabs[0].title }))
    expect(screen.getByRole('button', { expanded: false, name: tabs[1].title }))
    expect(screen.getByText(tabs[0].description)).toBeInTheDocument()
    expect(screen.getByText(tabs[1].description)).toBeInTheDocument()
  })

  it('should expand the selected tab', () => {
    setup({ activeTab: { id: 'id2' } })
    expect(screen.getByRole('button', { expanded: false, name: tabs[0].title }))
    expect(screen.getByRole('button', { expanded: true, name: tabs[1].title }))
  })

  it('should render layers', () => {
    setup()
    expect(screen.getByText(layers[0].name)).toBeInTheDocument()
    expect(screen.getByText(layers[1].name)).toBeInTheDocument()
    expect(screen.getByText(layers[2].name)).toBeInTheDocument()
    expect(screen.getByText(layers[3].name)).toBeInTheDocument()
    expect(screen.queryByText(layers[4].name)).not.toBeInTheDocument()
    expect(screen.queryByText(layers[5].name)).not.toBeInTheDocument()
  })
})

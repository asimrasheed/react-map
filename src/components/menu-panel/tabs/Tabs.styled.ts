// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Container = styled.div`
  display: block;
  ${mediaQueries.mdDown`
    background: ${({ theme }) => theme.tokens.colorBackground};
    padding: 0 1rem ;
    overflow-y: scroll;
    overflow-x: hidden;

    > div {
      box-sizing: content-box;
      height: 30vh;
    }
    `}

  ${mediaQueries.lgUp`
    padding-top: 1.5rem;
    padding-bottom: 6rem;
  `};
`

export const TabContainer = styled.div`
  border-color: rgba(189, 189, 189, 1);
  border-top-width: 1px;

  :last-child {
    border-bottom-width: 1px;
  }
  > .heading {
    padding-top: 1rem;
    padding-bottom: 1rem;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
    gap: 0.5rem;
    justify-content: space-evenly;
    align-items: center;
    cursor: pointer;
    width: 100%;
    display: flex;

    > h2 {
      font-weight: 700;
      font-size: 1.125rem;
      line-height: 1.5rem;
      flex: 1 1 0%;
      height: 1.5rem;
      color: #212121;
    }
  }
  .headingSmall {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    padding-top: 1rem;
  }

  & .collapseContent {
    padding-left: 2.5rem;
    padding-bottom: 1.5rem;
    padding-right: 0.5rem;
  }

  @media screen and (max-width: 1280px) {
    & .collapseContent {
      padding-left: 1.5rem;
    }
  }

  :last-child {
    /* border */
  }
`

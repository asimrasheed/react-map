// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Container = styled.header`
  display: grid;
  grid-template-columns: 33px 1fr;
  grid-template-rows: repeat(2, 1fr);
  column-gap: 1rem;

  > p {
    color: ${({ theme }) => theme.tokens.colorPaletteGray600};
    font-size: ${({ theme }) => theme.tokens.fontSizeSmall};
  }

  ${mediaQueries.mdDown`
    background: ${({ theme }) => theme.tokens.colorBackground};
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.19);
    padding: 0.5rem;
    width: 100vw;
  `}

  ${mediaQueries.lgUp`  
    padding-bottom: 2rem;
  `};

  & img {
    align-self: center;
    grid-row: span 2 / span 2;
  }
  & h1 {
    font-size: 1.125rem;
    line-height: 1.5rem;
  }
  & p {
    line-height: 1.5rem;
  }
`

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC } from 'react'
import { IconFlippingChevron } from '@commonground/design-system'

import { useIsMobile } from '../../hooks/use-is-mobile'
import { TabContextProps, TabProps } from '../../providers/tab-provider'
import { LocationNavBar } from './locationNavBar/LocationNavBar'
import Header from './header/Header'
import Tabs from './tabs/Tabs'
import * as Styled from './MenuPanel.styled'

export type MenuPanelProps = {
  activeTab: TabProps
  tabLayers: TabContextProps['tabLayers']
  changeActiveTab: (...args: any[]) => any
  tabThemes: TabProps[]
  isCollapsed?: boolean
  setIsCollapsed?: React.Dispatch<React.SetStateAction<boolean>>
  iconSlug?: string
}

const MenuPanel: FC<MenuPanelProps> = ({
  activeTab,
  changeActiveTab,
  tabLayers,
  tabThemes,
  isCollapsed = false,
  setIsCollapsed,
}) => {
  const isMobile = useIsMobile()

  return (
    <Styled.Container>
      <Header />
      <LocationNavBar />

      {isMobile && (
        <Styled.CollapseButton
          isCollapsed={isCollapsed}
          onClick={() => setIsCollapsed(!isCollapsed)}
        >
          <IconFlippingChevron />
        </Styled.CollapseButton>
      )}
      {(!isMobile || !isCollapsed) && (
        <Tabs
          activeTab={activeTab}
          changeActiveTab={changeActiveTab}
          tabThemes={tabThemes}
          tabLayers={tabLayers}
        />
      )}
    </Styled.Container>
  )
}

export default MenuPanel

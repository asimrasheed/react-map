// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Container = styled.div`
  position: relative;
  overflow-y: auto;
  box-shadow: auto;
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
  z-index: 10;
  box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
  padding: 2rem 2rem 5rem 2rem;
  width: auto;
  @media screen and (max-width: 1280px) {
    padding: 2rem 1.3rem 5rem 1.3rem;
  }

  /* NOTE: fill-available in combination with 'position:fixed' fills the entire screen on mobile */
  ${mediaQueries.mdDown`

    height: fill-available;
    position: fixed;

    padding: 0;
      pointer-events: none;
    > * {
      pointer-events: initial;
    }
    
    button:last-of-type {
      margin-top: auto;
    }
  `}

  @media screen and (max-width: 992px) {
    position: fixed;
    height: 100vh;

    button:last-of-type {
      margin-top: auto;
    }
  }
`

export const CollapseButton = styled.button<any>`
  background: ${({ theme }) => theme.tokens.colorPaletteGray100};
  height: 2.5rem;
  top: ${(p) => (p.isCollapsed ? 300 : 100)}px;
  width: 100vw;

  > svg {
    margin: auto;
    transform: ${(p) => (p.isCollapsed ? 'scaleY(-1)' : 'scaleY(1)')};
  }
`

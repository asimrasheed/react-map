// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, useCallback, useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import { setURLParams, UrlParamsKey } from '../../../utils/url-params'
import { MapContext } from '../../../providers/map-provider'
import {
  fetchLocations,
  lookupLocation,
} from '../../../utils/map/lookup-location'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import { ToasterContext } from '../../../providers/toaster-provider'
import SearchIcon from './search.svg'
import * as Styled from './LocationNavBar.styled'

export const LocationNavBar = () => {
  const router = useRouter()
  const { query } = router || {}
  const queryParam = String(query?.query || '')

  const [displayName, setDisplayName] = useState(queryParam)
  const [resultList, setResultList] = useState([])
  const [cursorPosition, setCursorPosition] = useState(-1)

  const { showToast } = useContext(ToasterContext)
  const { setSearchGeometry } = useContext(MapContext)
  const isMobile = useIsMobile()

  const handleClear = () => {
    setSearchGeometry('')
    setDisplayName('')
    setResultList([])
    setURLParams(UrlParamsKey.Query, null)
  }

  const handleChange = async (e) => {
    setDisplayName(e.target.value || '')

    if (!e.target.value?.length) {
      handleClear()
    }

    if (e.target.value.length >= 2) {
      const maxResults = isMobile ? 5 : 8
      const locations = await fetchLocations(
        e.target.value,
        `rows=${maxResults}&`
      )

      if (!locations?.response.docs) {
        return
        //   showToast({
        //     title: 'Service om locaties op te halen is momenteel offline',
        //     variant: 'error',
        //   })
        //   return
      }

      setResultList(locations.response.docs)
    }
  }

  const clearList = useCallback(() => {
    setResultList([])
    setCursorPosition(-1)
  }, [setCursorPosition, setResultList])

  const handleLookup = async (item) => {
    const geocodeResult = await lookupLocation(item)

    if (!geocodeResult) {
      return
    }
    setSearchGeometry(geocodeResult)
    clearList()
  }

  const onHandleClick = (item, e) => {
    e.preventDefault()
    setDisplayName(item.weergavenaam || '')
    setURLParams(UrlParamsKey.Query, item.weergavenaam || '')
    handleLookup(item)
    clearList()
  }

  const handleKeyDown = useCallback(
    async (e) => {
      if (e.key === 'Enter') {
        e.preventDefault()
      }
      if (resultList.length > 0) {
        switch (e.key) {
          case 'Enter':
            if (cursorPosition >= 0) {
              const item = resultList[cursorPosition]
              onHandleClick(item, e)
              clearList()
              e.target.blur()
            }
            break
          case 'ArrowDown':
            if (resultList.length - 1 > cursorPosition) {
              setCursorPosition(cursorPosition + 1)
            }
            break
          case 'ArrowUp':
            if (cursorPosition !== 0) {
              setCursorPosition(cursorPosition - 1)
            }
            break
          case 'Backspace':
            clearList()
            break
          case 'Escape':
            clearList()
            break
          default:
            break
        }
      }
    },
    [clearList, cursorPosition, resultList]
  )

  // Alleen voor router url
  useEffect(() => {
    setDisplayName(queryParam)
  }, [queryParam])

  useEffect(() => {
    if (queryParam) {
      ;(async () => {
        const locations = await fetchLocations(queryParam)

        if (!location) {
          console.warn('Suggested location query not found')
          return
        }
        const geocodeResult = await lookupLocation(
          locations?.response.docs?.[0]
        )

        if (!geocodeResult) {
          console.warn('Suggested location not found')
          return
        }
        setSearchGeometry(geocodeResult)
      })()
    }
  }, [queryParam, setSearchGeometry])

  return (
    <Styled.NavBarContainer>
      <Input
        placeholder="Zoek uw locatie hier"
        type="text"
        value={displayName}
        onChange={handleChange}
        onFocus={handleChange}
        onKeyDown={handleKeyDown}
        autoComplete="off"
        autoFocus="off"
        icon={SearchIcon}
        spellCheck="false"
        handleClear={handleClear}
        style={{ width: '100%' }}
      />
      {resultList.length ? (
        <Styled.ResultList>
          {resultList.map((item, i) => (
            <Styled.ResultListItem
              className={i === cursorPosition ? 'focus' : ''}
              key={item.id}
              value={item.id}
              onClick={(e) => onHandleClick(item, e)}
            >
              <span
                style={{
                  fontWeight: 'bold',
                  backgroundColor: '#ece2cf',
                  padding: '3px',
                }}
              >
                {item.type}
              </span>{' '}
              {item.weergavenaam}
            </Styled.ResultListItem>
          ))}
        </Styled.ResultList>
      ) : null}
    </Styled.NavBarContainer>
  )
}

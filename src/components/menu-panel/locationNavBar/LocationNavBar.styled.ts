// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const NavBarContainer = styled.div`
  position: relative;

  ${mediaQueries.mdDown`
    margin: 1rem 1rem auto 1rem;
  `};
`

export const ResultList = styled.ul`
  max-height: 400px;
  list-style: none;
  margin: 0;
  padding: 0;
  border-width: 1px;
  z-index: 10;
  position: absolute;
  shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  background-color: rgba(255, 255, 255, 1);
  border-color: rgba(158, 158, 158, 1);
  width: 100%;
  margin-top: 0.25rem;
  overflow: hidden;
  overflow-y: scroll;
`

export const ResultListItem = styled.li`
  border-width: 0px;
  line-height: 1.5rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  padding-left: 1rem;
  padding-right: 1rem;
  border-color: rgba(238, 238, 238, 1);
  cursor: pointer;
  width: 100%;
  min-height: 3rem;
  border-bottom-width: 1px;

  &:hover {
    background-color: rgba(238, 238, 238, 1);
  }

  &.focus {
    background-color: rgba(238, 238, 238, 1);
  }
`

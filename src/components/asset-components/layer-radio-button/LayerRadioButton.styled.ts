// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Label = styled.label`
  display: flex;
  cursor: pointer;
  width: 100%;
`

export const RadioButton = styled.input`
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-radius: 50%;
  width: 16px;
  height: 16px;
  border: 2px solid var(--colorPaletteGray500);
  transition: 0.2s all linear;
  margin: auto;
  :checked {
    border: 6px solid var(--colorBackground);
    background-color: var(--colorInfo);
  }
`

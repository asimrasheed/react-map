// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, InputHTMLAttributes } from 'react'
import * as Styled from './LayerRadioButton.styled'

// A reusable radio button in the styling of the CommonDataFactory
const RadioButton: FC<InputHTMLAttributes<HTMLInputElement>> = ({
  name,
  value,
  checked,
  onChange,
  ...rest
}) => {
  return (
    <Styled.Label htmlFor={name}>
      <Styled.RadioButton
        id={name}
        name={name}
        value={value}
        type="radio"
        checked={checked}
        onChange={onChange}
        readOnly
        {...rest}
      />
    </Styled.Label>
  )
}

export default RadioButton

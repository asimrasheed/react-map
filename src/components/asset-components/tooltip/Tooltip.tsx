// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, ReactNode } from 'react'

import * as Styled from './Tooltip.styled'

interface TooltipProps {
  tooltipText: string
  position?: 'top' | 'bottom'
  children: ReactNode
}
export const TooltipComp: FunctionComponent<TooltipProps> = (props) => {
  const [hover, setHover] = useState(false)

  return (
    <>
      <Styled.TooltipWrapper
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        {props.children}
      </Styled.TooltipWrapper>

      {hover && (
        <Styled.Tooltip
          style={hover ? { visibility: 'visible' } : { visibility: 'hidden' }}
          className={props.position}
        >
          <p> {props.tooltipText}</p>
        </Styled.Tooltip>
      )}
    </>
  )
}

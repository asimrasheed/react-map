// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Label = styled.label`
  cursor: pointer;
  line-height: 1;
  transition: 180ms all ease-in-out;
  opacity: 0.8;
  margin-top: 4px;
  @supports (-moz-appearance: none) {
    margin-top: 1px;
  }
`

export const Text = styled.span`
  margin-left: 0.5em;
`

export const RadioButton = styled.input`
  cursor: pointer;
  margin: 0;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-radius: 50%;
  width: 16px;
  height: 16px;
  border: 2px solid var(--colorPaletteGray500);
  transition: 0.2s all linear;
  box-shadow: inset 0em 0em #0b71a1;
  margin: auto;

  transition: 180ms transform ease-in-out;
  :checked {
    box-shadow: inset 0px 0px 0px 2px #fff;
    background-color: #0b71a1;
  }
`

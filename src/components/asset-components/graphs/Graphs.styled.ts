// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const GraphDiv = styled.div`
  p {
    color: var(--colorPaletteGray700);
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 0.875rem;
    margin: 0;
  }

  .dataElement {
    padding-top: 2px;
    padding-right: 5px;
  }

  .dataElement > div.rectangle {
    display: flex;
    padding-right: 10px;
  }
  .dataElement > div.rectangle > p {
    color: #f5f5f5;
    margin: auto 0 auto auto;
    font-weight: bold;
  }

  .dataElement > .title {
    text-transform: lowercase;
  }

  .dataElement > .title::first-letter {
    text-transform: uppercase;
  }
`

export const GraphSvg = styled.svg`
  text {
    fill: var(--colorPaletteGray700);
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 0.75rem;
    margin: 0;
  }
  line {
    stroke: var(--colorPaletteGray700);
  }
  path {
    stroke: var(--colorPaletteGray700);
  }

  @media screen and (max-width: 768px) {
    text {
      font-size: 0.7rem;
    }
  }

  .label {
    font-variant-numeric: tabular-nums;
    text-anchor: center;
  }
`

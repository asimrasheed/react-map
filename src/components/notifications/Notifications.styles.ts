// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { Alert as ImportAlert } from '@commonground/design-system'
import styled from 'styled-components'

export const Container = styled.div`
  grid-column: 2;
  grid-row: 1;
  margin: ${(p) => p.theme.tokens.spacing05};
  display: flex;
  flex-direction: column;
  gap: 1rem;
  width: 23rem;
  z-index: 50;

  @media screen and (max-width: 1250px) and (orientation: landscape) {
    width: 20rem;

    p,
    a {
      font-size: 0.9rem;
    }
  }
`

export const Alert = styled(ImportAlert)`
  > * > .description {
    display: block;
    margin-top: 0.5rem;
  }
`

export const Header = styled.span`
  align-items: center;
  display: flex;

  > span {
    font-weight: bold;
  }

  > small {
    margin: 0 8px 0 auto;
    font-size: 0.9rem;
  }

  > svg {
    cursor: pointer;
  }
`

export const Buttons = styled.span`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

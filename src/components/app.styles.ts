// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Container = styled.main`
  display: grid;
  grid-template-columns: 30rem auto;
  grid-template-rows: 10vh 10vh repeat(3, 1fr) 2rem 3rem;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  min-width: 150px;
  min-height: 300px;

  /* intermediate */
  @media screen and (max-width: 1250px) {
    grid-template-columns: 25rem auto;
  }

  /* Klein  */
  ${mediaQueries.mdDown`
    display: block;
  `}
`

export const TopRightContainer = styled.div`
  display: flex;
  gap: ${(p) => p.theme.tokens.spacing05};
  position: absolute;
  pointer-events: none;
  top: ${(p) => p.theme.tokens.spacing05};
  right: ${(p) => p.theme.tokens.spacing05};

  > * {
    pointer-events: initial;
  }
`

export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  height: fit-content;
  gap: ${(p) => p.theme.tokens.spacing05};
`

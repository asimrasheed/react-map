// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import React, { FunctionComponent, useEffect, useRef, useState } from 'react'
import * as d3 from 'd3'
import { sliderBottom } from 'd3-simple-slider'
import { StyleLayer } from '../../data/DataLayers'
import { useMap } from '../../hooks/use-map'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
  firstLabel: string
  lastLabel: string
  average: number
}

const LegendContinuousBar: FunctionComponent<LegendProps> = ({
  styleLayer,
  firstLabel,
  lastLabel,
  average,
}) => {
  const svgBar = useRef(null)

  const [min, setMin] = useState(0)
  const [max, setMax] = useState(0)
  const margin = { top: average ? 24 : 0, right: 0, bottom: 14, left: 0 }
  const barHeight = 12
  const width = 480 - margin.left - margin.right
  const { setCustomFilter } = useMap()

  function translateTickText(
    normalText: string,
    firstLabel: string,
    lastLabel: string,
    parentNode: any
  ) {
    if (!parentNode.previousSibling) {
      if (firstLabel) {
        return `translate( ${firstLabel.length * 3.5},0)`
      } else {
        return `translate( ${normalText.length * 4},0)`
      }
    } else if (!parentNode.nextSibling) {
      if (lastLabel) {
        return `translate( -${lastLabel.length * 3.5},0)`
      } else {
        return `translate( -${normalText.length * 4},0)`
      }
    } else {
      return 'translate(0,0)'
    }
  }

  function setTickText(
    normalText: string,
    firstLabel: string,
    lastLabel: string,
    parentNode: any
  ) {
    if (!parentNode.previousSibling) {
      if (firstLabel) {
        return firstLabel
      } else {
        return normalText
      }
    } else if (!parentNode.nextSibling) {
      if (lastLabel) {
        return lastLabel
      } else {
        return normalText
      }
    } else {
      return normalText
    }
  }

  function setTickAnchor(parentNode: any) {
    if (!parentNode.previousSibling) {
      return 'left'
    } else if (!parentNode.nextSibling) {
      return 'right'
    } else {
      return 'middle'
    }
  }
  // Format decimals and thousands to Dutch style
  d3.formatDefaultLocale({
    decimal: ',',
    thousands: '.',
  })

  useEffect(() => {
    // inital setting up graph
    const svg = d3.select(svgBar.current)

    svg
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('width', width + margin.left + margin.right)
      .attr('height', barHeight + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${width + margin.left + margin.right} ${
          barHeight + margin.top + margin.bottom
        }`
      )

    svg
      .append('g')
      .attr('transform', `translate(${margin.left} , ${margin.top})`)
      .classed('bars', true)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [average])

  useEffect(() => {
    // Update graph
    if (svgBar.current) {
      const range: number[] | string[] = styleLayer.legendStops
      const min = d3.min(styleLayer.legendStops)
      const max = d3.max(styleLayer.legendStops)
      const steps = 60
      const datastep: number = (max - min) / steps
      const widthStep = width / steps
      const data: number[] = d3.range(min, max, datastep)
      const colorScale = styleLayer.colorScale

      // Linear scale for X-axis
      const xScale = d3.scaleLinear().domain(d3.extent(range)).range([0, width])

      // Define the informative ticks
      const xAxis = d3
        .axisBottom()
        .scale(xScale)
        .tickValues(range)
        .tickSize(barHeight)

      const svg = d3.select(svgBar.current)

      svg
        .select('.bars')
        .attr('transform', `translate(${margin.left} , ${margin.top})`)

      svg
        .select('g.bars')
        .selectAll('.bar')
        .data(data, function (d) {
          return d
        })
        .join(
          (enter) => {
            enter
              .append('rect')
              .classed('bar', true)
              .attr('y', 0)
              .attr('x', function (d, i) {
                return i * widthStep
              })
              .attr('width', widthStep)
              .attr('height', barHeight)
              .style('fill', function (d) {
                return colorScale(d)
              })
              .style('fill-opacity', styleLayer.opacity || 1)
          },
          (update) => {
            update
              .attr('x', function (d, i) {
                return i * widthStep
              })
              .style('fill', function (d) {
                return colorScale(d)
              })
              .style('fill-opacity', styleLayer.opacity || 1)
          },
          (exit) => exit.call((exit) => exit.remove())
        )

      // Xaxis
      svg
        .selectAll('g.x.axis')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, ${margin.top} )`)
              .classed('x axis horz', true)
              .call(xAxis)
              .call((g) => g.select('.domain').remove())
              .call((g) =>
                g
                  .selectAll('.tick')
                  .selectAll('text')
                  .attr('transform', function () {
                    return translateTickText(
                      this.textContent,
                      firstLabel,
                      lastLabel,
                      this.parentNode
                    )
                  })
              )
          },
          (update) => {
            update
              .call(xAxis)
              .call((g) =>
                g
                  .attr(
                    'transform',
                    `translate(${margin.left}, ${margin.top} )`
                  )
                  .select('.domain')
                  .remove()
              )
              .call((g) =>
                g
                  .selectAll('.tick')
                  .selectAll('text')
                  .attr('transform', function () {
                    return translateTickText(
                      this.textContent,
                      firstLabel,
                      lastLabel,
                      this.parentNode
                    )
                  })
              )
          },
          (exit) => exit.call((exit) => exit.remove())
        )

      if (firstLabel || lastLabel) {
        svg
          .selectAll('g.x.axis')
          .selectAll('.tick')
          .selectAll('text')
          .text(function (d: string) {
            return setTickText(d, firstLabel, lastLabel, this.parentNode)
          })
          .attr('text-anchor', function () {
            setTickAnchor(this.parentNode)
          })
          .attr('transform', function () {
            return translateTickText(
              this.textContent,
              firstLabel,
              lastLabel,
              this.parentNode
            )
          })
      }

      if (average) {
        svg
          .selectAll('line.average')
          .data([average])
          .join(
            (enter) =>
              enter
                .append('line')
                .classed('average', true)
                .attr('transform', `translate(${margin.left} , 0)`)
                .attr('y1', margin.top / 2)
                .attr('y2', barHeight + margin.top)
                .style('stroke', '#212121')
                .style('stroke-width', 2)
                .attr('x1', xScale(average))
                .attr('x2', xScale(average)),
            (update) =>
              update.call((update) =>
                update
                  .transition()
                  .attr('x1', xScale(average))
                  .attr('x2', xScale(average))
              ),
            (exit) => exit.call((exit) => exit.transition().remove())
          )
        svg
          .selectAll('text.average')
          .data([average])
          .join(
            (enter) =>
              enter
                .append('text')
                .classed('average', true)
                .attr('transform', `translate(${margin.left} ,0)`)
                .attr('text-anchor', 'middle')
                .attr('dy', '-3px')
                .attr('y', 12)
                .attr('x', xScale(average) + 4)
                .text(`Gemiddeld ${average}`),
            (update) =>
              update.call((update) =>
                update
                  .transition()
                  .attr('x', xScale(average) + 4)
                  .text(`Gemiddeld ${average}`)
              ),
            (exit) => exit.call((exit) => exit.transition().remove())
          )
      } else {
        svg.selectAll('line.average').remove()
        svg.selectAll('text.average').remove()
      }

      const slider = sliderBottom(xScale)
        .min(min)
        .max(max)
        .default([min, max])
        .on('onchange', (val) => {
          svg
            .select('g.bars')
            .selectAll('.bar')
            .style('fill-opacity', (d) => {
              if (d > val[1]) return 0.2
              else if (d < val[0]) return 0.2
              else return 1
            })
          setCustomFilter([
            'all',
            ['has', styleLayer.attrName],
            ['>=', styleLayer.attrName, Math.round(val[0])],
            ['<=', styleLayer.attrName, Math.round(val[1])],
          ])
        })

      svg
        .selectAll('g.slider')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('width', width)
              .attr('height', 50)
              .attr('transform', `translate(${margin.left}, ${margin.top} )`)
              .classed('slider', true)
              .call(slider)
          },
          (update) => {
            update.call(slider)
            // .call((g) =>
            // g
            //   .selectAll('.tick')
            //   .selectAll('text')
            //   .remove()
            // )
          },
          (exit) => exit.call((exit) => exit.remove())
        )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [styleLayer, firstLabel, average, lastLabel])

  return (
    <div>
      {/* <input type="range" name="mySlider" id='mySlider' min={min} max={max} /> */}
      {/* <div><svg id="sliderD3" ref={(el) => (svgSlider.current = el)} /></div> */}
      <svg className="continuous" ref={(el) => (svgBar.current = el)} />
    </div>
  )
}

export default LegendContinuousBar

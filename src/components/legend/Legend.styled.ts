// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled, { css } from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

interface ContainerProps {
  orientation: string
}

export const Container = styled.div<ContainerProps>`
  display: flex;
  position: relative;
  flex-direction: column;
  align-content: space-evenly;
  background-color: var(--colorBackgroundAlt);
  padding: var(--spacing05);
  border-radius: 4px;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  margin-bottom: calc(20px + 1rem);
  margin-right: calc(1rem + 36px + 1rem);
  max-height: 90vh;
  max-width: 530px;
  overflow-y: auto;

  grid-column: 2 / span 1;
  grid-row: 6 / span 1;
  /* max-width: 450px; */
  justify-self: end;
  align-self: end;
  z-index: 2;

  ${(p) =>
    p.orientation === 'portrait' &&
    css`
      & .legendInfo {
        margin: 0;
      }
    `}

  ${(p) =>
    p.orientation === 'landscape' &&
    css`
      & .legendInfo {
        display: flex;
        justify-content: flex-start;
        align-items: baseline;
        gap: 1rem;
        margin-bottom: 1rem;
      }
    `}

  & img {
    width: auto;
    height: auto;
  }

  & .legendItem {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 0.5rem;
  }

  & .legendItem div {
    width: 24px;
    height: 16px;
  }
  & .legendItem input {
    width: 24px;
    height: 16px;
    cursor: pointer;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    opacity: 0.2 !important;
  }
  & .legendItem input:hover {
    opacity: 0.2 !important;
  }
  & .legendItem input:checked {
    opacity: 1 !important;
  }
  & .dot {
    height: 20px !important;
    width: 20px !important;
    border-radius: 50%;
  }

  & .legendItem p {
    margin: 0;
  }

  & .continuous g.x.axis.horz g.tick text {
    font-size: 14px;
    font-family: 'Source Sans Pro', sans-serif;
    text-rendering: optimizeLegibility;
    fill: #757575;
  }

  & svg.continuous g.x.axis.horz g.tick line {
    stroke-width: 2px;
    stroke: #ffffff;
  }

  & text.average {
    font-size: 14px;
    font-family: 'Source Sans Pro', sans-serif;
    text-rendering: optimizeLegibility;
    fill: #757575;
  }

  & .legendFootnote {
    width: 250px;
  }

  & .slider line {
    stroke: none;
  }

  @media screen and (max-width: 1250px) {
    max-width: 430px;
    overflow-y: hidden;
    padding: var(--spacing03);

    & svg.continuous {
      width: auto;
    }
    p {
      font-size: 0.9rem;
    }

    & .legendItem div {
      width: 18px;
      height: 12px;
    }
    & .dot {
      height: 12px !important;
      width: 12px !important;
      border-radius: 50%;
    }
  }
`

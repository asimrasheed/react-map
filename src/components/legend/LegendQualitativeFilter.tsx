// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { StyleLayer } from '../../data/DataLayers'
import { useMap } from '../../hooks/use-map'
import { useDraw } from '../../hooks/use-draw'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
  filterValues: Array<any>
  attrName: string
  filterNames: Array<string>
}

const LegendQualitativeFilter: FunctionComponent<LegendProps> = ({
  styleLayer,
  filterValues,
  attrName,
  filterNames,
}) => {
  const { setCustomFilter } = useMap()
  const { setCustomCountFilter } = useDraw()

  const [filterElements, setFilterElements] = useState(null)
  const [toggleAll, setToggleAll] = useState(false)

  const makeNewCustomFilter = () => {
    if (!filterElements) {
      return
    }
    const mapFilter: Array<any> = ['any']

    for (const [key, value] of Object.entries(filterElements)) {
      if (value) mapFilter.push(['==', ['to-string', ['get', attrName]], key])
    }
    setCustomFilter(mapFilter)

    let countFilter = ''
    for (const [key, value] of Object.entries(filterElements)) {
      if (value) {
        countFilter = countFilter.concat('')
      } else {
        let n = ''

        switch (attrName) {
          case 'energieklasse_score':
            n = 'energieklasse'
            break
          case 'elabel_voorlopig':
            n = 'labelscore_voorlopig'
            break
          case 'elabel_definitief':
            n = 'labelscore_definitief'
            break
          default:
            n = attrName
            break
        }

        const string = `&!contains-${n}=${key}`
        countFilter = countFilter.concat(string)
      }
    }
    setCustomCountFilter(countFilter)
  }

  const handleChange = (e, value) => {
    setFilterElements(
      Object.assign(filterElements, { [value]: !!e.target.checked })
    )
    makeNewCustomFilter()
  }

  const handleToggle = () => {
    for (let i = 0; i < filterValues.length; i++) {
      const element = filterValues[i]
      setFilterElements(Object.assign(filterElements, { [element]: toggleAll }))
    }
    setToggleAll(!toggleAll)
    makeNewCustomFilter()
  }

  useEffect(() => {
    setFilterElements(
      filterValues.reduce((a, element) => ({ ...a, [element]: true }), {})
    )
    return function cleanup() {
      setCustomFilter(null)
      setCustomCountFilter(null)
    }
  }, [])

  useEffect(() => {
    if (filterElements) {
      for (let i = 0; i < filterValues.length; i++) {
        const element = filterValues[i]
        setFilterElements(Object.assign(filterElements, { [element]: true }))
      }
      setToggleAll(false)
    }
    makeNewCustomFilter()
  }, [filterElements, filterValues])

  return (
    <>
      {filterValues?.map((element, i) => (
        <div key={element} className="legendItem">
          <input
            key={element}
            onClick={(e) => handleChange(e, element)}
            type="checkbox"
            style={{
              backgroundColor: (styleLayer as any).colorScale(element),
              opacity: styleLayer.opacity || 1,
            }}
            checked={filterElements?.[element]}
          />
          <label>{filterNames[i]}</label>
        </div>
      ))}
      <div className="legendFootnote">
        <p style={{ fontSize: '0.9em', fontStyle: 'italic' }}>
          Klik op een legenda item om te filteren of{' '}
          <span
            style={{ textDecoration: 'underline', cursor: 'pointer' }}
            onClick={handleToggle}
          >
            {' '}
            zet alle waarden {toggleAll ? 'aan' : 'uit'}{' '}
          </span>
        </p>
      </div>
    </>
  )
}

export default LegendQualitativeFilter

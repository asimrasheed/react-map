// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { DataLayerProps, StyleLayer } from '../../data/DataLayers'
import LegendQualitative from './LegendQualitative'
import LegendQualitativeFilter from './LegendQualitativeFilter'
import LegendContinuousBar from './LegendContinuousBar'
import LegendWMSImage from './LegendWMSImage'
import LegendCircle from './LegendCircle'
import * as Styled from './Legend.styled'

interface LegendProps {
  activeDataLayer: Partial<DataLayerProps>
}

const Legend: FunctionComponent<LegendProps> = ({ activeDataLayer }) => {
  const [legendOrientation, setLegendOrientation] = useState('')
  const [styleLayer, setStylelayer] = useState<
    undefined | Partial<StyleLayer>
  >()

  useEffect(() => {
    const layer = activeDataLayer?.styleLayers?.find(
      (currentLayer) => currentLayer.id !== 'layer0'
    )
    setStylelayer(layer || null)
  }, [activeDataLayer.styleLayers])

  useEffect(() => {
    if (styleLayer) {
      if (
        styleLayer.geomType === 'raster' ||
        styleLayer.expressionType === 'step' ||
        styleLayer.expressionType === 'match' ||
        styleLayer.expressionType === 'case'
      ) {
        setLegendOrientation('portrait')
      } else {
        setLegendOrientation('landscape')
      }
    }
  }, [styleLayer])

  return (
    <>
      {styleLayer && Object.keys(styleLayer).length && !styleLayer.noLegend && (
        <Styled.Container orientation={legendOrientation}>
          <div className="legendInfo">
            <p>{activeDataLayer.unit}</p>
            <small>{activeDataLayer.scale}</small>
          </div>

          {styleLayer.geomType === 'raster' ? (
            <LegendWMSImage image={styleLayer.image} />
          ) : styleLayer.geomType === 'circle' &&
            styleLayer.expressionType === 'match' &&
            activeDataLayer.filterValues ? (
            <LegendQualitativeFilter
              styleLayer={styleLayer}
              filterValues={activeDataLayer.filterValues}
              filterNames={activeDataLayer.filterNames}
              attrName={activeDataLayer.filterAttrb}
            />
          ) : (styleLayer.geomType === 'circle' &&
              styleLayer.expressionType === 'match') ||
            styleLayer.expressionType === 'bevindingen' ? (
            <LegendCircle styleLayer={styleLayer} />
          ) : styleLayer.expressionType === 'interpolate' ? (
            <LegendContinuousBar
              firstLabel={styleLayer.firstLabel}
              lastLabel={styleLayer.lastLabel}
              styleLayer={styleLayer}
              average={activeDataLayer.average}
            />
          ) : styleLayer.expressionType === 'match' &&
            activeDataLayer.filterValues ? (
            <LegendQualitativeFilter
              styleLayer={styleLayer}
              filterValues={activeDataLayer.filterValues}
              filterNames={activeDataLayer.filterNames}
              attrName={activeDataLayer.filterAttrb}
            />
          ) : styleLayer.expressionType === 'step' ||
            styleLayer.expressionType === 'case' ||
            styleLayer.expressionType === 'match' ? (
            <LegendQualitative styleLayer={styleLayer} />
          ) : null}
        </Styled.Container>
      )}
    </>
  )
}

export default Legend

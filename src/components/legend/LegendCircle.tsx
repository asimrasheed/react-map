// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import * as d3 from 'd3'
import { StyleLayer } from '../../data/DataLayers'
import { useMap } from '../../hooks/use-map'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
}

interface legendItems {
  backgroundColor: string
  color: string
  label: string
}

const LegendCircle: FunctionComponent<LegendProps> = ({ styleLayer }) => {
  const [legendItems, setLegendItems] = useState<legendItems[]>([])
  const { customFilter } = useMap()

  useEffect(() => {
    const colorRange = styleLayer.colorScale.domain()
    const legendItems = styleLayer.legendStops
      .map((item, i) => ({
        label: item,
        color: d3
          .color((styleLayer as any).colorScale(colorRange[i]))
          .darker(0.7)
          .toString(),
        backgroundColor: d3
          .color((styleLayer as any).colorScale(colorRange[i]))
          .copy({ opacity: styleLayer.opacity || 0.8 })
          .toString(),
      }))
      .filter((item) =>
        customFilter
          ? customFilter?.some((filter) => item.label.slice(0, 1) === filter[2])
          : item
      )
    setLegendItems(legendItems)
  }, [styleLayer, customFilter])

  return (
    <>
      {legendItems.map((item) => (
        <div key={item.label} className="legendItem">
          <div
            className="dot"
            data-testid="color"
            style={{
              backgroundColor: item.backgroundColor,
              border: `2px solid ${item.color}`,
              flexShrink: 0,
            }}
          />
          <p>{item.label}</p>
        </div>
      ))}
    </>
  )
}

export default LegendCircle

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useRef, useState, useEffect, FC } from 'react'
import maplibregl from 'maplibre-gl'

import 'maplibre-gl/dist/maplibre-gl.css'
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css'

import { sources } from '../../data/TileSources'
import { useDraw } from '../../hooks/use-draw'
import useUser from '../../hooks/use-user'
import { useMap } from '../../hooks/use-map'

import { getFeatureInfoWMS } from '../../utils/map/get-wms-feature-info'
import { LayerContextProps } from '../../providers/layer-provider'
import { StyleLayer } from '../../data/DataLayers'
import { MapConfiguration } from './map-configuration'
import MapPopup from './MapPopup'
import MapContextLayers from './MapContextLayers'
import MapRelevantLayers from './MapRelevantLayers'

import * as Styled from './map.styles'
import { MapDrawTool } from './MapDrawTool'

const drawerWidth = 553
const modalWidth = 498

interface MapProps {
  activeDataLayer: LayerContextProps['activeDataLayer']
  isCollapsed: boolean
}

interface SelectedFeature {
  firstFeature: mapboxgl.EventData | Record<string, never>
  event: {
    lngLat: maplibregl.LngLat
    type: string
  }
  relevantStyleLayer
}
const Map: FC<MapProps> = ({ activeDataLayer, isCollapsed }) => {
  const {
    setMapLoading,
    mapTheme,
    threeDimensional,
    relevantMapLayerId,
    setAnalyzeMode,
    analyzeMode,
    selectedFeatureAndEvent,
    setSelectedFeatureAndEvent,
    reloadFindingsTiles,
    setReloadFindingsTiles,
  } = useMap()
  const { drawMode } = useDraw()
  const user = useUser()

  const mapContainer = useRef(null)
  const [map, setMap] = useState<maplibregl.Map>(null)
  const [customSourcesAdded, setCustomSourcesAdded] = useState(false)
  const [warning, setWarning] = useState<any>()
  const [popupContext, setPopupContext] = useState({})
  const [relevantStyleLayer, setRelevantStyleLayer] = useState(
    {} as Partial<StyleLayer> | Record<string, never>
  )

  const [previousFeatureId, setPreviousFeatureId] = useState()
  const [previousHighlight, setPreviousHighlight] = useState<SelectedFeature>()
  const [highlightFeature, setHighlightFeature] = useState<SelectedFeature>()

  const removeAllFeatureStates = (
    source: string,
    sourceLayer: string,
    activeDataLayer: any
  ) => {
    map.removeFeatureState({
      source: source,
      sourceLayer: sourceLayer,
    })

    if (activeDataLayer.highlight === 'postcode') {
      map.removeFeatureState({
        source: 'postcode',
        sourceLayer: 'kv_pc6group_2021',
      })
    }
  }

  // get the relevant StyleLayer
  useEffect(() => {
    if (activeDataLayer?.styleLayers?.[0]) {
      const layer = activeDataLayer.styleLayers.find(
        (currentLayer) => currentLayer.id !== 'layer0'
      )
      setRelevantStyleLayer(layer)
    }
  }, [activeDataLayer, activeDataLayer.styleLayers])

  // 1. INIT map
  useEffect(() => {
    if (!maplibregl.supported()) {
      alert(
        'WebGL is not supported. Either enable this in your browser or use a more recent version of your browser.'
      )
      return
    }

    const initMap = new maplibregl.Map({
      container: mapContainer.current,
      style: mapTheme as any,
      center: [4.308188, 52.087049],
      zoom: 16.5,
      bearing: 0,
      pitch: 0,
      maxBounds: [
        [1.5, 50],
        [8.6, 54.2],
      ],
      hash: true,
      attributionControl: false,
      maxZoom: 22,
      // resize: true,
      locale: {
        'NavigationControl.ZoomIn': 'Zoom in',
        'NavigationControl.ZoomOut': 'Zoom uit',
        'NavigationControl.ResetBearing': 'Draai de kaart terug op het Noorden',
        'FullscreenControl.Enter': 'Volledig scherm',
        'FullscreenControl.Exit': 'Exit volledig scherm',
      },
      transformRequest: (url) => {
        const envUrl =
          process.env.NEXT_PUBLIC_ENV === 'prod'
            ? 'https://auth'
            : 'https://acc.auth'

        if (url.startsWith(envUrl)) {
          return {
            url,
            credentials: 'include', // Include cookies for cross-origin requests
          }
        }
      },
    })

    // Attribution control
    initMap.addControl(
      new maplibregl.AttributionControl({
        customAttribution: "<a href='https://commondatafactory.nl/'> CDF</a>",
      }),
      'bottom-right'
    )
    // Navigation control
    initMap.addControl(new maplibregl.NavigationControl({}), 'bottom-right')
    // Scale bar control
    initMap.addControl(
      new maplibregl.ScaleControl({
        maxWidth: 180,
        unit: 'metric',
      }),
      'bottom-left'
    )
    // Fullscreen control
    initMap.addControl(
      new maplibregl.FullscreenControl({
        container: document.querySelector('App'),
      }),
      'bottom-right'
    )

    const handleRender = (e) => {
      setMapLoading(true)
    }

    const handleIdle = (e) => {
      setMapLoading(false)
    }

    const handleLoad = (e) => {
      setMap(initMap as maplibregl.Map)
      initMap.resize()
    }

    const handleError = (e) => {
      if (process.env.NEXT_PUBLIC_ENV === 'dev') {
        console.warn('handleError', e)
      }

      if (e?.source?.type === 'raster') {
        setWarning({
          title: 'Fout bij het ophalen van de kaart',
          description:
            'De data voor deze kaart is momenteel niet volledig beschikbaar',
        })
      }
    }

    initMap.on('load', handleLoad)
    initMap.on('render', handleRender)
    initMap.on('idle', handleIdle)
    initMap.on('error', handleError)

    return () => {
      initMap.off('load', handleLoad)
      initMap.off('render', handleRender)
      initMap.off('idle', handleIdle)
      initMap.off('error', handleError)
    }
  }, [])

  // Werkt niet meer want relevant is verdwenen.
  useEffect(() => {
    setWarning(null)
  }, [relevantMapLayerId])

  // map.on mouse events
  useEffect(() => {
    if (!map || !relevantMapLayerId) {
      return
    }

    const handleClick = (event) => {
      event.originalEvent.cancelBubble = true
      if (activeDataLayer.highlight === 'postcode') {
        const clickedFeatures = map.queryRenderedFeatures(
          [
            [event.point.x - 250 / 2, event.point.y - 250 / 2], // geen meters?
            [event.point.x + 250 / 2, event.point.y + 250 / 2],
          ],
          { layers: ['postcode:kv_pc6group_2021:line:1'] }
        )
        const activePostalcode = clickedFeatures.find((layer) => layer.state)
        Object.assign(event.features?.[0].properties, {
          postcodes: activePostalcode.properties.postcodes,
        })
      }

      setSelectedFeatureAndEvent({
        event: {
          lngLat: event.lngLat,
          type: event.type,
        },
        firstFeature: event.features?.[0],
        features: event.features,
        relevantStyleLayer,
      })

      const offset =
        analyzeMode || activeDataLayer.theme === 'bevindingen' ? modalWidth : 0

      map.easeTo({
        center: [event.lngLat.lng, event.lngLat.lat],
        padding: { right: offset },
      })
    }

    const handleAllClicks = (event) => {
      if (event.originalEvent.cancelBubble) {
        return
      }

      if (analyzeMode !== 'findings') {
        setAnalyzeMode(null)
      }

      if (
        relevantStyleLayer?.geomType === 'raster' &&
        relevantStyleLayer.featureRequest
      ) {
        const bounds = event.lngLat.toBounds(1)
        getFeatureInfoWMS(bounds, relevantStyleLayer.featureRequest).then(
          (info) => {
            if (info) {
              setSelectedFeatureAndEvent({
                event: {
                  lngLat: event.lngLat,
                  type: event.type,
                },
                firstFeature: info,
                features: [info],
                relevantStyleLayer,
              })
            }
          }
        )
      }

      if (relevantStyleLayer?.geomType !== 'raster') {
        // remove all highlights on empty click
        removeAllFeatureStates(
          relevantStyleLayer.tileSource.source,
          relevantStyleLayer['source-layer'] ||
            relevantStyleLayer.tileSource['source-layer'],
          activeDataLayer
        )
      }
    }

    const handleMouseOver = (event) => {
      map.getCanvas().style.cursor = 'pointer'
    }

    const handleMouseMove = (event: maplibregl.MapLayerMouseEvent) => {
      if (event.features?.[0]) {
        setHighlightFeature({
          firstFeature: event.features?.[0],
          event: {
            lngLat: event.lngLat,
            type: event.type,
          },
          relevantStyleLayer,
        })
      }
    }

    const handleMouseOut = (event) => {
      map.getCanvas().style.cursor = 'unset'
      setHighlightFeature(null)
      setPopupContext({})
    }

    const handleMouseLeave = (event) => {
      map.getCanvas().style.cursor = 'unset'
      setHighlightFeature(null)
      setPreviousFeatureId(null)
    }

    if (
      !drawMode &&
      (!analyzeMode ||
        analyzeMode === 'detail' ||
        analyzeMode === 'buildingDetail' ||
        analyzeMode === 'findings')
    ) {
      map.on('click', relevantMapLayerId, handleClick)
      map.on('click', handleAllClicks)
      map.on('mouseover', relevantMapLayerId, handleMouseOver)
      map.on('mousemove', relevantMapLayerId, handleMouseMove)
      map.on('mouseleave', relevantMapLayerId, handleMouseLeave)
      map.on('mouseout', relevantMapLayerId, handleMouseOut)
    }

    return () => {
      map.getCanvas().style.cursor = 'unset'
      map.off('click', relevantMapLayerId, handleClick)
      map.off('click', handleAllClicks)
      map.off('mouseleave', relevantMapLayerId, handleMouseLeave)
      map.off('mousemove', relevantMapLayerId, handleMouseMove)
      map.off('mouseover', relevantMapLayerId, handleMouseOver)
      map.off('mouseout', relevantMapLayerId, handleMouseOut)
    }
  }, [map, analyzeMode, relevantStyleLayer, relevantMapLayerId, drawMode])

  // Hover highlight functions
  useEffect(() => {
    if (!map) {
      return
    }

    if (
      previousHighlight?.firstFeature.id !== highlightFeature?.firstFeature.id
    ) {
      if (previousHighlight?.firstFeature.source) {
        map.setFeatureState(
          {
            source: previousHighlight.firstFeature.source,
            sourceLayer: previousHighlight.firstFeature.sourceLayer,
            id: previousHighlight.firstFeature.id,
          },
          {
            hover: false,
          }
        )
      }

      if (activeDataLayer.highlight === 'postcode') {
        map.setFeatureState(
          {
            source: 'postcode',
            sourceLayer: 'kv_pc6group_2021',
            id: previousHighlight?.firstFeature.properties.group_id,
          },
          {
            hover: false,
          }
        )
      }
      setPreviousHighlight(highlightFeature)
      return
    }

    if (highlightFeature) {
      map.setFeatureState(
        {
          source: highlightFeature.firstFeature.source,
          sourceLayer: highlightFeature.firstFeature.sourceLayer,
          id: highlightFeature.firstFeature.id,
        },
        {
          hover: 'hover',
        }
      )
      if (activeDataLayer.highlight === 'postcode') {
        map.setFeatureState(
          {
            source: 'postcode',
            sourceLayer: 'kv_pc6group_2021',
            id: highlightFeature.firstFeature.properties?.group_id,
          },
          {
            hover: 'hover',
          }
        )
      }
    }
  }, [highlightFeature, previousHighlight, map])

  // Popup mousemove
  useEffect(() => {
    if (!map || !highlightFeature) {
      return
    }
    if (highlightFeature.event && highlightFeature.event.type === 'mousemove') {
      const feature = highlightFeature.firstFeature
      const { lngLat } = highlightFeature.event

      if (highlightFeature.relevantStyleLayer?.popup) {
        setPreviousFeatureId(feature?.id)
        if (feature?.id !== previousFeatureId) {
          setPopupContext({
            feature,
            lngLat,
            styleLayer: highlightFeature.relevantStyleLayer,
          })
        }
      }
    } else {
      setPopupContext({})
      setPreviousFeatureId(null)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [highlightFeature, previousFeatureId])

  // Click events
  useEffect(() => {
    if (
      !map ||
      !selectedFeatureAndEvent ||
      selectedFeatureAndEvent.event.type !== 'click'
    ) {
      return
    }

    // zelfde als featureevent

    removeAllFeatureStates(
      selectedFeatureAndEvent.firstFeature.source,
      selectedFeatureAndEvent.firstFeature.sourceLayer,
      activeDataLayer
    )

    map.setFeatureState(
      {
        source: selectedFeatureAndEvent.firstFeature.source,
        sourceLayer: selectedFeatureAndEvent.firstFeature.sourceLayer,
        id: selectedFeatureAndEvent.firstFeature.id,
      },
      {
        click: 'click',
      }
    )

    if (activeDataLayer.highlight === 'postcode') {
      map.setFeatureState(
        {
          source: 'postcode',
          sourceLayer: 'kv_pc6group_2021',
          id: previousHighlight.firstFeature.properties?.group_id,
        },
        {
          click: 'click',
        }
      )
    }

    if (selectedFeatureAndEvent.relevantStyleLayer?.detailModal) {
      setAnalyzeMode(
        activeDataLayer.theme === 'bevindingen'
          ? 'findings'
          : selectedFeatureAndEvent.relevantStyleLayer.detailModal(
              selectedFeatureAndEvent.firstFeature
            ).type === 'building'
          ? 'buildingDetail'
          : 'detail'
      )
    } else {
      setAnalyzeMode(null)
    }
  }, [selectedFeatureAndEvent])

  // Remove feature state of all
  useEffect(() => {
    // if (map && !selectedFeatureAndEvent && !analyzeMode) {
    if (map && !analyzeMode && relevantStyleLayer) {
      if (relevantStyleLayer?.geomType !== 'raster') {
        removeAllFeatureStates(
          relevantStyleLayer.tileSource.source,
          relevantStyleLayer['source-layer'] ||
            relevantStyleLayer.tileSource['source-layer'],
          activeDataLayer
        )
      }
    }
  }, [analyzeMode])

  // 2. Update map style theme
  useEffect(() => {
    if (map) {
      setCustomSourcesAdded(!customSourcesAdded)
      map.setStyle(mapTheme as any, { diff: true })
    }
  }, [mapTheme])

  // 3. Add custom data sources (context and relevant)
  // Define your custom sources in TileSources
  useEffect(() => {
    if (!map) {
      return
    }

    Object.entries(sources).forEach(([name, config]) => {
      if (name === 'kvk2') {
        if (!user?.user?.permissions) {
          return
        }
        const activeMunicipalityDataName = user?.user.permissions.find(
          (permission) =>
            permission.object.includes('kvk') &&
            permission.relation === 'access'
        )

        if (!activeMunicipalityDataName) {
          return
        }

        const municipalityName = activeMunicipalityDataName.object.replace(
          'kvk_',
          ''
        )

        ;(config as any).tiles[0] = (config as any).tiles[0].replace(
          '{gm}',
          municipalityName
        )
      }

      let existingSource = map.getSource(name)
      if (existingSource) {
        map.removeSource(name)
      }
      existingSource = map.getSource(name)

      if (!existingSource) {
        map.addSource(name, config as any)
      }
    })

    // TODO: Why is this necessary? Seems off
    setCustomSourcesAdded(!customSourcesAdded)
  }, [map, user?.user])

  // 101. Set pitch. When toggling the 3D function, the map is tilted slightly
  useEffect(() => {
    if (map) {
      if (map.getPitch() === 0 && threeDimensional) {
        map.setPitch(40)
        return
      }
      if (map.getPitch() > 0 && !threeDimensional) {
        map.setPitch(0)
      }
    }
  }, [threeDimensional])

  useEffect(() => {
    if (!map || !reloadFindingsTiles) {
      return
    }

    if (map.style.sourceCaches?.bevindingen?.clearTiles) {
      const tileUrl =
        process.env.NEXT_PUBLIC_ENV === 'prod'
          ? [
              `https://tileserver.commondatafactory.nl/dook.finding_point/{z}/{x}/{y}.pbf`,
            ]
          : process.env.NEXT_PUBLIC_ENV === 'acc'
          ? [
              `https://acc.tileserver.commondatafactory.nl/dook.finding_point/{z}/{x}/{y}.pbf`,
            ]
          : [`http://127.0.0.1:7800/dook.finding_point/{z}/{x}/{y}.pbf}`]

      ;(map.getSource('bevindingen') as any).tiles = [
        `${tileUrl}?dt=${Date.now()}`,
      ]
      map.style.sourceCaches.bevindingen.clearTiles()
      map.style.sourceCaches.bevindingen.update(map.transform)
      map.triggerRepaint()
    }
    setReloadFindingsTiles(false)
  }, [reloadFindingsTiles])

  const styleLayersHaveExtrusion =
    activeDataLayer?.styleLayers.some(
      (styleLayer) => styleLayer.hasExtrusion
    ) || relevantStyleLayer?.extrusionAttr

  return (
    <>
      <Styled.Map
        isCollapsed={isCollapsed}
        className="map"
        ref={(el) => (mapContainer.current = el)}
      />

      {/* DrawMode voor Count en Findings  */}
      {map && drawMode && <MapDrawTool map={map} />}

      {(activeDataLayer.theme === 'bevindingen' ? user.isLoggedIn : true) && (
        <MapPopup map={map} context={popupContext} />
      )}

      {customSourcesAdded && (
        <>
          <MapContextLayers map={map} theme={mapTheme.name} />
          <MapRelevantLayers map={map} activeDataLayer={activeDataLayer} />
        </>
      )}

      <Styled.UiLayer>
        <MapConfiguration disablethreeDimensional={!styleLayersHaveExtrusion} />
        {warning && (
          <Styled.Alert
            title={warning.title}
            variant="warning"
            maxWidth={analyzeMode ? drawerWidth : null}
          >
            {warning.description}
          </Styled.Alert>
        )}
      </Styled.UiLayer>
    </>
  )
}

export default Map

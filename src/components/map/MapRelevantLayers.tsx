// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FunctionComponent, useContext, useEffect } from 'react'
import { makeInvertLayer, makeMapLayer } from '../../data/MakeStyleLayer'
import {
  getPostcodeHighLightLayerLine,
  getBuildingLayer,
} from '../../data/ContextStyleLayers'
import useUser from '../../hooks/use-user'
import { MapContext } from '../../providers/map-provider'
import { DataLayerProps } from '../../data/DataLayers'

const getInsertDefaults = (
  mapLayer: { source: string; type: string; id: string },
  map: maplibregl.Map
) => {
  // NOTE: Specific layers
  if (mapLayer.type === 'fill-extrusion') {
    const pointLayer = map
      .getStyle()
      .layers.find(
        (mapLayer) => mapLayer.type === 'circle' || mapLayer.type === 'symbol'
      )
    if (pointLayer) {
      return pointLayer.id
    }
    return ''
  }

  if (mapLayer.id === 'bag:panden:fill:1') {
    const rasterLayer = map
      .getStyle()
      .layers.find((mapLayer) => mapLayer.type === 'raster')
    if (rasterLayer) {
      return rasterLayer.id
    }
  }

  // NOTE: Generic layers
  if (mapLayer.type === 'circle') {
    return 'labels_populated_places'
  }

  if (mapLayer.type === 'raster') {
    if (map.getLayer('bag:panden:fill-extrusion:1')) {
      return 'bag:panden:fill-extrusion:1'
    }
  }

  if (mapLayer.type === 'symbol') {
    return ''
  }

  // NOTE: All other layers
  return 'road_labels'
}

const compareLayers = (currentMapLayers, newMapLayers) => {
  const add = newMapLayers.filter(
    (newMapLayer) =>
      !currentMapLayers.some(
        (currentMapLayer) => newMapLayer.id === currentMapLayer.id
      )
  )

  const update = currentMapLayers.reduce((acc, currentMapLayer) => {
    const sameSourceLayer = newMapLayers.find(
      (newMapLayer) => newMapLayer.id === currentMapLayer.id
    )
    if (sameSourceLayer) {
      return [...acc, [currentMapLayer, sameSourceLayer]]
    }
    return acc
  }, [])

  const remove = currentMapLayers.filter(
    (currentMapLayer) =>
      !newMapLayers.some((newMapLayer) => newMapLayer.id === currentMapLayer.id)
  )

  return { add, update, remove }
}

// NOTE: Convert map layers to MapLibre accepted layers
const generateMapLayer = (
  styleLayer,
  relevantLayer,
  theme,
  threeDimensional
) => {
  if (styleLayer.id === 'layer0') {
    const buildingLayer = getBuildingLayer('layer0', theme, threeDimensional)
    return {
      ...buildingLayer,
      attrName: styleLayer.attrName,
      insert: styleLayer.insert || '',
    }
  }

  if (styleLayer.id === 'noDataLayer') {
    const invertLayer = makeInvertLayer({ ...relevantLayer, id: 'noDataLayer' })
    return {
      ...invertLayer,
      attrName: styleLayer.attrName,
      insert: styleLayer.insert || '',
    }
  }

  if (styleLayer.id === 'postalCodes') {
    const postcodeLayer = getPostcodeHighLightLayerLine('postalCode', theme)
    return {
      ...postcodeLayer,
      attrName: styleLayer.attrName,
      insert: styleLayer.insert || '',
    }
  }

  if (styleLayer.geomType === 'symbol') {
    const layer = { ...relevantLayer }
    layer.geomType = 'symbol'
    // layer.attrName = styleLayer.attrName
    // console.log({...relevantLayer})
    // console.log({...styleLayer})

    const symbolLayer = makeMapLayer(
      { ...layer },
      threeDimensional,
      styleLayer.attrName
    )
    // console.log(symbolLayer)
    return {
      ...symbolLayer,
      attrName: styleLayer.attrName,
      insert: '',
    }
  }

  if (styleLayer.tileSource || styleLayer.rasterSource) {
    const l = makeMapLayer(styleLayer, threeDimensional)

    return {
      relevant: true,
      attrName: styleLayer.attrName,
      insert: styleLayer.insert || '',
      ...l,
    }
  }
}

const addDynamicMapLayerIds = (mapLayers) =>
  mapLayers.reduce((acc, mapLayer) => {
    const sourceId = `${mapLayer.source ? `${mapLayer.source}:` : ''}${
      mapLayer['source-layer'] ? `${mapLayer['source-layer']}:` : ''
    }${mapLayer.type || ''}`
    const count = acc.reduce(
      (n, mapLayer) => n + (mapLayer?.id.includes(sourceId) ? 1 : 0),
      1
    )

    return [
      ...acc,
      { ...mapLayer, id: `${sourceId}:${count}`, actualId: mapLayer.id || '' },
    ]
  }, [])

interface MapRelevantLayersProps {
  map: maplibregl.Map
  activeDataLayer: Partial<DataLayerProps>
}

const MapRelevantLayers: FunctionComponent<MapRelevantLayersProps> = ({
  map,
  activeDataLayer,
}) => {
  const {
    currentMapLayers,
    setCurrentMapLayers,
    mapTheme,
    threeDimensional,
    customFilter,
    setRelevantMapLayerId,
  } = useContext(MapContext)
  const user = useUser()

  // Build Relevant styleLayer(s)
  useEffect(() => {
    if (!map || !activeDataLayer.styleLayers) {
      return
    }

    const relevantLayer =
      activeDataLayer.styleLayers.find((layer) => layer.isRelevantLayer) ||
      activeDataLayer.styleLayers[0]

    const userData = user.isLoggedIn
      ? {
          userId: user.user.identity.id,
        }
      : null

    const maplibreUsableLayers = activeDataLayer.styleLayers.map(
      (styleLayer) => {
        if (styleLayer.userAttrName) {
          styleLayer.user = userData
        }
        return generateMapLayer(
          styleLayer,
          relevantLayer,
          mapTheme.name,
          threeDimensional
        )
      }
    )

    const newMapLayers = addDynamicMapLayerIds(maplibreUsableLayers)

    const updateStrategy = compareLayers(currentMapLayers, newMapLayers)

    // console.log('updateStrategy', updateStrategy)

    updateStrategy.remove.forEach((mapLayer) => {
      map.getLayer(mapLayer.id) && map.removeLayer(mapLayer.id)
    })

    updateStrategy.update.forEach(([currentLayer, newLayer]) => {
      const layerId = map.getLayer(currentLayer.id)?.id

      if (!layerId) {
        return
      }

      Object.entries(newLayer.paint).forEach(([elem, value]) => {
        if (map.getPaintProperty(layerId, elem)) {
          map.setPaintProperty(layerId, elem, value)
        }
      })

      map.setFilter(
        layerId,
        customFilter && currentLayer.relevant ? customFilter : newLayer.filter,
        { validate: true }
      )
    })

    updateStrategy.add.forEach((mapLayer) => {
      const insert = mapLayer.insert || getInsertDefaults(mapLayer, map)
      if (!map.getLayer(mapLayer.id)) {
        map.addLayer(mapLayer, insert)
        map.setFilter(
          mapLayer.id,
          customFilter && mapLayer.relevant ? customFilter : mapLayer.filter
        )
      }
    })

    // NOTE: check the layer order through this log:
    // console.log('active layers', map.getStyle().layers)

    // relevant layer
    const zoomTo = relevantLayer.tileSource?.minZoom
    if (zoomTo && map.getZoom() < zoomTo) {
      map.zoomTo(zoomTo)
    }

    setCurrentMapLayers(newMapLayers)

    const maplayerid =
      newMapLayers.find((newMapLayer) => newMapLayer.relevant) || null

    setRelevantMapLayerId(maplayerid?.id)
  }, [map, activeDataLayer, threeDimensional, mapTheme.name, customFilter])

  return null
}

export default MapRelevantLayers

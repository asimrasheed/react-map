// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { screen, render, act } from '@testing-library/react'
import { MapContext, MapContextProps } from '../../../providers/map-provider'
import {
  LabelContext,
  LabelContextProps,
} from '../../../providers/label-provider'
import { MapConfiguration } from '.'

const setup = (value = {}, disablethreeDimensional = false) => {
  const defaultMapContextValue = {
    threeDimensional: false,
    toggleThreeDimensional: jest.fn(),
  } as unknown as MapContextProps

  const defaultLabelContextValue = {
    labelState: 'geen',
    toggleLabelState: jest.fn(),
    setBorderState: jest.fn(),
  } as unknown as LabelContextProps

  render(
    <MapContext.Provider value={{ ...defaultMapContextValue, ...value }}>
      <LabelContext.Provider value={defaultLabelContextValue}>
        <MapConfiguration disablethreeDimensional={disablethreeDimensional} />
      </LabelContext.Provider>
    </MapContext.Provider>
  )

  return {
    ...defaultMapContextValue,
    ...defaultLabelContextValue,
  }
}

describe('MapConfiguration', () => {
  it('should render correctly', () => {
    setup()

    expect(screen.getByText('Geen Labels')).toBeInTheDocument()
    expect(screen.getByText('Topografische labels')).toBeInTheDocument()
    expect(
      screen.getByText('Administratieve labels en grenzen')
    ).toBeInTheDocument()
    expect(screen.getByText('Object hoogte')).toBeInTheDocument()
  })

  it('should handle click on "Geen Labels" correctly', () => {
    const { toggleLabelState } = setup()

    act(() => {
      screen.getByText('Geen Labels').click()
    })

    expect(toggleLabelState).toHaveBeenCalledWith('geen')
  })

  it('should handle click on "Topografische labels" correctly', () => {
    const { toggleLabelState } = setup()

    act(() => {
      screen.getByText('Topografische labels').click()
    })

    expect(toggleLabelState).toHaveBeenCalledWith('topo')
  })

  it('should handle click on "Administratieve labels en grenzen" correctly', () => {
    const { toggleLabelState } = setup()

    act(() => {
      screen.getByText('Administratieve labels en grenzen').click()
    })

    expect(toggleLabelState).toHaveBeenCalledWith('admin')
  })

  it('should handle click on "Object hoogte" correctly', () => {
    const { toggleLabelState, toggleThreeDimensional } = setup()

    act(() => {
      screen.getByText('Object hoogte').click()
    })

    expect(toggleLabelState).not.toHaveBeenCalled()
    expect(toggleThreeDimensional).toHaveBeenCalled()
  })
})

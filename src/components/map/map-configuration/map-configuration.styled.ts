// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.div`
  position: absolute;
  user-select: none;
  display: flex;
  justify-content: flex-start;
  min-height: 3rem;
  bottom: 0;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  align-items: center;
  backdrop-filter: blur(3px);
  background: rgba(255, 255, 255, 0.7);
  width: 100%;
  z-index: 10;

  /* intermediate */
  @media screen and (max-width: 1380px) {
    width: -moz-available;
    width: -webkit-fill-available;
    display: grid;
    min-height: 2rem;
    grid-template-columns: repeat(3, auto);
    grid-template-rows: repeat(2, 1fr);
    justify-content: space-between;

    p,
    span {
      font-size: 0.9rem;
    }
  }
`

export const Item = styled.div<{ disabled?: boolean }>`
  align-items: center;
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  display: flex;
  margin-left: 1.5rem;
  margin-right: 0.25rem;
  text-decoration: ${({ disabled }) => (disabled ? 'line-through' : '')};

  /* intermediate */
  @media screen and (max-width: 1250px) {
    margin-left: 0.5rem;
    margin-right: 0.5rem;
  }
`

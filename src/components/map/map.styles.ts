// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Alert as ImportAlert } from '@commonground/design-system'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Map = styled.div<{ isCollapsed: boolean }>`
  grid-column: 2 / span 1;
  grid-row: 1;
  align-self: stretch;
  margin: 0;
  padding: 0;
  height: 100vh;
  transition: width 550ms ease-in-out;
  .mapboxgl-ctrl-bottom-left {
    padding-bottom: 3rem;
  }
  .mapboxgl-ctrl-bottom-right {
    padding-bottom: 3rem;
  }
  .mapboxgl-ctrl.mapboxgl-ctrl-attrib {
    box-shadow: none !important;
  }
  .mapboxgl-ctrl.mapboxgl-ctrl-scale {
    background: none !important;
    box-shadow: none !important;
  }

  & .mapboxgl-popup-content {
    pointer-events: none;
  }

  .mapboxgl-ctrl.mapboxgl-ctrl-group
    button.mapboxgl-ctrl-graphImage
    span.mapboxgl-ctrl-icon {
    background-image: url('/images/bar-chart.svg');
  }

  .mapboxgl-ctrl.mapboxgl-ctrl-group button.mapboxgl-ctrl-spinner {
    pointer-events: none !important;
  }

  /* Custom styling controls */

  .mapboxgl-ctrl.mapboxgl-ctrl {
    margin: 0 !important;
    box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24) !important;
  }

  .mapboxgl-ctrl-bottom-right div.mapboxgl-ctrl.mapboxgl-ctrl-group {
    margin-right: 1rem !important;
    margin-bottom: 1rem !important;
  }

  .mapboxgl-ctrl-top-left div.mapboxgl-ctrl.mapboxgl-ctrl-group {
    margin-top: 1rem !important;
    margin-left: 1rem !important;
  }

  .mapboxgl-ctrl-bottom-left div.mapboxgl-ctrl.mapboxgl-ctrl-scale {
    margin-left: 1rem !important;
    margin-bottom: 1rem !important;
  }

  .mapboxgl-ctrl.mapboxgl-ctrl-group button {
    width: 36px;
    height: 36px;
    padding: 8px !important;
  }

  .mapboxgl-ctrl-zoom-out > span {
    background-image: url('/images/subtract-line.svg') !important;
    background-size: 20px 20px !important;
    background-repeat: no-repeat;
  }

  .mapboxgl-ctrl-zoom-in > span {
    background-image: url('/images/add-line.svg') !important;
    background-size: 20px 20px !important;
    background-repeat: no-repeat;
  }

  .mapboxgl-ctrl-compass > span {
    background-image: url('/images/compass.svg') !important;
    background-size: 20px 20px !important;
    background-repeat: no-repeat;
  }

  .mapboxgl-ctrl-fullscreen > span {
    background-image: url('/images/fullscreen.svg') !important;
    background-size: 20px 20px !important;
    background-repeat: no-repeat;
  }

  ${mediaQueries.mdDown`
    display: block;
    position: absolute;
    max-height: calc(100vh - 64px);
    top: 64px;
    width: 100vw;

    .mapboxgl-ctrl-bottom-right{
      display: none;
    }
  `}
`

export const UiLayer = styled.div`
  grid-column: 2 / span 1;
  grid-row: 1;
  align-self: stretch;
  margin: 0;
  padding: 0;
  height: 100vh;
  pointer-events: none;

  > * {
    pointer-events: initial;
  }

  ${mediaQueries.mdDown`
    display: none;
  `}
`

export const Alert = styled(ImportAlert)`
  > * {
    max-width: ${({ maxWidth }) => `calc(100% - ${maxWidth}px)` || ''};
  }
`

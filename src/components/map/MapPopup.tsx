// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FunctionComponent, useEffect } from 'react'
import maplibregl from 'maplibre-gl'
import 'maplibre-gl/dist/maplibre-gl.css'

const popup = new maplibregl.Popup({
  closeButton: false,
  closeOnClick: true,
  closeOnMove: true,
})

// Make popup content
function makePopupContent({ feature, styleLayer, postalCodes }) {
  if (!feature) return
  const popupData = styleLayer?.popup(feature)
  const title =
    popupData?.title ||
    (styleLayer.attrName &&
      `${feature.properties[styleLayer.attrName]} ${styleLayer.unit}`)
  const content = popupData?.content
  const titleHtml = title ? `<h4>${title}</h4>` : ''
  const contentHtml = content ? `<p>${content}</p>` : ''
  const postalCodesHtml = postalCodes ? `<p>${postalCodes}</p>` : ''

  return (
    (titleHtml || contentHtml) && `${titleHtml}${contentHtml}${postalCodesHtml}`
  )
}

interface MapPopupProps {
  map: any
  context: any
}

const MapPopup: FunctionComponent<MapPopupProps> = ({
  map,
  context: { lngLat, ...context },
}) => {
  useEffect(() => {
    if (!map) {
      return
    }

    const content = makePopupContent(context)

    lngLat && popup.setLngLat(lngLat)
    content && popup.setHTML(content)
    content ? popup.addTo(map) : popup.remove()
  }, [context, lngLat, map])

  return null
}

export default MapPopup

// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

// NOTE: because of issues with the mapboxdraw types, we're currently disabling TS checking in this file
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import { useState, useEffect } from 'react'
import 'maplibre-gl/dist/maplibre-gl.css'
import MapboxDraw from '@mapbox/mapbox-gl-draw'
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css'

import { useDraw } from '../../hooks/use-draw'
import { pointDrawStyle, polygonDrawStyle } from '../../data/DrawToolStyles'

const drawPolygonOptions = {
  displayControlsDefault: false,
  defaultMode: 'draw_polygon',
  actions: {
    trash: true,
    combineFeatures: false,
    uncombineFeatures: false,
  },
  styles: polygonDrawStyle,
  keyBindings: false,
  touchEnabled: false,
}

const idleMode = {
  toDisplayFeatures: (state, geojson, display) => {
    display(geojson)
  },
} as MapboxDraw.DrawCustomMode

const drawPointOptions = {
  displayControlsDefault: false,
  defaultMode: 'draw_point',
  actions: {
    trash: true,
    combineFeatures: true,
    uncombineFeatures: false,
  },
  styles: pointDrawStyle,
  modes: Object.assign(
    {
      idle: idleMode,
    },
    MapboxDraw.modes
  ),
}

const mapCenterDeviation = 434

interface DrawProps {
  map: maplibregl.Map
}

export const MapDrawTool: React.FC<DrawProps> = ({ map }) => {
  const [drawTool, setDrawTool] = useState<MapboxDraw>()
  const [hasUpdatedSelection, setUpdatedSelection] = useState(false)

  const {
    drawMode,
    setDrawMode,
    deleteSelectedFeature,
    setDeleteSelectedFeature,
    startDrawing,
    setStartDrawing,
    featureCollection,
    setFeatureCollection,
    setSelectedPolygonForCount,
    setSelectedPointForFindings,
  } = useDraw()

  const getCenterOfPolygon = (polygon) =>
    polygon.reduce(
      (acc, point, i, a) => {
        return [acc[0] + point[0] / a.length, acc[1] + point[1] / a.length]
      },
      [0, 0]
    )

  const selectLatestFeature = () => {
    const allFeatures = drawTool.getAll().features
    if (allFeatures.length) {
      const lastFeature = allFeatures[allFeatures.length - 1]

      setUpdatedSelection(true)
      drawTool.changeMode('simple_select', {
        featureIds: [lastFeature.id],
      })
      return lastFeature
    }
    return null
  }

  const handleDeleteFeatures = () => {
    setDeleteSelectedFeature(false)

    // TODO: Check if this goes off on reopening a count draw polygon after deleting a count polygon
    if (!drawTool) {
      return
    }

    if (drawMode === 'initPolygons') {
      const selectedIds = drawTool.getSelectedIds()

      if (!selectedIds) {
        return
      }

      drawTool.delete(selectedIds)
      const getLatestFeature = !!selectLatestFeature()

      if (!getLatestFeature) {
        setSelectedPolygonForCount(null)
        setFeatureCollection(null)
        drawTool.deleteAll()
        drawTool.changeMode('draw_polygon', drawPolygonOptions)
      }
      return
    }

    if (drawMode === 'initPoint') {
      const collection = drawTool.getAll()
      collection.features?.forEach((feature, i) => {
        if (i !== collection.features.length - 1) drawTool.delete(feature.id)
      })
    }
  }

  // Post actions after changing selection
  useEffect(() => {
    setUpdatedSelection(false)
    if (!drawTool || !hasUpdatedSelection || drawMode === 'idle') {
      return
    }

    const selectedFeature = drawTool.getSelected()?.features?.[0]

    const drawnFeatures = drawTool.getAll()
    setFeatureCollection(drawnFeatures)

    if (drawMode === 'initPolygons') {
      if (selectedFeature) {
        setSelectedPolygonForCount(selectedFeature)
        if (drawTool.getMode() === 'simple_select') {
          map.easeTo({
            center: getCenterOfPolygon(selectedFeature.geometry.coordinates[0]),
            padding: { right: mapCenterDeviation },
          })
        }
      } else {
        setSelectedPolygonForCount(null)
      }
      return
    }

    if (selectedFeature && drawMode === 'initPoint') {
      setSelectedPointForFindings(selectedFeature)
      handleDeleteFeatures()
      drawTool.changeMode('draw_point', drawPointOptions)
      map.easeTo({
        center: selectedFeature.geometry.coordinates,
        padding: { right: mapCenterDeviation },
      })
    }
  }, [hasUpdatedSelection])

  // Init DrawTool
  useEffect(() => {
    if (!map || drawTool) {
      return
    }

    const options =
      drawMode === 'initPolygons' ? drawPolygonOptions : drawPointOptions
    const draw = new MapboxDraw(options)
    map.addControl(draw)
    setDrawTool(draw)

    // // add Feature collection (history)
    if (featureCollection?.features?.length && drawMode === 'initPolygons') {
      if (draw.getMode() === 'draw_point') {
        console.warn('get draw mode init issue')
        return
      }
      draw.add(featureCollection)
      draw.changeMode('direct_select', {
        featureId: featureCollection.features[0].id,
      })
    }

    return () => {
      if (drawMode === 'initPolygons') {
        draw.changeMode('simple_select')
        const drawnFeatures = draw.getAll()
        setFeatureCollection(drawnFeatures)
      } else {
        setFeatureCollection(null)
      }

      setSelectedPolygonForCount(null)
      setSelectedPointForFindings(null)

      setDrawTool(null)
      setDrawMode(null)
      map.removeControl(draw)
    }
  }, [])

  // Fire draw tool
  useEffect(() => {
    if (!drawTool) {
      return
    }

    if (startDrawing === 'polygons') {
      drawTool.changeMode('draw_polygon', drawPolygonOptions)
      setStartDrawing(false)
      return
    }

    if (drawMode === 'points') {
      drawTool.changeMode('draw_point', drawPointOptions)
      setStartDrawing(false)
    }
  }, [startDrawing])

  // Update drawtool mode
  useEffect(() => {
    if (!drawTool) {
      return
    }

    if (drawMode === 'idle') {
      drawTool.changeMode('idle')
    }
  }, [drawMode])

  //  General draw tool actions and what to do
  useEffect(() => {
    if (!map || !drawTool) {
      return
    }

    const handleSelectionChange = (
      event: MapboxDraw.DrawSelectionChangeEvent
    ) => {
      setUpdatedSelection(true)
    }

    const handleUpdate = (event: MapboxDraw.DrawUpdateEvent) => {
      setUpdatedSelection(true)
    }

    map.on('draw.selectionchange', handleSelectionChange)
    map.on('draw.update', handleUpdate)

    return () => {
      map.off('draw.selectionchange', handleSelectionChange)
      map.off('draw.update', handleUpdate)
    }
  }, [map, drawTool, drawMode])

  // Delete
  useEffect(() => {
    if (deleteSelectedFeature) {
      handleDeleteFeatures()
    }
  }, [deleteSelectedFeature])

  return null
}

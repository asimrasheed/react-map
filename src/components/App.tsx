// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useEffect, useState } from 'react'

import { useLayers } from '../hooks/use-layers'
import { useTabs } from '../hooks/use-tabs'
import { LabelProvider } from '../providers/label-provider'
import { useIsMobile } from '../hooks/use-is-mobile'
import { useMap } from '../hooks/use-map'
import { setURLParams, UrlParamsKey } from '../utils/url-params'
import { degoDataLayers, dookDataLayers } from '../data/DataLayers'
import MenuPanelFooter from './menu-panel/footer/Footer'
import Map from './map/Map'
import Legend from './legend/Legend'
import MenuPanel from './menu-panel'
import { Authentication } from './authentication'
import { MapSpinner } from './map-spinner'
import { Modal } from './modal/Modal'
import * as Styled from './app.styles'
import { Notifications } from './notifications/Notifications'

const App = ({ product }) => {
  const { tabs, defaultTab, activeTab, setActiveTab, tabLayers, setTabLayers } =
    useTabs()
  const isMobile = useIsMobile()
  const { analyzeMode } = useMap()
  const { activeLayerId, activeDataLayer, hasAccess } = useLayers()

  const [isAppMounted, setIsAppMounted] = useState(false)
  const [filteredTabs, setFilteredTabs] = useState([])
  const [isCollapsed, setIsCollapsed] = useState(false)

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    const tabKey = searchParams.has('tab') && searchParams.get('tab')

    setActiveTab(
      (tabKey && tabs.find((tab) => tab.id === tabKey)) || defaultTab
    )
  }, [])

  useEffect(() => {
    const productLayers = product === 'DEGO' ? degoDataLayers : dookDataLayers

    const layersPerTab = tabs.reduce(
      (acc, tab) => ({
        ...acc,
        [tab.id]: productLayers.filter(
          (dataLayer) =>
            (dataLayer.id !== 'layer0' &&
              (typeof dataLayer.theme === 'string'
                ? dataLayer.theme === tab.id
                : (dataLayer.theme as any[]).includes(tab.id)) &&
              hasAccess(dataLayer)) ||
            dataLayer.authLocked
        ),
      }),
      {}
    )

    const visibleTabs = tabs.filter((tab) => layersPerTab[tab.id].length > 0)

    setFilteredTabs(visibleTabs)
    setIsAppMounted(true)

    if (activeTab) {
      const visibleLayers = layersPerTab[activeTab.id]

      setURLParams(UrlParamsKey.Tab, activeTab.id)
      setTabLayers(visibleLayers)
    }
  }, [hasAccess, activeTab])

  if (!isAppMounted) {
    return <></>
  }

  return (
    <Styled.Container>
      <MenuPanel
        activeTab={activeTab}
        changeActiveTab={setActiveTab}
        tabThemes={filteredTabs}
        tabLayers={tabLayers}
        isCollapsed={isCollapsed}
        setIsCollapsed={setIsCollapsed}
      />

      <MenuPanelFooter />

      {activeLayerId !== 'layer0' && !isMobile && (
        <Legend activeDataLayer={activeDataLayer} />
      )}

      <LabelProvider>
        <Map activeDataLayer={activeDataLayer} isCollapsed={isCollapsed} />
      </LabelProvider>

      {!isMobile && (
        <>
          <Notifications />

          <Styled.TopRightContainer>
            <Styled.FlexColumn>
              <Authentication />
              <MapSpinner />
            </Styled.FlexColumn>

            {analyzeMode && <Modal activeDataLayer={activeDataLayer} />}
          </Styled.TopRightContainer>
        </>
      )}
    </Styled.Container>
  )
}

export default App

// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Authentication = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  img {
    cursor: pointer;
    box-shadow: 0 0 #0000, 0 0 #0000, 0 10px 15px -3px rgba(0, 0, 0, 0.1),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
    border-radius: 9999px;
    width: 2.25rem;
    height: 2.25rem;
    max-width: 100%;
    display: block;
    border-style: solid;
  }
`

export const Popup = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 16rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  background-color: rgba(255, 255, 255, 1);
  border-radius: 0.25rem;
  margin-top: 0.25rem;
  box-shadow: 0 0 #0000, 0 0 #0000, 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
`

export const Button = styled.button<{ active?: boolean }>`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  border-style: none;
  padding: 0;
  padding-left: 1rem;
  padding-right: 1rem;
  user-select: none;
  width: 100%;
  height: 3rem;
  cursor: pointer;

  :hover {
    background-color: #eeeeee;
  }

  background-color: ${(props) => (props.active ? '#E0EEE0 !important' : '')};
`

export const Category = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  border-style: none;
  padding: 0;
  padding-left: 1rem;
  padding-right: 1rem;
  user-select: none;
  width: 100%;
  height: 3rem;
  background-color: #eeeeee;
  font-weight: bold;
`

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useContext } from 'react'
import { Spinner, Button, Icon } from '@commonground/design-system'
import ExternalLinkIcon from '../../../public/images/external-link.svg'
import useUser from '../../hooks/use-user'
import AppContext from '../AppContext'
import * as Styled from './Authentication.styled'

const Authentication = () => {
  const { user, isLoading, isError } = useUser()
  const config = useContext(AppContext)

  const permissionsUrl = `${process.env.NEXT_PUBLIC_AUTH_URL}/permissies`
  const loginUrl = `${process.env.NEXT_PUBLIC_AUTH_URL}/login`

  return (
    config?.auth && (
      <Styled.Authentication>
        {isLoading ? (
          <div data-testid="spinner">
            <Spinner />
          </div>
        ) : (
          <Button
            variant="secondary"
            as="a"
            href={user ? permissionsUrl : loginUrl}
          >
            {user ? 'Terug naar instellingen' : 'Inloggen/Registreren'}{' '}
            <Icon as={ExternalLinkIcon} inline />
          </Button>
        )}
      </Styled.Authentication>
    )
  )
}

export default Authentication

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Address = styled.p`
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;
`

export const Center = styled.div`
  text-align: center;
  margin-left: 2rem;
  margin-right: 2rem;
  & p {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    flex: 1 1 0%;
    margin: 0.75rem;
  }
`

export const GreyContainer = styled.div`
  background-color: var(--colorPaletteGray100);
  overflow: hidden;
  padding: 0.5rem;
  border-radius: 0.25rem;
  margin-top: 2rem;

  & p.bold {
    font-weight: 700;
    margin-bottom: 1rem;
    color: rgb(33, 33, 33);
    font-size: 1rem;
  }
`

export const Item = styled.div`
  display: flex;
  margin-top: 0.25rem;
  margin-bottom: 0;

  p:first-child {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    flex: 1 1 0%;
  }
`

export const MarginButton = styled.div`
  margin-top: 2rem;
`

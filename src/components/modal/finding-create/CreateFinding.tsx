// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import {
  Dispatch,
  FC,
  SetStateAction,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react'
import { Button, TextInput, Spinner, Radio } from '@commonground/design-system'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'

import { getNearestLocation } from '../../../utils/get-nearest-location'
import { postFinding } from '../../../utils/findings/post-finding'
import { postFindingNote } from '../../../utils/findings/post-note'

import useUser from '../../../hooks/use-user'
import { useDraw } from '../../../hooks/use-draw'
import { useMap } from '../../../hooks/use-map'
import { ToasterContext } from '../../../providers/toaster-provider'
import * as Styled from './create-finding.styles'

type FindingDetailProps = {
  setTitle: Dispatch<SetStateAction<string>>
}

export type StageProp =
  | 'chooseLocation'
  | 'confirmLocation'
  | 'createNewFinding'
  | 'loadingNewFinding'

export const CreateFinding: FC<FindingDetailProps> = ({ setTitle }) => {
  const { user } = useUser()
  const { setAnalyzeMode, setReloadFindingsTiles } = useMap()
  const { selectedPointForFindings, setDrawMode } = useDraw()
  const [stage, setStage] = useState<StageProp>('chooseLocation')
  const [addressData, setAddressData] = useState(null)
  const addressController = useRef(new AbortController())
  const { showToast } = useContext(ToasterContext)

  const handleCreateFinding = async (finding, noteDescription) => {
    const postedFinding = await postFinding(finding)

    if (!postedFinding) {
      return
    }

    const postFindingNoteResult: any = await postFindingNote(
      postedFinding.data.ID,
      {
        description: noteDescription,
      }
    )

    if (!postFindingNoteResult.data) {
      return
    }

    return { ...postedFinding, notes: [] }
  }

  useEffect(() => {
    setTitle('Bevinding maken')
  }, [])

  // NOTE: Set and get stages for creating the finding
  useEffect(() => {
    if (stage === 'chooseLocation' && selectedPointForFindings) {
      setStage('confirmLocation')
      setTitle('Bevestig locatie')
    }

    if (stage === 'confirmLocation') {
      ;(async () => {
        const { signal } = addressController.current

        if (!selectedPointForFindings) {
          console.warn('No selected point was found')
          return
        }

        const newAddressData = await getNearestLocation(
          selectedPointForFindings?.geometry.coordinates[0],
          selectedPointForFindings?.geometry.coordinates[1],
          signal
        )

        if (newAddressData) {
          setAddressData(newAddressData || { error: true })
        }
      })()
    }

    if (stage === 'createNewFinding') {
      setTitle('Bevinding aanmaken')
    }
  }, [selectedPointForFindings?.geometry.coordinates, stage])

  if (stage === 'chooseLocation') {
    return (
      <Styled.Item>
        <p>
          Klik op een punt op de kaart om een nieuwe bevinding toe te voegen.
        </p>
      </Styled.Item>
    )
  }

  if (stage === 'confirmLocation') {
    const handleConfirm = () => {
      setStage('createNewFinding')
      setDrawMode('idle')
      addressController.current.abort()
    }

    return (
      <>
        {addressData && !addressData?.error ? (
          <p>
            {addressData.afstand === 0
              ? 'Dit adres is gevonden op onderstaande locatie.'
              : 'Dit adres ligt het dichtste in de buurt van onderstaande locatie.'}
          </p>
        ) : (
          !addressData && (
            <div style={{ display: 'flex' }}>
              <Spinner />
            </div>
          )
        )}

        <Formik
          enableReinitialize
          initialValues={{
            address: addressData && !addressData?.error ? 'a' : '',
          }}
          onSubmit={(values) => {
            if (values.address === '') {
              setAddressData(null)
            }
            handleConfirm()
          }}
        >
          <Form>
            <Styled.GreyContainer>
              <p className="bold" style={{ marginBottom: -16 }}>
                Kies adres:
              </p>

              <Radio.Group label="">
                {addressData && !addressData?.error && (
                  <Radio
                    name="address"
                    value="a"
                    label={`adres is ${addressData.weergavenaam}`}
                  >
                    {addressData.weergavenaam || ''}
                  </Radio>
                )}
                <Radio name="address" value="">
                  Geen adres gebruiken
                </Radio>
              </Radio.Group>
            </Styled.GreyContainer>

            <Styled.MarginButton>
              <Button size="default" type="submit">
                Locatie bevestigen
              </Button>
            </Styled.MarginButton>
          </Form>
        </Formik>
      </>
    )
  }

  if (stage === 'createNewFinding') {
    const currentDate = new Date()
    const date = new Intl.DateTimeFormat('nl-NL', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    }).format(currentDate)

    const handleSubmit = async ({
      findingTitle,
      findingDescription,
      noteDescription,
    }) => {
      const newFinding = {
        addressDisplay: addressData?.weergavenaam,
        description: findingDescription,
        geodata: JSON.stringify(addressData || {}),
        houseNlt: addressData?.huis_nlt,
        locality: addressData?.woonplaatsnaam,
        postalCode: addressData?.postcode,
        street: addressData?.straatnaam,
        title: findingTitle,
        userId: user.identity.id,
        username: `${user.identity.traits.name.first} ${user.identity.traits.name.last}`,
        geojson: {
          type: 'Point',
          coordinates: selectedPointForFindings?.geometry.coordinates as [
            number,
            number
          ],
        },
      }

      const createFinding = await handleCreateFinding(
        newFinding,
        noteDescription
      )

      if (!createFinding) {
        showToast({ title: 'Kon bevinding niet aanmaken', variant: 'error' })
        return
      }

      setAnalyzeMode(null)
      setReloadFindingsTiles(true)
      setDrawMode(null)
      showToast({
        title: 'Bevinding aangemaakt',
        body: `'${findingTitle}' is aangemaakt`,
        variant: 'success',
      })
    }

    const { first: firstName, last: lastName } = user.identity.traits.name

    return (
      <>
        <Styled.Item>
          <p style={{ flex: 0.25 }}>Datum</p>
          <p style={{ flex: 0.75 }}>{date}</p>
        </Styled.Item>

        <Styled.Item>
          <p style={{ flex: 0.25 }}>Auteur</p>
          <p style={{ flex: 0.75 }}>
            {firstName} {lastName}
          </p>
        </Styled.Item>

        {addressData && (
          <Styled.Item>
            <p style={{ flex: 0.25 }}>Adres</p>
            <p style={{ flex: 0.75 }}>{addressData?.weergavenaam}</p>
          </Styled.Item>
        )}

        <Formik
          enableReinitialize
          initialValues={{
            findingTitle: '',
            findingDescription: '',
            noteDescription: '',
          }}
          validationSchema={Yup.object({
            findingTitle: Yup.string()
              .required('Veld is vereist')
              .min(3, 'Titel moet minimaal 3 karakters bevatten'),
            findingDescription: Yup.string()
              .required('Veld is vereist')
              .min(3, 'Beschrijving moet minimaal 3 karakters bevatten'),
            noteDescription: Yup.string()
              .required('Veld is vereist')
              .min(3, 'Notitie moet minimaal 3 karakters bevatten'),
          })}
          onSubmit={handleSubmit}
        >
          {({ handleChange, isSubmitting }) => (
            <Form>
              <TextInput
                maxLength={40}
                name="findingTitle"
                placeholder=""
                onChange={handleChange}
                style={{ width: 418 }}
              >
                Titel van de bevinding
              </TextInput>

              <TextInput
                placeholder=""
                type="textarea"
                name="findingDescription"
                onChange={handleChange}
                style={{ width: 418, height: 80 }}
              >
                Beschrijving van de soort bevinding
              </TextInput>
              <TextInput
                placeholder=""
                type="textarea"
                name="noteDescription"
                onChange={handleChange}
                style={{ width: 418, height: 100 }}
              >
                Notitie
              </TextInput>

              <Styled.MarginButton>
                <Button size="default" type="submit">
                  {isSubmitting ? (
                    <Spinner style={{ margin: 0 }} />
                  ) : (
                    'Bevinding aanmaken'
                  )}
                </Button>
              </Styled.MarginButton>
            </Form>
          )}
        </Formik>
      </>
    )
  }

  if (stage === 'loadingNewFinding') {
    return (
      <>
        <Spinner />
      </>
    )
  }

  return <></>
}

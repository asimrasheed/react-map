// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, {
  FunctionComponent,
  useEffect,
  useState,
  useContext,
} from 'react'

import { DataLayerProps } from '../../data/DataLayers'

import { DrawContext } from '../../providers/draw-provider'
import { MapContext } from '../../providers/map-provider'
import { useLayers } from '../../hooks/use-layers'
import useUser from '../../hooks/use-user'

import { getFinding } from '../../utils/findings/get-finding'
import { getFindingNotes } from '../../utils/findings/get-notes'
import { Count } from './count/Count'
import { ModalBasics } from './ModalBasics'

import BuildingDetail from './detail/BuildingDetail'
import Detail from './detail/Detail'

import { CreateFinding } from './finding-create/CreateFinding'
import { FindingDetail } from './finding-detail/FindingDetail'
import { NotesDrawer } from './finding-detail/NotesDrawer'
import { DeleteDrawer } from './finding-detail/DeleteDrawer'
import { EditDrawer } from './finding-detail/EditDrawer'
import RDWdetail from './detail/RDWDetail'
import VerbruikDetail from './detail/VerbruikDetail'

interface ModalProps {
  activeDataLayer: Partial<DataLayerProps>
}

export const Modal: FunctionComponent<ModalProps> = ({ activeDataLayer }) => {
  // TODO: Get selected finding through map when selecting finding

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [showFindingNotes, setShowFindingNotes] = useState(false)
  const [selectedFinding, setSelectedFinding] = useState(null)
  const { isLoggedIn } = useUser()
  const { user } = useUser()
  const { setDrawMode, drawMode } = useContext(DrawContext)
  const {
    analyzeMode,
    setAnalyzeMode,
    selectedFeatureAndEvent,
    setSelectedFeatureAndEvent,
  } = useContext(MapContext)

  const handleModalClose = () => {
    setTitle(null)
    setDescription(null)
    setSelectedFeatureAndEvent(null)
    setAnalyzeMode(null)
    setDrawMode(null)
  }

  // set modal title and description
  useEffect(() => {
    if (analyzeMode) {
      if (analyzeMode === 'count') {
        switch (activeDataLayer.source) {
          case 'SBI codes uit RVO energielabels':
            setTitle('SBI codes analyseren')
            setDescription(`Verdeling ${activeDataLayer.name}`)
            break
          case 'BAG, RDW, BOVAG':
            setTitle('Download basis dataset')
            setDescription(`${activeDataLayer.name}`)
            break
          default:
            setTitle('Verblijfsobjecten analyseren')
            setDescription(`Verdeling ${activeDataLayer.name}`)
            break
        }
        setDrawMode('initPolygons')
      }
    } else {
      handleModalClose()
    }
  }, [
    activeDataLayer.id,
    activeDataLayer.name,
    analyzeMode,
    handleModalClose,
    selectedFeatureAndEvent?.firstFeature,
    selectedFeatureAndEvent?.relevantStyleLayer,
  ])

  const handleGetSelectedFinding = async () => {
    const selectedFinding: any = await getFinding(
      selectedFeatureAndEvent.firstFeature.properties.id
    )

    if (!selectedFinding?.id) {
      return
    }

    const notes = isLoggedIn
      ? (await getFindingNotes(selectedFinding.id)) || []
      : []

    setSelectedFinding({ ...selectedFinding, notes })
  }

  const showFindingsDetail =
    analyzeMode === 'findings' && !drawMode && selectedFeatureAndEvent

  useEffect(() => {
    if (!selectedFeatureAndEvent || !showFindingsDetail) {
      return
    }
    handleGetSelectedFinding()
  }, [selectedFeatureAndEvent])

  // Modal uit on layer change
  const { activeLayerId } = useLayers()
  const [previousActiveDataLayer, setPreviousActiveDataLayer] =
    useState(activeDataLayer)

  useEffect(() => {
    if (activeLayerId !== previousActiveDataLayer.id) {
      if (analyzeMode === 'detail' || analyzeMode === 'buildingDetail') {
        if (
          activeDataLayer.styleLayers[0].tileSource ===
          previousActiveDataLayer.styleLayers[0].tileSource
        ) {
          // reload analyzing ???
        } else {
          setAnalyzeMode(null)
        }
      } else if (analyzeMode === 'findings') {
        handleModalClose()
      } else if (
        analyzeMode === 'count' &&
        activeDataLayer.drawType !== 'findings'
      ) {
        return
      } else {
        handleModalClose()
      }
      setPreviousActiveDataLayer(activeDataLayer)
    }
  }, [activeDataLayer])

  const [modalButtons, setModalButtons] = useState({
    delete: false,
    edit: false,
  })

  const handleModalButtons = (item: any) => {
    setModalButtons({ ...modalButtons, ...item })
  }

  const isCreatorOfFinding = user?.identity?.id === selectedFinding?.userId

  return (
    <ModalBasics
      title={title}
      description={description}
      handleClose={handleModalClose}
      handleEdit={
        showFindingsDetail &&
        isCreatorOfFinding &&
        (() => handleModalButtons({ edit: true }))
      }
      handleDelete={
        showFindingsDetail &&
        isCreatorOfFinding &&
        (() => handleModalButtons({ delete: true }))
      }
      style={
        analyzeMode === 'findings'
          ? { maxHeight: 'calc(100vh - 72px - 28px)' }
          : null
      }
      className={analyzeMode === 'findings' ? 'findingsDrawer' : ''}
    >
      {analyzeMode === 'count' && (
        <Count
          activeDataLayer={activeDataLayer}
          handleModalClose={handleModalClose}
        />
      )}

      {analyzeMode === 'findings' && !selectedFeatureAndEvent && (
        <CreateFinding setTitle={setTitle} />
      )}

      {showFindingsDetail &&
        (selectedFinding ? (
          <FindingDetail
            setTitle={setTitle}
            setShowFindingNotes={setShowFindingNotes}
            selectedFinding={selectedFinding}
            isCreatorOfFinding={isCreatorOfFinding}
          />
        ) : (
          <p>Kan geen bevinding ophalen op dit moment.</p>
        ))}

      {selectedFinding && isCreatorOfFinding && (
        <>
          {showFindingNotes && (
            <NotesDrawer
              handleClose={() => setShowFindingNotes(false)}
              selectedFinding={selectedFinding}
            />
          )}

          {modalButtons.edit && (
            <EditDrawer
              handleClose={() => handleModalButtons({ edit: false })}
              selectedFinding={selectedFinding}
              handleGetSelectedFinding={handleGetSelectedFinding}
            />
          )}

          {modalButtons.delete && (
            <DeleteDrawer
              handleModalClose={handleModalClose}
              handleClose={() => handleModalButtons({ delete: false })}
              selectedFinding={selectedFinding}
            />
          )}
        </>
      )}

      {analyzeMode === 'buildingDetail' && (
        <BuildingDetail
          feature={selectedFeatureAndEvent.firstFeature}
          setTitle={setTitle}
          content={selectedFeatureAndEvent.relevantStyleLayer?.detailModal(
            selectedFeatureAndEvent.firstFeature
          )}
        />
      )}
      {analyzeMode === 'detail' && activeLayerId === 'layer32' && (
        <VerbruikDetail
          setTitle={setTitle}
          content={selectedFeatureAndEvent.relevantStyleLayer?.detailModal(
            selectedFeatureAndEvent.firstFeature
          )}
          properties={selectedFeatureAndEvent.firstFeature.properties}
        />
      )}

      {analyzeMode === 'detail' &&
        activeLayerId !== 'layer32' &&
        selectedFeatureAndEvent.features.map((feature, i) => {
          return activeDataLayer.id === 'layerDook112' ||
            activeDataLayer.id === 'layerDook113' ? (
            <RDWdetail
              key={feature.id}
              setTitle={setTitle}
              content={selectedFeatureAndEvent.relevantStyleLayer?.detailModal(
                feature
              )}
              properties={feature.properties}
              defaultOpen={i === 0}
              title={
                selectedFeatureAndEvent.relevantStyleLayer?.popup(feature).title
              }
              onlyOne={selectedFeatureAndEvent.features.length <= 1}
              sum={selectedFeatureAndEvent.features.length}
            />
          ) : (
            <Detail
              key={feature.id}
              defaultOpen={i === 0}
              content={selectedFeatureAndEvent.relevantStyleLayer?.detailModal(
                feature
              )}
              setTitle={setTitle}
              title={
                selectedFeatureAndEvent.relevantStyleLayer?.popup
                  ? selectedFeatureAndEvent.relevantStyleLayer?.popup(feature)
                      .title
                  : ''
              }
              onlyOne={selectedFeatureAndEvent.features.length <= 1}
              sum={selectedFeatureAndEvent.features.length}
              selectedFeatureAndEvent={selectedFeatureAndEvent}
            />
          )
        })}
    </ModalBasics>
  )
}

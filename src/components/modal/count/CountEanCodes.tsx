// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'
import { getTotals } from '../../../utils/count/get-total'
import { DataLayerProps, StyleLayer } from '../../../data/DataLayers'

interface CountProps {
  activeDataLayer: Partial<DataLayerProps>
  relevantStyleLayer: Partial<StyleLayer>
  selectedPolygonForCount: any
}
export const CountEanCodes: FunctionComponent<CountProps> = ({
  activeDataLayer,
  relevantStyleLayer,
  selectedPolygonForCount,
}) => {
  const [totaalEANcodes, setTotaalEANcodes] = useState(null)

  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      if (relevantStyleLayer.attrName === 'pand_gas_ean_aansluitingen') {
        Promise.all([getTotals(selectedPolygonForCount, 'eancodes')]).then(
          (data: any) => {
            if (data[0]) {
              data = data[0].data.eancodes || 0
              setTotaalEANcodes(data)
            }
          }
        )
      } else if (!selectedPolygonForCount) {
        setTotaalEANcodes(null)
      }
    }
  }, [activeDataLayer, selectedPolygonForCount, relevantStyleLayer])

  return (
    <>
      {totaalEANcodes && (
        <p>
          <span>{totaalEANcodes.toLocaleString('nl-NL')}</span> gasaansluitingen
        </p>
      )}
    </>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'
import { Button, Icon } from '@commonground/design-system'
import { getSBICount, getSbiCsv } from '../../../utils/count/get-sbi-count'
import { DataLayerProps, StyleLayer } from '../../../data/DataLayers'
import GraphDiv from '../../asset-components/graphs/horizontalBars'
import { useDraw } from '../../../hooks/use-draw'
import Download from './download.svg'
import * as Styled from './Count.styled'

interface CountProps {
  activeDataLayer: Partial<DataLayerProps>
  relevantStyleLayer: Partial<StyleLayer>
  selectedPolygonForCount: any
}
export const CountSbiCodes: FunctionComponent<CountProps> = ({
  activeDataLayer,
  relevantStyleLayer,
  selectedPolygonForCount,
}) => {
  const [vizData, setVizData] = useState([])
  const [color, setColors] = useState<any[]>()
  const [colorRange, setColorRange] = useState([])
  const [total, setTotal] = useState(0)

  const { customCountFilter } = useDraw()

  // Get data requests
  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      const hasFilter = activeDataLayer.filterValues
      Promise.all([
        getSBICount(
          selectedPolygonForCount,
          relevantStyleLayer.attrName,
          hasFilter ? customCountFilter : null
        ),
      ]).then((data: any) => {
        if (data[0]) {
          setTotal(data[0].totaal)
          data = data[0].data
          const newData = Object.entries(data).map(([key, { count }]: any) => ({
            key,
            value: parseInt(count),
          }))
          setColorRange(Object.keys(data))
          setVizData(newData)
        } else {
          setVizData([])
          setTotal(0)
        }
      })
    } else {
      setVizData([])
    }
  }, [
    activeDataLayer,
    selectedPolygonForCount,
    relevantStyleLayer,
    customCountFilter,
  ])

  // Get color scale for graph
  useEffect(() => {
    if (relevantStyleLayer && vizData && vizData.length >= 1) {
      const stops = colorRange.map((color) =>
        relevantStyleLayer.colorScale(color)
      )
      setColors(stops || [])
    }
  }, [vizData, colorRange, relevantStyleLayer])

  return (
    <>
      {selectedPolygonForCount?.id && (
        <>
          <Styled.Totals>
            <p>
              <span>{total.toLocaleString('nl-NL')}</span> sbi codes
            </p>
          </Styled.Totals>

          {color && vizData?.length > 0 && (
            <>
              <GraphDiv
                colorScale={relevantStyleLayer.colorScale}
                labels={relevantStyleLayer.legendStops}
                dataSet={vizData}
                colors={color}
                width={500}
                time={750}
              />
              <Styled.Download>
                <Button
                  variant="link"
                  size="small"
                  onClick={() =>
                    getSbiCsv(selectedPolygonForCount, customCountFilter)
                  }
                >
                  <Icon as={Download} inline />
                  Download data
                </Button>
              </Styled.Download>
            </>
          )}
        </>
      )}
    </>
  )
}

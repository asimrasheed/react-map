// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'
import { Button, Icon } from '@commonground/design-system'
import { getTotals } from '../../../utils/count/get-total'
import { DataLayerProps, StyleLayer } from '../../../data/DataLayers'
import { useDraw } from '../../../hooks/use-draw'
import { useConfig } from '../../../hooks/use-config'
import { TooltipComp } from '../../asset-components/tooltip/Tooltip'
import Add from './add.svg'
import Delete from './delete.svg'
import { CountBuildings } from './CountBuildings'
import { CountSbiCodes } from './CountSbiCodes'
import { CountEanCodes } from './CountEanCodes'
import { CountAutoBranche } from './CountAutoBranche'
import * as Styled from './Count.styled'

interface CountProps {
  activeDataLayer: Partial<DataLayerProps>
  handleModalClose: () => void
}

export const Count: FunctionComponent<CountProps> = ({ activeDataLayer }) => {
  const [relevantStyleLayer, setRelevantStyleLayer] =
    useState<Partial<StyleLayer>>()
  const config = useConfig()
  const [totaalVerblijfsObject, setTotaalVerblijfsObject] = useState(0)
  const [totaalWoningEq, setTotaalWoningEqu] = useState(0)
  const {
    selectedPolygonForCount,
    setDeleteSelectedFeature,
    setStartDrawing,
    featureCollection,
  } = useDraw()

  // get the relevant StyleLayer
  useEffect(() => {
    if (activeDataLayer?.styleLayers?.[0]) {
      const layer = activeDataLayer.styleLayers.find(
        (currentLayer) => currentLayer.id !== 'layer0'
      )
      setRelevantStyleLayer(layer)
    }
  }, [activeDataLayer, activeDataLayer.styleLayers])

  // Get general data requests
  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      Promise.all([
        getTotals(selectedPolygonForCount, 'woningequivalent'),
      ]).then((data: any) => {
        if (data[0]) {
          data = data[0]
          setTotaalVerblijfsObject(parseInt(data.totaal))
          if (data.data) {
            setTotaalWoningEqu(parseInt(data.data.woningenquivalent))
          }
        }
      })
    } else {
      setTotaalVerblijfsObject(0)
      setTotaalWoningEqu(0)
    }
  }, [selectedPolygonForCount, relevantStyleLayer])

  const handleAddAreaClick = () => {
    setStartDrawing('polygons')
  }

  const handleDeleteAreaClick = () => {
    setDeleteSelectedFeature(true)
  }

  return (
    <>
      {!featureCollection ? (
        <Styled.Intro>
          <img src="/images/tekentool_uitleg_4.gif" alt="uitleg tekenen" />
          <p>
            Klik rondom een gebied op de kaart. Klik op het laatste punt om het
            gebied af te ronden.
          </p>
          <p>Let op, het vlak mag zichzelf niet kruisen.</p>
        </Styled.Intro>
      ) : (
        !selectedPolygonForCount?.id && (
          <Styled.Intro>
            <p>Selecteer of verwijder een bestaand gebied</p>
          </Styled.Intro>
        )
      )}

      {selectedPolygonForCount?.id && (
        <>
          <Styled.Totals>
            <Styled.TotalText>
              <span>{totaalVerblijfsObject.toLocaleString('nl-NL')}</span>
              Verblijfsobjecten
            </Styled.TotalText>
            {config.product === 'DEGO' && (
              <Styled.TotalText>
                <span>{totaalWoningEq.toLocaleString('nl-NL')}</span>
                <TooltipComp
                  tooltipText="Niet alle utiliteitsgebouwen zijn even groot. Om toch te kunnen vergelijken, worden utiliteitsgebouwen vaak omgerekend naar woningequivalenten. Daarbij staat 130 m² utiliteit gelijk aan 1 woningequivalent (weq). Een woning is ook gelijk aan 1 woningequivalent."
                  position="bottom"
                >
                  Woningequivalenten
                </TooltipComp>
              </Styled.TotalText>
            )}
            {relevantStyleLayer?.attrName === 'pand_gas_ean_aansluitingen' && (
              <CountEanCodes
                activeDataLayer={activeDataLayer}
                relevantStyleLayer={relevantStyleLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
          </Styled.Totals>

          {relevantStyleLayer?.tileSource?.source === 'bag' &&
            relevantStyleLayer.attrName !== 'pand_gas_ean_aansluitingen' &&
            activeDataLayer.drawType === 'count' && (
              <CountBuildings
                activeDataLayer={activeDataLayer}
                relevantStyleLayer={relevantStyleLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
          {relevantStyleLayer?.tileSource?.source === 'sbi' &&
            activeDataLayer.drawType === 'count' && (
              <CountSbiCodes
                activeDataLayer={activeDataLayer}
                relevantStyleLayer={relevantStyleLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
          {relevantStyleLayer?.tileSource?.source === 'verblijfsobjecten' &&
            activeDataLayer.drawType === 'count' && (
              <CountAutoBranche
                activeDataLayer={activeDataLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
        </>
      )}
      {!!featureCollection && (
        <Styled.Buttons>
          <Button variant="secondary" onClick={handleAddAreaClick}>
            <Icon as={Add} inline />
            Nog een gebied toevoegen
          </Button>
          <Button variant="secondary" onClick={handleDeleteAreaClick}>
            {selectedPolygonForCount ? (
              <>
                <Icon as={Delete} inline /> Verwijderen
              </>
            ) : (
              'Gebied selecteren'
            )}
          </Button>
        </Styled.Buttons>
      )}
    </>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Intro = styled.div`
  color: rgba(117, 117, 117, 1);
  font-size: 0.875rem;
  line-height: 1.5rem;
  text-align: center;
  margin-left: 2rem;
  margin-right: 2rem;
  > p {
    margin: 0.75rem;
  }
`

export const Totals = styled.div`
  /* display: flex;
    justify-content: space-between; */
  margin-bottom: 1.5rem;
  width: 100%;
  flex-wrap: wrap;
  position: relative;

  p {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    margin: 0;
    white-space: nowrap;
  }
`
export const TotalText = styled.p`
  display: flex;
  align-items: baseline;
  min-width: 45%;

  & + p span {
    margin-top: 1.5rem;
  }

  & > span {
    color: rgba(33, 33, 33, 1);
    font-size: 2rem;
    line-height: 2.5rem;
    padding-right: 0.25rem;
  }
`

export const Buttons = styled.div`
  gap: 1rem;
  display: flex;
  margin-top: 2rem;

  /* intermediate */
  @media screen and (max-width: 1250px) {
    margin-top: 1rem;
    p {
      font-size: 0.9rem;
    }
  }
`

export const GraphTitle = styled.p`
  text-align: center;
  margin-bottom: 0.25rem;
`

export const Download = styled.div`
  text-align: center;
  margin-top: 0.5rem;
`

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'
import { Button, Icon } from '@commonground/design-system'
import * as d3 from 'd3'
import Graph from '../../asset-components/graphs/verticalGraph'
import { DataLayerProps, StyleLayer } from '../../../data/DataLayers'
import { useDraw } from '../../../hooks/use-draw'
import {
  getVerblijfsobjectCount,
  getVerblijfsobjectCSV,
} from '../../../utils/count/get-verblijfsobject-count'
import * as Styled from './Count.styled'
import Download from './download.svg'

interface CountProps {
  activeDataLayer: Partial<DataLayerProps>
  relevantStyleLayer: Partial<StyleLayer>
  selectedPolygonForCount: any
}
export const CountBuildings: FunctionComponent<CountProps> = ({
  activeDataLayer,
  relevantStyleLayer,
  selectedPolygonForCount,
}) => {
  const [vizData, setVizData] = useState([])
  const [color, setColors] = useState()
  const [colorRange, setColorRange] = useState([])

  const { customCountFilter } = useDraw()

  // Get data requests
  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      const hasFilter = activeDataLayer.filterValues
      Promise.all([
        getVerblijfsobjectCount(
          selectedPolygonForCount,
          relevantStyleLayer.attrName,
          hasFilter ? customCountFilter : null
        ),
      ]).then((data: any) => {
        if (data[0]) {
          data = data[0].data
          if (
            relevantStyleLayer.attrName === 'elabel_voorlopig' ||
            relevantStyleLayer.attrName === 'elabel_definitief'
          ) {
            setVizData([
              { key: 'A', value: data[1] ? parseInt(data[1].count) : 0 },
              { key: 'B', value: data[2] ? parseInt(data[2].count) : 0 },
              { key: 'C', value: data[3] ? parseInt(data[3].count) : 0 },
              { key: 'D', value: data[4] ? parseInt(data[4].count) : 0 },
              { key: 'E', value: data[5] ? parseInt(data[5].count) : 0 },
              { key: 'F', value: data[6] ? parseInt(data[6].count) : 0 },
              { key: 'G', value: data[7] ? parseInt(data[7].count) : 0 },
            ])
          } else if (relevantStyleLayer.attrName === 'energieklasse_score') {
            setVizData([
              {
                key: 'A++++',
                value: data['A++++'] ? parseInt(data['A++++'].count) : 0,
              },
              {
                key: 'A+++',
                value: data['A+++'] ? parseInt(data['A+++'].count) : 0,
              },
              {
                key: 'A++',
                value: data['A++'] ? parseInt(data['A++'].count) : 0,
              },
              {
                key: 'A+',
                value: data['A+'] ? parseInt(data['A+'].count) : 0,
              },
              { key: 'A', value: data.A ? parseInt(data.A.count) : 0 },
              { key: 'B', value: data.B ? parseInt(data.B.count) : 0 },
              { key: 'C', value: data.C ? parseInt(data.C.count) : 0 },
              { key: 'D', value: data.D ? parseInt(data.D.count) : 0 },
              { key: 'E', value: data.E ? parseInt(data.E.count) : 0 },
              { key: 'F', value: data.F ? parseInt(data.F.count) : 0 },
              { key: 'G', value: data.G ? parseInt(data.G.count) : 0 },
            ])
          } else {
            const newData = Object.entries(data).map(
              ([key, { count }]: any) => ({
                key,
                value: parseInt(count),
              })
            )
            const totalRecords = d3.count(newData, (d: any) => d.key)
            // Making bins when too many categories
            if (totalRecords > 15) {
              const occuringValues = newData.map((d) => parseInt(d.key))
              const min = Math.max(d3.min(occuringValues), 1850)
              const max = Math.min(d3.max(occuringValues), 2021)
              const bin = d3.bin().domain([min, max]).thresholds(12) // Divide over a max of 12 bins
              const bins = bin(occuringValues)
              const dataForVisualisation = bins.map((element) =>
                element.length === 1
                  ? {
                      key: `${element[0]}`,
                      value: data[element[0]].count,
                    }
                  : {
                      key:
                        element.x0 === element.x1 - 1
                          ? element.x0
                          : `${element.x0}-${element.x1 - 1}`,
                      value: d3.sum(
                        newData.map(
                          (d) =>
                            d.key >= element.x0 && d.key <= element.x1 - 1
                              ? d.value
                              : 0,
                          (a: number, b: number) => a + b
                        )
                      ),
                    }
              )
              const colorValues = bins.map((element: any) =>
                element.length === 1 ? element[0] : element.x1
              )
              setColorRange([...colorValues, bins[bins.length - 1].x1])
              setVizData([
                ...dataForVisualisation,
                {
                  key: '' + bins[bins.length - 1].x1,
                  value: data[bins[bins.length - 1].x1].count,
                },
              ])
            } else {
              setColorRange(Object.keys(data))
              setVizData(newData)
            }
          }
        } else {
          setVizData([])
        }
      })
    } else if (!selectedPolygonForCount) {
      setVizData([])
      // setTotaalVerblijfsObject(0)
      // setTotaalWoningEqu(0)
    }
  }, [
    activeDataLayer,
    selectedPolygonForCount,
    relevantStyleLayer,
    customCountFilter,
  ])
  // Get color scale for graph
  useEffect(() => {
    if (relevantStyleLayer && vizData && vizData.length >= 1) {
      let stops
      if (relevantStyleLayer.expressionType === 'interpolate') {
        stops = colorRange.map((color) => relevantStyleLayer.colorScale(color))
      } else if (relevantStyleLayer.expressionType === 'match') {
        stops = relevantStyleLayer.colorScale.range()
      }
      setColors(stops || [])
    }
  }, [vizData, colorRange, relevantStyleLayer, activeDataLayer])

  return (
    <>
      {selectedPolygonForCount?.id && (
        <>
          {vizData?.length >= 1 && (
            <>
              <Styled.GraphTitle>
                Verdeling {activeDataLayer.name} van verblijfsobjecten
              </Styled.GraphTitle>

              {color && vizData?.length >= 1 && (
                <Graph
                  dataSet={vizData}
                  colors={color}
                  margin={{ top: 10, right: 0, bottom: 65, left: 35 }}
                  height={230}
                  time={750}
                  labels="true"
                  average={undefined}
                />
              )}
              <Styled.Download>
                <Button
                  variant="link"
                  size="small"
                  onClick={() =>
                    getVerblijfsobjectCSV(
                      selectedPolygonForCount,
                      customCountFilter
                    )
                  }
                >
                  <Icon as={Download} inline />
                  Download data
                </Button>
              </Styled.Download>
            </>
          )}
        </>
      )}
    </>
  )
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { Button, Icon } from '@commonground/design-system'
import {
  getAllAutoBrancheInfo,
  getBovagObjecten,
  getRDWobjecten,
} from '../../../utils/count/get-autobranche-export'
import { getVerblijfsobjectCSV } from '../../../utils/count/get-verblijfsobject-count'
import * as Styled from './Count.styled'
import Download from './download.svg'

interface CountProps {
  activeDataLayer: any
  selectedPolygonForCount: any
}
export const CountAutoBranche: FunctionComponent<CountProps> = ({
  activeDataLayer,
  selectedPolygonForCount,
}) => {
  const [totaalRdw, setTotaalRdw] = useState(0)
  const [totaalBovag, setTotaalBovag] = useState(0)

  useEffect(() => {
    if (selectedPolygonForCount?.id) {
      Promise.all([
        getRDWobjecten(selectedPolygonForCount),
        getBovagObjecten(selectedPolygonForCount),
      ]).then((data: any) => {
        if (data[0]) {
          setTotaalRdw(data[0].totaal)
        }
        if (data[1]) {
          setTotaalBovag(data[1].totaal)
        }
      })
    }
  }, [selectedPolygonForCount])

  return (
    <>
      {activeDataLayer.name !== 'Adressen (verblijfsobjecten)' && (
        <Styled.Totals>
          <Styled.TotalText>
            <span>{totaalRdw.toLocaleString('nl-NL')}</span> Bedrijven met RDW
            erkenning
          </Styled.TotalText>
          <Styled.TotalText>
            <span>{totaalBovag.toLocaleString('nl-NL')}</span> Bedrijven met
            BOVAG lidmaatschap
          </Styled.TotalText>
        </Styled.Totals>
      )}

      {selectedPolygonForCount?.id &&
        activeDataLayer.name !== 'Adressen (verblijfsobjecten)' && (
          <Styled.Download>
            <Button
              variant="link"
              size="small"
              onClick={() => getAllAutoBrancheInfo(selectedPolygonForCount)}
            >
              <Icon as={Download} inline />
              Download basis dataset
            </Button>
          </Styled.Download>
        )}
      {selectedPolygonForCount?.id &&
        activeDataLayer.name === 'Adressen (verblijfsobjecten)' && (
          <Styled.Download>
            <Button
              variant="link"
              size="small"
              onClick={() =>
                getVerblijfsobjectCSV(selectedPolygonForCount, null)
              }
            >
              <Icon as={Download} inline />
              Download data
            </Button>
          </Styled.Download>
        )}
    </>
  )
}

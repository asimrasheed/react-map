// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'
import Graph from '../../asset-components/graphs/verticalGraph'
import * as Styled from './Detail.styled'

type DetailProps = {
  content
  setTitle
  properties
}

const VerbruikDetail: FC<DetailProps> = ({ properties, content, setTitle }) => {
  setTitle(content.title)
  const [dataElek, setDataElek] = useState([])
  const [dataGas, setDataGas] = useState([])

  useEffect(() => {
    if (properties && Object.values(properties).length > 0) {
      setDataElek([
        {
          key: 'Appartement',
          value:
            properties.gemiddeld_elektriciteitsverbruik_appartement >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_appartement
              : 0,
        },
        {
          key: 'Tussen woning',
          value:
            properties.gemiddeld_elektriciteitsverbruik_tussenwoning >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_tussenwoning
              : 0,
        },
        {
          key: 'Hoek woning',
          value:
            properties.gemiddeld_elektriciteitsverbruik_hoekwoning >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_hoekwoning
              : 0,
        },
        {
          key: 'TweeOnder EenKap',
          value:
            properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning >= 0
              ? properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning
              : 0,
        },
        {
          key: 'Vrijstaand',
          value:
            properties.gem_elektriciteitsverbruik_vrijstaande_woning >= 0
              ? properties.gem_elektriciteitsverbruik_vrijstaande_woning
              : 0,
        },
      ])
      setDataGas([
        {
          key: 'Appartement',
          value:
            properties.gemiddeld_gasverbruik_appartement >= 0
              ? properties.gemiddeld_gasverbruik_appartement
              : 0,
        },
        {
          key: 'Tussen woning',
          value:
            properties.gemiddeld_gasverbruik_tussenwoning >= 0
              ? properties.gemiddeld_gasverbruik_tussenwoning
              : 0,
        },
        {
          key: 'Hoek woning',
          value:
            properties.gemiddeld_gasverbruik_hoekwoning >= 0
              ? properties.gemiddeld_gasverbruik_hoekwoning
              : 0,
        },
        {
          key: 'TweeOnder EenKap',
          value:
            properties.gemiddeld_gasverbruik_2_onder_1_kap_woning >= 0
              ? properties.gemiddeld_gasverbruik_2_onder_1_kap_woning
              : 0,
        },
        {
          key: 'Vrijstaand',
          value:
            properties.gemiddeld_gasverbruik_vrijstaande_woning >= 0
              ? properties.gemiddeld_gasverbruik_vrijstaande_woning
              : 0,
        },
      ])
    } else {
      setDataElek([])
      setDataGas([])
    }
  }, [properties])

  return (
    <>
      {content.content.map((item) =>
        item ? (
          <Styled.Item key={item}>
            <p>{item[0]}</p>
            <p>{item[1]}</p>
          </Styled.Item>
        ) : null
      )}
      {content.url && (
        <Styled.Item>
          <p>Externe link</p>
          <p>
            <a target="blank" href={content.url}>
              Klik hier
            </a>
          </p>
        </Styled.Item>
      )}
      <Styled.StyledGraph>
        <h4 className="graphTitle">
          Gemiddeld elektriciteitsverbruik naar woning type
        </h4>
        <h5 className="graphNote">
          Elektriciteit (
          <span style={{ color: '#ffbc2c', fontWeight: 'bold' }}>kwh</span>)
        </h5>
        {dataElek && (
          <Graph
            dataSet={dataElek}
            colors={['#ffbc2c']}
            height={200}
            margin={{ top: 10, right: 0, bottom: 110, left: 35 }}
            time={750}
            labels
          />
        )}
      </Styled.StyledGraph>
      <Styled.StyledGraph>
        <h4 className="graphTitle">Gemiddeld gasverbruik naar woning type</h4>
        <h5 className="graphNote">
          Gas (<span style={{ color: '#0B71A1', fontWeight: 'bold' }}>m3</span>)
        </h5>
        {dataGas && (
          <Graph
            dataSet={dataGas}
            colors={['#0B71A1']}
            height={200}
            margin={{ top: 10, right: 0, bottom: 110, left: 35 }}
            time={750}
            labels
          />
        )}
      </Styled.StyledGraph>
    </>
  )
}

export default VerbruikDetail

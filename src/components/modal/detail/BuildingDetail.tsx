// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { Dispatch, FC, SetStateAction } from 'react'
import { Icon } from '@commonground/design-system'
import ExternalIcon from '../../../../public/images/external-link.svg'
import useBuilding, { Building } from '../../../hooks/use-building'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'
type BuildingDetailProps = {
  feature
  setTitle: Dispatch<SetStateAction<string>>
  content
}

const renderAddress = ({
  straat,
  huisnummer,
  huisletter,
  huisnummertoevoeging,
  postcode,
  gemeentenaam,
}: Partial<Building>) =>
  `${straat} ${huisnummer}${huisletter}${
    huisnummertoevoeging && ` ${huisnummertoevoeging}`
  }, ${postcode} ${gemeentenaam}`

const BuildingDetail: FC<BuildingDetailProps> = ({
  feature,
  setTitle,
  content,
}) => {
  const { building, isLoading, isError } = useBuilding(
    feature.properties.pid || feature.id
  )
  const { selectedFeatureAndEvent } = useMap()

  setTitle(
    building?.length
      ? `${building.length} ${
          building.length > 1 ? 'resultaten' : 'resultaat'
        } gevonden`
      : 'Geen resultaat gevonden'
  )

  return (
    <>
      {isError && <p>Pandgegevens konden niet worden opgehaald.</p>}
      {!isLoading && (
        <>
          {building?.length ? (
            <>
              {content.type === 'building' && (
                <Styled.Item>
                  <p>Pand</p>
                  <p> {building?.[0].pid}</p>
                </Styled.Item>
              )}
              <Styled.Item>
                <p>Postcode gebied</p>
                <p>{building?.[0].postcode}</p>
              </Styled.Item>

              <Styled.Item>
                <p>Buurt</p>
                <p>{building?.[0].buurtnaam}</p>
              </Styled.Item>

              <Styled.Item>
                <p>Wijk</p>
                <p>{building?.[0].wijknaam}</p>
              </Styled.Item>

              {content.content.map((item) => {
                return item ? (
                  <Styled.Item key={item}>
                    <p>{item[0]}</p>
                    <p>{item[1]}</p>
                  </Styled.Item>
                ) : null
              })}

              {content.url && (
                <Styled.Item>
                  <a target="blank" href={content.url}>
                    {content.urlTitle ? content.urlTitle : 'Externe link'}{' '}
                    <Icon as={ExternalIcon} size="small" />
                  </a>
                </Styled.Item>
              )}
              <Styled.Item>
                <a
                  target="blank"
                  href={`https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html#?searchQuery=${building?.[0].pid}`}
                >
                  Bag viewer
                  <Icon as={ExternalIcon} size="small" />
                </a>
              </Styled.Item>

              {selectedFeatureAndEvent && (
                <Styled.Item>
                  {/* http://web.archive.org/web/20110903160743/http://mapki.com/wiki/Google_Map_Parameters#Street_View */}
                  <a
                    target="blank"
                    href={`http://maps.google.com/maps?q=&layer=c&cbll=${selectedFeatureAndEvent.event.lngLat.lat},${selectedFeatureAndEvent.event.lngLat.lng}&cbp=11,90,0,0,0`}
                  >
                    Bekijk deze locatie op Google streetview{' '}
                    <Icon as={ExternalIcon} size="small" />
                  </a>
                </Styled.Item>
              )}

              {content.type === 'building' && (
                <>
                  <Styled.Title>Adressen</Styled.Title>
                  {building.map((building) => (
                    <Styled.Address key={building.numid}>
                      {renderAddress({
                        straat: building.straat,
                        huisnummer: building.huisnummer,
                        huisletter: building.huisletter,
                        huisnummertoevoeging: building.huisnummertoevoeging,
                        postcode: building.postcode,
                        gemeentenaam: building.gemeentenaam,
                      })}
                    </Styled.Address>
                  ))}
                </>
              )}
            </>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  )
}

export default BuildingDetail

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useState } from 'react'
import { Icon, IconFlippingChevron } from '@commonground/design-system'
import { useMap } from '../../../hooks/use-map'
import { getRdwErkenning } from '../../../utils/rdw/get-rdw-erkenning'
import ExternalIcon from '../../../../public/images/external-link.svg'
import * as Styled from './Detail.styled'

type DetailProps = {
  content
  setTitle
  properties
  defaultOpen
  title
  onlyOne
  sum
}

const RDWdetail: FC<DetailProps> = ({
  properties,
  content,
  setTitle,
  defaultOpen,
  title,
  onlyOne,
  sum,
}) => {
  // setTitle(content.title)
  const [erkenningLijst, setErkenningLijst] = useState([])
  Promise.all([getRdwErkenning(properties.volgnummer)]).then(
    (data: string[]) => {
      return setErkenningLijst(data)
    }
  )
  const { selectedFeatureAndEvent } = useMap()

  setTitle(onlyOne ? content.title : sum + ' resultaten gevonden')

  const [isOpen, setIsOpen] = useState(defaultOpen)

  const toggle = () => {
    setIsOpen(!isOpen)
  }
  return (
    <Styled.Feature onClick={toggle} role="button">
      {!onlyOne && (
        <div className="heading">
          <h5>{title}</h5>
          <IconFlippingChevron
            flipHorizontal={isOpen}
            animationDuration={300}
          />
        </div>
      )}
      {isOpen && (
        <>
          {content.content.map((item) =>
            item ? (
              <Styled.Item key={item}>
                <p>{item[0]}</p>
                <p>{item[1]}</p>
              </Styled.Item>
            ) : null
          )}

          {content.url && (
            <Styled.Item>
              <a target="blank" href={content.url}>
                {content.urlTitle ? content.urlTitle : 'Externe link'}{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}
          {properties.volgnummer && (
            <Styled.Item>
              <p>Erkenningen</p>
              <ul>
                {erkenningLijst[0] &&
                  erkenningLijst[0].mappedErkenningen.map(
                    (item) => item && <li>{item}</li>
                  )}
              </ul>
            </Styled.Item>
          )}
          <Styled.Item>
            <a
              target="blank"
              href={`https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html#?searchQuery=${properties.vid}`}
            >
              Bag viewer <Icon as={ExternalIcon} size="small" />
            </a>
          </Styled.Item>

          {selectedFeatureAndEvent && (
            <Styled.Item>
              {/* http://web.archive.org/web/20110903160743/http://mapki.com/wiki/Google_Map_Parameters#Street_View */}
              <a
                target="blank"
                href={`http://maps.google.com/maps?q=${properties.straat} ${properties.huisnummer} ${properties.postcode} ${properties.plaats},&layer=c&cbll=${selectedFeatureAndEvent.event.lngLat.lat},${selectedFeatureAndEvent.event.lngLat.lng}&cbp=11,90,0,0,0`}
              >
                Bekijk deze locatie op Google streetview{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}
        </>
      )}
    </Styled.Feature>
  )
}

export default RDWdetail

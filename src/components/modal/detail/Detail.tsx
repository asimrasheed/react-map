// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useCallback, useState } from 'react'
import { Icon, IconFlippingChevron } from '@commonground/design-system'
import ExternalIcon from '../../../../public/images/external-link.svg'
import * as Styled from './Detail.styled'

type DetailProps = {
  content
  setTitle
  defaultOpen
  title
  onlyOne
  sum
  selectedFeatureAndEvent
}

const Detail: FC<DetailProps> = ({
  content,
  setTitle,
  defaultOpen,
  title,
  onlyOne,
  sum,
  selectedFeatureAndEvent,
}) => {
  setTitle(onlyOne ? content.title : sum + ' resultaten gevonden')

  const [isOpen, setIsOpen] = useState(defaultOpen)

  const toggle = () => {
    onlyOne ? '' : setIsOpen(!isOpen)
  }
  return (
    <Styled.Feature onClick={toggle} role="button">
      {!onlyOne && (
        <div className="heading">
          <h5>{title}</h5>
          <IconFlippingChevron
            flipHorizontal={isOpen}
            animationDuration={300}
          />
        </div>
      )}
      {isOpen && (
        <>
          {content.content.map((item) =>
            item ? (
              <Styled.Item key={item}>
                <p>{item[0]}</p>
                <p>{item[1]}</p>
              </Styled.Item>
            ) : null
          )}

          {content.url && (
            <Styled.Item>
              <a target="blank" href={content.url}>
                {content.urlTitle ? content.urlTitle : 'Externe link'}{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}

          {selectedFeatureAndEvent &&
            selectedFeatureAndEvent.relevantStyleLayer.tileSource?.source !==
              'cbs' && (
              <Styled.Item>
                {/* http://web.archive.org/web/20110903160743/http://mapki.com/wiki/Google_Map_Parameters#Street_View */}
                <a
                  target="blank"
                  onClick={(e) => {
                    e.stopPropagation()
                  }}
                  href={`http://maps.google.com/maps?layer=c&cbll=${selectedFeatureAndEvent.event.lngLat.lat},${selectedFeatureAndEvent.event.lngLat.lng}&cbp=11,90,0,0,0`}
                >
                  Bekijk deze locatie op Google streetview{' '}
                  <Icon as={ExternalIcon} size="small" />
                </a>
              </Styled.Item>
            )}
        </>
      )}
    </Styled.Feature>
  )
}

export default Detail

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Item = styled.div`
  display: flex;
  gap: 1rem;

  > p {
    flex: 1 1 0%;
    font-size: 0.875rem;
    line-height: 1.5rem;
    color: rgba(117, 117, 117, 1);
    margin-bottom: 0.125rem;
  }

  > p:last-child {
    font-weight: 700;
    margin-bottom: 1rem;
    color: rgb(33, 33, 33);
    font-size: 1rem;
    align-items: center;
    display: flex;
  }

  > ul li {
    font-weight: 700;
    margin-bottom: 0.125rem;
    color: rgb(33, 33, 33);
    font-size: 1rem;
    list-style: circle;
  }

  > ul {
    flex: 1 1 0%;
    margin-bottom: 1rem;
  }

  > a {
    align-items: center;
    display: flex;
    font-weight: 700;
    text-decoration: underline;
    font-size: 1rem;
    margin-bottom: 1rem;
    color: #424242;
  }
`
export const Address = styled.p`
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;
`
export const Title = styled.p`
  color: rgba(117, 117, 117, 1);
  font-size: 0.875rem;
  line-height: 1.5rem;
  margin-bottom: 0.125rem;
`
export const StyledGraph = styled.div`
  .graphTitle {
    margin-top: 1.125rem;
  }
  > h4 {
    margin-bottom: 1rem;
    color: rgb(33, 33, 33);
    font-size: 1rem;
  }
  > h5 {
    font-size: 0.875rem;
    line-height: 1.5rem;
    color: rgba(117, 117, 117, 1);
    flex: 1 1 0%;
    margin-bottom: 0.125rem;
  }
`

export const Feature = styled.div`
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;
  :last-child {
    border-bottom: 0;
  }

  .heading {
    display: flex;
    justify-content: space-between;
    aling-items: center;
    > h5 {
      line-height: 1.5rem;
      flex: 1 1 0%;
      margin-bottom: 0.125rem;
      color: rgb(33, 33, 33);
      font-weight: 700;
    }
  }
`

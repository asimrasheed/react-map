// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

import { Drawer } from '@commonground/design-system'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Address = styled.p`
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;
`

export const MarginButton = styled.div`
  margin-top: 2rem;
`

export const Item = styled.div`
  display: flex;
  margin-top: 0.25rem;
  margin-bottom: 0;

  p:first-child {
    color: rgba(117, 117, 117, 1);
    font-size: 0.875rem;
    line-height: 1.5rem;
    flex: 1 1 0%;
  }
`

export const DrawerContent = styled(Drawer.Content)`
  h3 {
    font-size: 1.5rem;
    font-weight: 700;
    padding-bottom: 1rem;
    margin-top: 2rem;
    border-bottom: 1px solid lightgray;
  }

  img {
    flex-shrink: 0;
    margin-left: auto;
    padding-left: 20;
    width: 260px;
  }

  .notecontainer {
    display: flex;
    padding-top: 1rem;
    padding-bottom: 1rem;
    border-bottom-width: 1px;
    border-bottom-color: lightgray;

    > p {
      color: rgb(107 114 128);
      flex-shrink: 0;
      font-size: 0.875rem; /* 14px */
      width: 120px;
    }
  }
`

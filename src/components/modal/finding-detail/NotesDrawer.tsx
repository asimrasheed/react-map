// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useContext, useState } from 'react'
import { Button, TextInput, Icon, Drawer } from '@commonground/design-system'
import { Formik } from 'formik'
import * as Yup from 'yup'

import useUser from '../../../hooks/use-user'
import { postFindingNote } from '../../../utils/findings/post-note'
import AddIcon from '../../../../public/images/add.svg'
import { ToasterContext } from '../../../providers/toaster-provider'
import * as Styled from './finding-detail.styles'

interface NotesDrawer {
  handleClose: () => void
  selectedFinding: any
}

export const NotesDrawer: FC<NotesDrawer> = ({
  handleClose,
  selectedFinding,
}) => {
  const { user } = useUser()
  const [isAddingNote, setIsAddingNote] = useState(false)
  const { showToast } = useContext(ToasterContext)

  const { title, notes, id: findingId } = selectedFinding

  const formattedTitle = title.charAt(0).toUpperCase() + title.slice(1)

  const handleAddNote = async ({ description }) => {
    const body = {
      description,
      userId: user.id,
      username: `${user.identity.traits.name.first} ${user.identity.traits.name.last}`,
      image: null,
    }
    const newFindingNote = await postFindingNote(findingId, body)

    if (newFindingNote) {
      notes.unshift({ ...body, createdAt: new Date().toISOString() })
    }

    setIsAddingNote(false)
    showToast({
      title: 'Notitie toegevoegd',
      variant: 'success',
    })
  }

  const handleAddNoteVisiblity = () => {
    setIsAddingNote(true)
  }

  // TODO: This drawer has additional styling using classname findingsDrawer. That is bad practice.
  return (
    <Drawer closeHandler={handleClose}>
      <Drawer.Header
        as="div"
        style={{ fontWeight: 'bold', textSize: '1.5rem' }}
        title={formattedTitle}
      />
      <Styled.DrawerContent>
        {!isAddingNote && (
          <Button
            size="default"
            onClick={handleAddNoteVisiblity}
            style={{ marginRight: '1rem' }}
            variant="secondary"
          >
            <Icon as={AddIcon} inline />
            Notitie toevoegen
          </Button>
        )}

        {isAddingNote && (
          <Formik
            initialValues={{ description: '' }}
            validationSchema={Yup.object().shape({
              description: Yup.string().required('Veld is vereist'),
            })}
            onSubmit={handleAddNote}
          >
            {({ handleSubmit, isSubmitting }) => (
              <form onSubmit={handleSubmit} style={{ width: '100%' }}>
                <TextInput name="description" type="textarea" size="l">
                  Notitie
                </TextInput>

                <Styled.MarginButton>
                  <Button
                    disabled={isSubmitting}
                    size="default"
                    variant="secondary"
                    style={{ marginRight: '1.5rem' }}
                    onClick={() => setIsAddingNote(false)}
                  >
                    Annuleren
                  </Button>
                  <Button
                    type="submit"
                    disabled={isSubmitting}
                    size="default"
                    variant="primary"
                  >
                    <Icon
                      as={AddIcon}
                      style={{
                        transform: 'rotate(-90deg)',
                      }}
                      inline
                    />
                    Notitie opslaan
                  </Button>
                </Styled.MarginButton>
              </form>
            )}
          </Formik>
        )}

        <h3>
          {notes.length} {notes.length === 1 ? 'notitie' : 'notities'}
        </h3>

        {notes.map(({ description, createdAt, image }) => {
          const date = new Intl.DateTimeFormat('nl-NL', {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
          }).format(new Date(createdAt))

          return (
            <div key={date} className="notecontainer">
              <p>{date}</p>
              <div>
                <p>{description}</p>
              </div>
              {image && <img src={image} />}
            </div>
          )
        })}
      </Styled.DrawerContent>
    </Drawer>
  )
}

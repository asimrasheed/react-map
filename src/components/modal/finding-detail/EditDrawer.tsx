// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useContext } from 'react'
import { Button, TextInput, Icon, Drawer } from '@commonground/design-system'
import { Formik } from 'formik'
import * as Yup from 'yup'

import useUser from '../../../hooks/use-user'

import { postFindingNote } from '../../../utils/findings/post-note'
import { getFindingNotes } from '../../../utils/findings/get-notes'
import { updateFinding } from '../../../utils/findings/update-finding'
import CheckIcon from '../../../../public/images/check.svg'
import { ToasterContext } from '../../../providers/toaster-provider'
import * as Styled from './finding-detail.styles'

interface EditDrawerProps {
  handleClose: () => void
  handleGetSelectedFinding: () => void
  selectedFinding: any
}

export const EditDrawer: FC<EditDrawerProps> = ({
  handleClose,
  handleGetSelectedFinding,
  selectedFinding,
}) => {
  const { user } = useUser()
  const { showToast } = useContext(ToasterContext)

  const handleUpdateFinding = async (body, noteBody) => {
    if (!selectedFinding?.id) {
      return
    }
    const updates = await Promise.all([
      await updateFinding(selectedFinding.id, body),
      await postFindingNote(selectedFinding.id, noteBody),
    ])
    if (!updates) {
      console.warn('Could not update finding')
      return
    }

    handleGetSelectedFinding()
    return true
  }

  const { title, description } = selectedFinding

  const formattedTitle = title.charAt(0).toUpperCase() + title.slice(1)
  const formattedDescription =
    description.charAt(0).toUpperCase() + description.slice(1)

  return (
    <Drawer closeHandler={handleClose}>
      <Drawer.Header
        as="div"
        style={{ fontWeight: 'bold', textSize: '1.5rem' }}
        title="Bevinding bewerken"
      />
      <Drawer.Content>
        <Formik
          initialValues={{
            title,
            description,
          }}
          validationSchema={Yup.object().shape({
            title: Yup.string()
              .min(3, 'Titel moet minimaal 3 karakters bevatten')
              .required('Veld is vereist'),
            description: Yup.string()
              .min(3, 'Beschrijving moet minimaal 3 karakters bevatten')
              .required('Veld is vereist'),
          })}
          onSubmit={async ({ title, description }) => {
            const noteBody = {
              description: `De titel en beschrijving van de bevinding zijn aangepast. Voorheen was de titel "${formattedTitle}" en de beschrijving "${formattedDescription}".`,
              userId: user.id,
              username: `${user.identity.traits.name.first} ${user.identity.traits.name.last}`,
              image: null,
            }

            const updatedFinding = await handleUpdateFinding(
              { title, description },
              noteBody
            )

            if (updatedFinding) {
              showToast({
                title: 'Bevinding bewerkt',
                variant: 'success',
              })
            } else {
              showToast({
                title: 'Bevinding niet bewerkt',
                variant: 'error',
              })
            }

            handleClose()
          }}
        >
          {({ values, handleChange, handleSubmit, isSubmitting }) => (
            <form onSubmit={handleSubmit} style={{ width: '100%' }}>
              <TextInput
                maxLength={40}
                name="title"
                placeholder={title}
                onChange={handleChange}
                style={{ width: 418 }}
              >
                Bevinding titel
              </TextInput>

              <TextInput
                placeholder={description}
                type="textarea"
                name="description"
                onChange={handleChange}
                style={{ width: 418, height: 80 }}
              >
                Bevinding beschrijving
              </TextInput>

              <Styled.MarginButton>
                <Button
                  type="submit"
                  disabled={
                    isSubmitting ||
                    (values.title === title &&
                      values.description === description)
                  }
                  size="default"
                  variant="primary"
                >
                  <Icon as={CheckIcon} inline />
                  Bevinding opslaan
                </Button>
              </Styled.MarginButton>
            </form>
          )}
        </Formik>
      </Drawer.Content>
    </Drawer>
  )
}

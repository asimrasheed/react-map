// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useContext } from 'react'
import { Button, Icon, Drawer } from '@commonground/design-system'

import { useMap } from '../../../hooks/use-map'
import { deleteFinding } from '../../../utils/findings/delete-finding'
import DeleteIcon from '../../../../public/images/delete.svg'
import { ToasterContext } from '../../../providers/toaster-provider'
import * as Styled from './finding-detail.styles'

type DeleteDrawerProps = {
  handleModalClose: () => void
  handleClose: () => void
  selectedFinding: any
}

export const DeleteDrawer: FC<DeleteDrawerProps> = ({
  handleModalClose,
  handleClose,
  selectedFinding,
}) => {
  const { setReloadFindingsTiles } = useMap()
  const { showToast } = useContext(ToasterContext)

  const { title, id: findingId } = selectedFinding

  const formattedTitle = title.charAt(0).toUpperCase() + title.slice(1)

  const handleDeleteFinding = async () => {
    await deleteFinding(findingId)
    handleModalClose()
    setReloadFindingsTiles(true)
    showToast({
      title: 'Bevinding verwijderd',
      variant: 'success',
    })
  }

  return (
    <Drawer closeHandler={handleClose}>
      <Drawer.Header
        as="div"
        style={{ fontWeight: 'bold', textSize: '1.5rem' }}
        title="Bevinding verwijderen"
      />
      <Drawer.Content>
        <p>
          Weet je zeker dat je de bevinding{' '}
          <span style={{ fontWeight: 'bold' }}>{formattedTitle}</span> inclusief
          alle notities wilt verwijderen?
        </p>
        <Styled.MarginButton>
          <Button size="default" onClick={handleDeleteFinding}>
            <Icon as={DeleteIcon} inline />
            Bevinding verwijderen
          </Button>
        </Styled.MarginButton>
      </Drawer.Content>
    </Drawer>
  )
}

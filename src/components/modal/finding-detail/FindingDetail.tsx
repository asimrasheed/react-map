// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useEffect } from 'react'
import { Button, Icon } from '@commonground/design-system'

import { objectToCsv } from '../../../utils/to-csv'

import DownloadIcon from '../../../../public/images/download-line.svg'
import LockIcon from '../../../../public/images/lock.svg'
import * as Styled from './finding-detail.styles'

type FindingDetailProps = {
  setShowFindingNotes: (state: any) => void
  selectedFinding: any
  isCreatorOfFinding: boolean
  setTitle: (state: any) => void
}

export const FindingDetail: FC<FindingDetailProps> = ({
  setShowFindingNotes,
  selectedFinding,
  isCreatorOfFinding,
  setTitle,
}) => {
  const { title, notes, username, description, addressDisplay } =
    selectedFinding

  useEffect(() => setTitle(title || ''), [selectedFinding])

  const currentDate = new Date(selectedFinding.createdAt)
  const date = new Intl.DateTimeFormat('nl-NL', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }).format(currentDate)

  const handleDownload = () => {
    const formatDate = new Intl.DateTimeFormat('nl-NL', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    }).format(currentDate)

    const notes = selectedFinding.notes
      .map((note) => ['\n', formatDate, note.description].join(','))
      .join('\n')

    const findingDownload = {
      auteur: username,
      bevinding: title,
      beschrijving: description,
      locatie: addressDisplay || 'Geen adres opgegegeven',
      notities: `datum, beschrijving, ${notes}`,
    }
    objectToCsv(findingDownload)
  }

  return (
    <>
      <div>
        {title && (
          <Styled.Item>
            <p style={{ flex: 0.25 }}>Titel</p>
            <p style={{ flex: 0.75 }}>{title}</p>
          </Styled.Item>
        )}

        {description && (
          <Styled.Item style={{ marginTop: '0.25rem' }}>
            <p style={{ flex: 0.25 }}>Beschrijving</p>
            <p style={{ flex: 0.75 }}>{description}</p>
          </Styled.Item>
        )}

        {username && (
          <Styled.Item style={{ marginTop: '2.25rem' }}>
            <p style={{ flex: 0.25 }}>Auteur</p>
            <p style={{ flex: 0.75 }}>
              {username}{' '}
              {isCreatorOfFinding && (
                <span style={{ color: '#6b7280' }}> ( jij )</span>
              )}
            </p>
          </Styled.Item>
        )}

        <Styled.Item style={{ marginTop: '0.25rem' }}>
          <p style={{ flex: 0.25 }}>Datum</p>
          <p style={{ flex: 0.75 }}>{date}</p>
        </Styled.Item>

        {addressDisplay && (
          <Styled.Item style={{ marginTop: '0.25rem' }}>
            <p style={{ flex: 0.25 }}>Locatie</p>
            <p style={{ flex: 0.75 }}>{addressDisplay}</p>
          </Styled.Item>
        )}
      </div>

      <div style={{ marginTop: '1.5rem' }}>
        {isCreatorOfFinding ? (
          <>
            <Button
              style={{ marginRight: '1.5rem' }}
              size="default"
              onClick={() => setShowFindingNotes(true)}
            >
              {notes.length} {notes.length === 1 ? 'notitie' : 'notities'}{' '}
              inzien
            </Button>
            <Button size="default" variant="secondary" onClick={handleDownload}>
              <Icon as={DownloadIcon} inline />
              Exporteer naar CSV
            </Button>
          </>
        ) : (
          <p style={{ display: 'flex', alignItems: 'center' }}>
            <Icon as={LockIcon} inline style={{ color: '#6b7280' }} />
            Alleen de auteur van de bevinding kan de notities inzien.
          </p>
        )}
      </div>
    </>
  )
}

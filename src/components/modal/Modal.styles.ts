// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const MainContainer = styled.div`
  width: 482px;
  border-radius: ${(p) => p.theme.tokens.spacing02};
  background-color: ${(p) => p.theme.tokens.colorBackground};
  box-shadow: ${(p) => p.theme.boxShadow};
  z-index: 500;
  max-height: 90vh;
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 2rem 2rem 0;

  /* intermediate */
  @media screen and (max-width: 1250px) {
    width: 350px;
    max-height: 80vh;
    padding: 1.3rem 1.3rem 0;
    overflow: hidden;
  }
`
export const Header = styled.div`
  display: flex;

  > div {
    margin-left: 1rem;
  }
  > div:first-of-type {
    margin-left: auto;
  }

  & h1 {
    max-width: 268px;
    overflow: hidden;
    text-overflow: ellipsis;
    font-weight: 700;
    font-size: 1.5rem;
    line-height: 2rem;
    z-index: 10;
    margin: 0;
  }

  /* intermediate */
  @media screen and (max-width: 1250px) {
    & h1 {
      font-size: 1.2rem;
      line-height: 1.5rem;
    }
  }
`

export const Icon = styled.div`
  display: flex;
  cursor: pointer;
  color: rgba(117, 117, 117, 1);
`

export const Description = styled.p`
  font-size: 1.125rem;
  line-height: 1.5rem;
`

export const Content = styled.div`
  padding: 2rem 0;
  overflow: auto;
  height: 100%;
  position: relative;
  @media screen and (max-width: 1250px) {
    padding: 1.2rem 0;
  }
`

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const getRdwErkenning = async (
  volgnummerId: number
): Promise<any | null> => {
  try {
    const response = await fetch(
      `https://opendata.rdw.nl/resource/nmwb-dqkz.json?volgnummer=${volgnummerId}`
    )

    if (!response.ok) {
      throw new Error(response?.statusText)
    }

    const erkenningen = await response.json()

    const mappedErkenningen = erkenningen.map((erkenning) => {
      return erkenning.erkenning
    })
    return { mappedErkenningen }
  } catch (e) {
    console.warn(`Could not fetch finding ${volgnummerId}. Message:`, e)
    return null
  }
}

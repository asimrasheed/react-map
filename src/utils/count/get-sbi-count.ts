// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

export const getSBICount = async function (
  geojson: { geometry: any },
  layer: string,
  filter: number[] | string[] | [string, string[], string] | string
): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  let totaal

  const url = filter
    ? `https://ds.sbicodes.commondatafactory.nl/list/?groupby=${layer}&reduce=count${filter}`
    : `https://ds.sbicodes.commondatafactory.nl/list/?groupby=${layer}&reduce=count`

  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })
    totaal = response.headers.get('total-items')
    const data = await response.json()
    return { totaal: totaal, data }
  } catch (e) {
    console.warn(
      'Dit getekend gebied is niet in orde, leeg of te klein. Probeer een nieuwe gebied te tekenen.'
    )
  }
}

export const getSbiCsv = async function (
  geojson: { geometry: any },
  filter: number[] | string[] | [string, string[], string] | string
): Promise<void> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)

  try {
    const response = await fetch(
      `https://ds.sbicodes.commondatafactory.nl/list/?format=csv&${filter}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const blob = await response.blob()
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')
    a.href = url
    const datum = new Date()
    a.download = `SelectieDownload${datum}.csv`
    document.body.appendChild(a)
    a.click()
    a.remove()
  } catch (e) {
    alert('Download here')
  }
}

// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

export const getTotals = async function (
  geojson: { geometry: any },
  layer: string
): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  let totaal: string
  try {
    const response = await fetch(
      `https://ds.vboenergie.commondatafactory.nl/list/?reduce=${layer}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    totaal = response.headers.get('total-items')
    const data = await response.json()
    return { totaal: totaal, data }
  } catch (e) {
    console.warn(
      'Dit getekend gebied is niet in orde, leeg of te klein. Probeer een nieuwe gebied te tekenen.'
    )
  }
}

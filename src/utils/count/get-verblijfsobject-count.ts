// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

export const getVerblijfsobjectCount = async function (
  geojson: { geometry: any },
  layer: string,
  filter: number[] | string[] | [string, string[], string] | string
): Promise<any> {
  switch (layer) {
    case 'energieklasse_score':
      layer = 'energieklasse'
      break
    case 'elabel_voorlopig':
      layer = 'labelscore_voorlopig'
      break
    case 'elabel_definitief':
      layer = 'labelscore_definitief'
      break
    case 'bouwjaar':
      layer = 'pand_bouwjaar'
      break
    default:
      break
  }

  const url = filter
    ? `https://ds.vboenergie.commondatafactory.nl/list/?groupby=${layer}&reduce=count&${filter}`
    : `https://ds.vboenergie.commondatafactory.nl/list/?groupby=${layer}&reduce=count`
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  let totaal
  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })
    totaal = response.headers.get('total-items')
    const data = await response.json()
    return { totaal, data }
  } catch (e) {}
}

export const getVerblijfsobjectCSV = async function (
  geojson: { geometry: any },
  filter: number[] | string[] | [string, string[], string] | string
): Promise<void> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const response = await fetch(
      `https://ds.vboenergie.commondatafactory.nl/list/?format=csv&${filter}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const blob = await response.blob()
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')
    a.href = url
    const datum = new Date()
    a.download = `SelectieDownload${datum}.csv`
    document.body.appendChild(a)
    a.click()
    a.remove()
  } catch (e) {
    alert('Download here')
  }
}

// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import { getRdwErkenning } from '../rdw/get-rdw-erkenning'

export const getVerblijfsobjecten = async function (geojson: {
  geometry: any
}): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const response = await fetch(
      `https://ds.vboenergie.commondatafactory.nl/list/?`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const data = await response.json()
    return data
  } catch (e) {
    alert('Download here')
  }
}

export const getRDWobjecten = async function (geojson: {
  geometry: any
}): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const response = await fetch(`https://ds.rdw.commondatafactory.nl/list/?`, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })
    const totaal = response.headers.get('total-items')

    let data = await response.json()

    data = await Promise.all(
      data.map(async (d) => {
        const e = await getRdwErkenning(d.volgnummer)
        return { ...d, rdw_erkenningen: e.mappedErkenningen }
      })
    )
    return { totaal, data }
  } catch (e) {
    alert('Download here')
  }
}

export const getBovagObjecten = async function (geojson: {
  geometry: any
}): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const response = await fetch(
      `https://ds.bovag.commondatafactory.nl/list/?`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const totaal = response.headers.get('total-items')

    const data = await response.json()

    return { totaal, data }
  } catch (e) {
    alert('Download here')
  }
}

export const getAllAutoBrancheInfo = async function (geojson: {
  geometry: any
}): Promise<void> {
  Promise.all([
    getVerblijfsobjecten(geojson),
    getRDWobjecten(geojson),
    getBovagObjecten(geojson),
  ]).then((data) => {
    let verblijfsobjectData = data[0]
    //Remove properties
    verblijfsobjectData = verblijfsobjectData.map((d) => {
      const {
        geometry,
        energieklasse,
        group_id_2020,
        labelscore_definitief,
        labelscore_voorlopig,
        p6_gas_aansluitingen_2020,
        p6_gasm3_2020,
        p6_totaal_pandoppervlak_m2,
        p6_kwh_2020,
        pand_gas_aansluitingen,
        pand_gas_ean_aansluitingen,
        point,
        woningequivalent,
        gemeentecode,
        buurtcode,
        buurtnaam,
        wijkcode,
        wijknaam,
        provinciecode,
        provincienaam,
        vid,
        pid,
        ...verblijfsobjectDataNew
      } = d
      return verblijfsobjectDataNew
    })

    let rdwData = data[1].data
    //Remove properties
    rdwData = rdwData.map((d) => {
      const {
        geometry,
        huisletter,
        huisnummer,
        huisnummertoevoeging,
        id,
        lid,
        pid,
        plaats,
        postcode,
        sid,
        straat,
        vid,
        oppervlakte,
        ...rdwDataNew
      } = d
      return rdwDataNew
    })

    let bovagData = data[2].data
    bovagData = bovagData.map((d) => {
      const {
        geometry,
        huisletter,
        huisnummer,
        huisnummertoevoeging,
        id,
        lid,
        pid,
        plaats,
        postcode,
        sid,
        straat,
        vid,
        oppervlakte,
        telephone,
        url,
        lidid,
        emailaddress,
        bovag_plaats,
        bovag_postcode,
        boavag_toevoeging,
        bag_toevoeging,
        address,
        bovag_toevoeging,
        ...bovagDataNew
      } = d
      return bovagDataNew
    })

    // Merge RDW data
    let allData = verblijfsobjectData.map((verblijfsobject) => [
      verblijfsobject,
      ...rdwData.filter(
        (rdwBedrijf) =>
          rdwBedrijf.numid.toString().padStart(16, '0') ===
          verblijfsobject.numid.toString().padStart(16, '0')
      ),
    ])

    // Rename fields
    function renameFields(feature, list) {
      list.forEach((names) => {
        delete Object.assign(feature, { [names[1]]: feature[names[0]] })[
          names[0]
        ]
      })
      return feature
    }

    // rename rdw incremental fields
    allData.map((d) => {
      d.length > 1 &&
        d.slice(1).forEach((feature, i) => {
          i = i + 1
          const renamingList = [
            ['volgnummer', `rdw_volgnummer_${i}`],
            ['naam_bedrijf', `rdw_naam_bedrijf_${i}`],
            ['gevelnaam', `rdw_gevelnaam_${i}`],
            ['rdw_erkenningen', `rdw_erkenningen_${i}`],
            ['match_score', `rdw_matchscore_${i}`],
            ['rdw_toevoeging', `rdw_toevoeging_${i}`],
          ]
          renameFields(feature, renamingList)
        })
    })

    // Merge objects together
    allData = allData.map((d) => {
      const data = d.reduce(function (result, current) {
        return Object.assign(result, current)
      }, {})
      return data
    })

    // Merge BOVAG DATA
    allData = allData.map((verblijfsobject) => [
      verblijfsobject,
      ...bovagData.filter(
        (bovagBedrijf) =>
          bovagBedrijf.numid.toString().padStart(16, '0') ===
          verblijfsobject.numid.toString().padStart(16, '0')
      ),
    ])

    // rename bovag incremental fields
    allData.map((d) => {
      d.length > 1 &&
        d.slice(1).forEach((feature, i) => {
          i = i + 1
          const renamingList = [
            ['name', `bovag_naam_${i}`],
            ['bovag_adres', `bovag_adres_${i}`],
            ['match_score', `bovag_matchscore_${i}`],
          ]
          renameFields(feature, renamingList)
        })
    })

    // Merge objects together
    allData = allData.map((d) => {
      const data = d.reduce(function (result, current) {
        return Object.assign(result, current)
      }, {})
      return data
    })

    // Fields of longest object
    const fields = Object.keys(
      allData.reduce(function (result, obj) {
        return Object.assign(result, obj)
      }, {})
    )

    // Make CSV Using ;
    const replacer = function (key, value) {
      return value === null ? '' : value
    }
    let csv = allData.map(function (row) {
      return fields
        .map(function (fieldName) {
          return JSON.stringify(row[fieldName], replacer)
        })
        .join(';')
    })
    csv.unshift(fields.join(';')) // add header column
    csv = csv.join('\r\n')

    // Download to csv
    const url = window.URL.createObjectURL(new Blob([csv]))
    const a = document.createElement('a')
    a.href = url
    const datum = new Date()
    a.download = `SelectieDownload${datum}.csv`
    document.body.appendChild(a)
    a.click()
    a.remove()
  })
}

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export enum UrlParamsKey {
  Label = 'label',
  Layer = 'layer',
  Sublayer = 'sublayer',
  Query = 'query',
  Tab = 'tab',
}

export const setURLParams = (key: UrlParamsKey, value: string): void => {
  const searchParams = new URLSearchParams(window.location.search)

  if (!value) {
    searchParams.delete(key)
  } else {
    searchParams.set(key, value)
  }

  const newurl = `${window.location.origin}${
    window.location.pathname
  }?${searchParams.toString()}${window.location.hash}`

  window.history.pushState(newurl, '', newurl)
}

export const getURLParams = (): string => {
  const searchParams = new URLSearchParams(window.location.search)

  return `${window.location.pathname}?${searchParams.toString()}${
    window.location.hash
  }`
}

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const objectToCsv = (data: Record<string, any>): void => {
  // const data = [
  //   ['rahul', 'delhi', 'accounts dept'],
  //   ['rajeev', 'UP', 'sales dept'],
  // ]
  // const csvArrayContent =
  // 'data:text/csv;charset=utf-8,' + data.map((e) => e.join(',')).join('\n')

  const csvObjectContent =
    'data:text/csv;charset=utf-8,' +
    Object.entries(data)
      .map(([key, value]) => [key, value].join(','))
      .join('\n')

  //     var csv = data.map(function(d){
  //       return JSON.stringify(d);
  //    })
  //    .join('\n')
  //    .replace(/(^\[)|(\]$)/mg, '');

  // const encodedUri = encodeURI(csvContent)
  // window.open(encodedUri)

  const encodedUri = encodeURI(csvObjectContent)
  const link = document.createElement('a')
  link.setAttribute('href', encodedUri)
  link.setAttribute(
    'download',
    `Bevinding - ${data.bevinding.toLowerCase()}.csv`
  )
  document.body.appendChild(link)
  link.click()
}

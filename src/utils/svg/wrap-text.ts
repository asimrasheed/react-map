// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const wrapTextVertical = (
  selection: any,
  width: number
): Promise<any> => {
  return selection.each(function () {
    const text = d3.select(this)
    const words = String(text.text()).split(/\s+/).reverse()

    let word:
      | string
      | number
      | boolean
      | d3.ValueFn<SVGTSpanElement, unknown, string | number | boolean>
    let line = []
    let lineNumber = 0
    const lineHeight = 1.1 // ems
    const y = text.attr('y')
    const dy = parseFloat(text.attr('dy'))
    let tspan = text
      .text(null)
      .append('tspan')
      .attr('x', 0)
      .attr('y', y)
      .attr('dy', dy + 'em')
    while ((word = words.pop())) {
      line.push(word)
      tspan.text(line.join(' '))
      if (tspan.node().getComputedTextLength() > width) {
        line.pop()
        tspan.text(line.join(' '))
        line = [word]
        tspan = text
          .append('tspan')
          .attr('x', 0)
          .attr('y', y)
          .attr('dy', ++lineNumber * lineHeight + dy + 'em')
          .text(word)
      }
    }
  })
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const wrapTextHorizontal = (
  selection: any,
  width: number
): Promise<any> => {
  return selection.each(function () {
    const text = d3.select(this)

    const words = String(text.text()).split(/\s+/).reverse()

    let word:
      | string
      | number
      | boolean
      | d3.ValueFn<SVGTSpanElement, unknown, string | number | boolean>
    let line = []
    let lineNumber = 0
    const lineHeight = 1 // ems
    const x = text.attr('x')
    const y = text.attr('y')
    const dy = parseFloat(text.attr('dy'))
    let tspan = text
      .text(null)
      .append('tspan')
      .attr('x', x)
      .attr('y', y)
      .attr('dy', dy + 'em')
    while ((word = words.pop())) {
      line.push(word)
      tspan.text(line.join(' '))
      if (tspan.node().getComputedTextLength() > width) {
        line.pop()
        tspan.text(line.join(' '))
        line = [word]
        tspan = text
          .append('tspan')
          .attr('x', x)
          .attr('y', y)
          .attr('dy', ++lineNumber * lineHeight + dy + 'em')
          .text(word)
      }
    }
  })
}

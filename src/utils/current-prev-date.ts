// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

export const getCurrentAndPrevDate = (
  months: number,
  middle?: string
): number[] => {
  const dateNow = new Date()
  const dateNowISO = Date.parse(dateNow.toString()) / 1000

  const prevDate = new Date()
  prevDate.setMonth(prevDate.getMonth() - months)
  const prevDateISO = Date.parse(prevDate.toString()) / 1000

  const middleData = new Date()
  middleData.setMonth(middleData.getMonth() - months / 2)
  const middleDataISO = Date.parse(middleData.toString()) / 1000

  if (middle) {
    return [prevDateISO, middleDataISO, dateNowISO]
  } else {
    return [prevDateISO, dateNowISO]
  }
}

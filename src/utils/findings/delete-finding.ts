// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const deleteFinding = async (findingId: string): Promise<any | null> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings/${findingId}`,
      {
        method: 'DELETE',
        credentials: 'include',
      }
    )

    if (!response.ok) {
      console.warn('Could not delete finding.')
      return null
    }
  } catch (e) {
    console.warn(e)
  }
}

// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const getFindings = async (): Promise<any | null> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings`,
      {
        credentials: 'include',
      }
    )

    if (!response.ok) {
      console.warn('Could not fetch findings')
      return
    }

    const findings = await response.json()
    const mappedFindings = findings.map((finding) => {
      finding.id = finding.ID
      finding.createdAt = finding.CreatedAt
      delete finding.ID
      delete finding.CreatedAt
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { ID, CreatedAt, ...restFinding } = finding
      return restFinding
    })

    return mappedFindings
  } catch (e) {
    console.warn('Could not fetch findings API')
  }
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

interface updateFindingProps {
  userId: string
  username: string

  geodata: string

  addressDisplay: string
  houseNlt: string
  street: string
  postalCode: string
  locality: string

  title: string
  description: string

  geojson: {
    type: string
    coordinates: [number, number]
  }
}

export const updateFinding = async (
  findingId: string,
  body: Partial<updateFindingProps>
): Promise<any | null> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings/${findingId}`,
      {
        credentials: 'include',
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )

    if (!response.ok) {
      console.warn('Could not update finding')
      return null
    }

    return await response.json()
  } catch (e) {
    console.warn(e)
  }
}

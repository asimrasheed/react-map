// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export interface getFindingNotesProps {
  findingId: string
}

export const getFindingNotes = async (
  findingId: getFindingNotesProps
): Promise<unknown> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings/${findingId}/notes`,
      {
        credentials: 'include',
      }
    )
    if (!response.ok) {
      console.warn('Could not fetch findings')
      return
    }

    const notes = await response.json()

    const linkedNotes = notes.filter(
      (note) => String(note.finding_id) === String(findingId)
    )
    if (!linkedNotes) {
      console.warn('Could not retrieve notes')
      return
    }
    const mappedNotes = linkedNotes.map((note) => {
      note.id = note.ID
      note.createdAt = note.CreatedAt
      note.title = note.Title

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { ID, CreatedAt, Title, ...restNote } = note
      return restNote
    })

    return mappedNotes
  } catch (e) {
    console.warn(e)
  }
}

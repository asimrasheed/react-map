// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export interface PostFindingNoteBody {
  description: string
}

export const postFindingNote = async (
  findingId: string,
  body: PostFindingNoteBody
): Promise<unknown> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings/${findingId}/notes`,
      {
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )

    if (!response.ok) {
      console.warn('Could not post note')
      return
    }

    return await response.json()
  } catch (e) {
    console.warn(e)
  }
}

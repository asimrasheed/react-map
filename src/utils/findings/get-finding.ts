// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const getFinding = async (findingId: number): Promise<any | null> => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_FINDINGS_API}/findings/${findingId}`,
      {
        credentials: 'include',
      }
    )

    if (!response.ok) {
      throw new Error(response?.statusText)
    }

    const finding = await response.json()

    const mappedFinding = finding.map((finding) => {
      finding.id = finding.ID
      finding.createdAt = finding.CreatedAt
      delete finding.ID
      delete finding.CreatedAt
      return finding
    })

    return mappedFinding[0]
  } catch (e) {
    console.warn(`Could not fetch finding ${findingId}. Message:`, e)
    return null
  }
}

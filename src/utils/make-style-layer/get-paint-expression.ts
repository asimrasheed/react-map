// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'

import { DataDrivenPropertyValueSpecification } from 'maplibre-gl'
import { StyleLayer } from '../../data/DataLayers'

// Get colors from d3 color scale
const getInterpolateColors = (outline: string, colorScale): string[] => {
  const breakpoints = colorScale.domain()
  const extraStops = colorScale.ticks(20)
  const colorStops = breakpoints.concat(extraStops)
  const uniqueItems = Array.from(new Set(colorStops))
  uniqueItems.sort((a: number, b: number) => a - b)

  let colorArray: any[] = []
  for (const elem of uniqueItems) {
    let c = colorScale(elem)
    c = outline === 'outline' ? d3.rgb(c).darker(0.7).toString() : c
    colorArray = colorArray.concat(elem, c)
  }
  return colorArray
}

// Get colors from d3 color scale
const getColors = (outline: string, colorScale): string[] => {
  const breakpoints = colorScale.domain()
  let colorArray: any[] = []
  for (const elem of breakpoints) {
    let color = colorScale(elem)
    color = outline === 'outline' ? d3.rgb(color).darker(0.7).toString() : color
    colorArray = colorArray.concat(elem, color)
  }
  return colorArray
}

export function getPaintExpression(
  outline: string,
  styleLayer: Partial<StyleLayer>
): DataDrivenPropertyValueSpecification<string> | string {
  let firstPart, middlePart

  switch (styleLayer.expressionType) {
    case 'interpolate':
      firstPart = ['interpolate', ['linear'], ['get', styleLayer.attrName]]
      styleLayer.attrName === 'pt'
        ? (firstPart = [
            'interpolate',
            ['linear'],
            ['to-number', ['get', styleLayer.attrName]],
          ])
        : (firstPart = [
            'interpolate',
            ['linear'],
            ['get', styleLayer.attrName],
          ])

      middlePart = getInterpolateColors(outline, styleLayer.colorScale)

      return firstPart.concat(middlePart)
    case 'step':
      firstPart = ['step', ['get', styleLayer.attrName]]
      middlePart = getColors(outline, styleLayer.colorScale)
      middlePart.shift()
      return firstPart.concat(middlePart)
    case 'match': {
      firstPart = ['match', ['get', styleLayer.attrName]]
      middlePart = getColors(outline, styleLayer.colorScale)
      middlePart.push('#e0e0e0')

      return firstPart.concat(middlePart)
    }
    case 'case':
      if (styleLayer.caseArray) {
        firstPart = ['case']
        const colorScale = styleLayer.colorScale
        styleLayer.caseArray.forEach((element) => {
          return firstPart.push(['has', element], colorScale(element as any))
        })
        middlePart = 'grey'
      } else {
        firstPart = ['case', ['has', styleLayer.attrName]]
        middlePart = styleLayer.colorScale.range()

        if (outline === 'outline')
          middlePart = middlePart.map((color) =>
            d3.rgb(color).darker(0.7).toString()
          )
      }

      return firstPart.concat(middlePart)
    case 'bevindingen':
      // Dit is een lelijke hack
      firstPart = ['match', ['get', styleLayer.attrName]]

      middlePart = [
        styleLayer.user?.userId,
        outline === 'outline'
          ? d3
              .rgb(styleLayer.colorScale(1) as any)
              .darker(0.7)
              .toString()
          : styleLayer.colorScale(1),

        outline === 'outline'
          ? d3
              .rgb(styleLayer.colorScale(0) as any)
              .darker(0.7)
              .toString()
          : styleLayer.colorScale(0),
      ]

      return firstPart.concat(middlePart)

    default:
      // default = 'fill'
      return styleLayer.color || '#9ecf1b'
  }
}

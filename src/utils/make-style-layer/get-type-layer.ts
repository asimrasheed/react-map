// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import {
  CircleLayerSpecification,
  FillExtrusionLayerSpecification,
  FillLayerSpecification,
  LineLayerSpecification,
  RasterLayerSpecification,
  SymbolLayerSpecification,
} from 'maplibre-gl'
import { StyleLayer } from '../../data/DataLayers'
import { getPaintExpression } from './get-paint-expression'

export const makeFillLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>,
  invert?: boolean
): FillLayerSpecification => {
  return {
    id,
    type: 'fill',
    ...styleLayer.tileSource,
    layout: {
      visibility: 'visible',
    },
    paint: {
      'fill-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression('outline', styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression('outline', styleLayer),
        invert ? '#d2d2d2' : getPaintExpression('fill', styleLayer),
      ],
      'fill-outline-color': [
        'case',
        ['==', ['feature-state', 'click'], 'click'],
        '#000000',
        invert ? '#d2d2d2' : getPaintExpression('outline', styleLayer),
      ],

      'fill-opacity': styleLayer.opacity || 1,
    },
  }
}

export const makeFillExtrusionLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>
): FillExtrusionLayerSpecification => {
  return {
    id: id,
    type: 'fill-extrusion',
    ...styleLayer.tileSource,
    layout: {
      visibility: 'visible',
    },
    paint: {
      'fill-extrusion-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression('outline', styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression('outline', styleLayer),
        getPaintExpression('fill', styleLayer),
      ],
      'fill-extrusion-height': [
        'interpolate',
        ['linear'],
        ['zoom'],
        13,
        0,
        16,
        ['*', 1.5, ['ceil', ['to-number', ['get', styleLayer.extrusionAttr]]]],
      ],
      'fill-extrusion-opacity': styleLayer.opacity || 1,
    },
  }
}

export const makeCircleLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>
): CircleLayerSpecification => {
  return {
    id: id,
    type: 'circle',
    ...styleLayer.tileSource,
    layout: {
      visibility: 'visible',
    },
    paint: {
      'circle-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression('outline', styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression('outline', styleLayer),
        getPaintExpression('fill', styleLayer),
      ],
      'circle-stroke-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        '#000000',
        ['==', ['feature-state', 'click'], 'click'],
        '#000000',
        getPaintExpression('outline', styleLayer),
      ],
      'circle-stroke-width': [
        'interpolate',
        ['linear'],
        ['zoom'],
        8,
        0.5,
        12,
        1,
        16,
        3,
      ],
      'circle-radius': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        1.5,
        12,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          6,
          ['==', ['feature-state', 'click'], 'click'],
          6,
          3,
        ],
        16,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          12,
          ['==', ['feature-state', 'click'], 'click'],
          12,
          5,
        ],
        20,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          15,
          ['==', ['feature-state', 'click'], 'click'],
          15,
          10,
        ],
      ],
      'circle-opacity': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        1,
        12,
        styleLayer.opacity / 2 || 0.8,
        16,
        styleLayer.opacity || 0.5,
      ],
      'circle-stroke-opacity': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        1,
        12,
        styleLayer.opacity || 1,
        16,
        styleLayer.opacity || 1,
      ],
    },
  }
}

export const makeLineLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>
): LineLayerSpecification => {
  return {
    id: id,
    type: 'line',
    ...styleLayer.tileSource,
    layout: {
      visibility: 'visible',
      'line-join': 'round',
      'line-cap': 'round',
    },
    paint: {
      'line-color': getPaintExpression('outline', styleLayer),
      'line-width': [
        'interpolate',
        ['linear'],
        ['zoom'],
        9,
        1,
        13.5,
        4,
        20,
        10,
      ],
      'line-opacity': styleLayer.opacity || 0.7,
    },
  }
}

export const makeRasterLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>
): RasterLayerSpecification => {
  return {
    id: id,
    type: 'raster',
    source: styleLayer.rasterSource,
    layout: {
      visibility: 'visible',
    },
    paint: {
      'raster-contrast': 0.4,
      'raster-brightness-min': 0,
      'raster-saturation': 0,
      'raster-opacity': styleLayer.opacity || 0.9,
    },
  }
}

export const makeSymbolLayer = (
  id: string,
  styleLayer: Partial<StyleLayer>,
  name: string
): SymbolLayerSpecification => {
  return {
    id: id,
    type: 'symbol',
    ...styleLayer.tileSource,
    layout: {
      'symbol-placement': 'point',
      'text-pitch-alignment': 'map',
      'text-field': `{${name}}`,
      'text-font': ['SourceSansPro-Bold'],
      'text-size': 12,
      'text-max-width': 5,
      'text-variable-anchor': [
        'top-right',
        'top-left',
        'bottom-right',
        'bottom-left',
        'right',
        'left',
        'top',
        'bottom',
      ],
      'text-line-height': 1.1,
      'text-radial-offset': 0.1,
      'text-justify': 'center',
      'text-padding': 5,
      'text-allow-overlap': false,
      'text-rotation-alignment': 'viewport',
    },
    paint: {
      'text-opacity': 1,
      'text-color': '#212121',
      'text-halo-color': '#F8F9FC',
      'text-halo-width': 1.5,
      'text-translate-anchor': 'viewport',
    },
  }
}

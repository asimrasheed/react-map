// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

// GIS WKT Function
export const wktPointToGeoJson = (
  wktPoint: string
): { type: string; coordinates: number[] } => {
  // Make a wkt Point object
  if (!wktPoint.includes('POINT')) {
    throw TypeError('Provided WKT geometry is not a point.')
  }
  const coordinateTuple = wktPoint.split('(')[1].split(')')[0]
  const x = parseFloat(coordinateTuple.split(' ')[0])
  const y = parseFloat(coordinateTuple.split(' ')[1])
  return {
    type: 'Point',
    coordinates: [x, y],
  }
}

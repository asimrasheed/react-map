// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export const getFeatureInfoWMS = async (
  bounds: { _sw: { lat: any; lng: any }; _ne: { lat: any; lng: any } },
  request: string
) => {
  const url =
    request +
    `&i=1&j=1` +
    `&BBOX=${bounds._sw.lat},${bounds._sw.lng},${bounds._ne.lat},${bounds._ne.lng}` +
    `&WIDTH=2&HEIGHT=2` +
    '&CRS=EPSG:4326'

  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      if (data.features.length) {
        return data.features?.[0]
      }
    })
}
